﻿using System;
using System.Collections.Generic;
using System.Linq;
using Energy.Core;
using Energy.Core.Data;
using Energy.Core.Domain.Splits;
using Energy.Core.Domain.Bills;
using Energy.Core.Domain.Customers;
using Energy.Core.Domain.Common;

namespace Energy.Services.Splits
{
    public partial class SplitService : ISplitService
    {
        #region Fields

        private readonly IRepository<Split> _splitRepository;
        private readonly IRepository<SplitArea> _splitAreaRepository;
        private readonly IRepository<Bill> _billRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<GenericAttribute> _gaRepository;

        #endregion

        #region Cunstructor

        public SplitService(
            IRepository<Split> splitRepository,
            IRepository<SplitArea> splitAreaRepository,
            IRepository<Bill> billRepository,
            IRepository<Customer> customerRepository,
            IRepository<GenericAttribute> gaRepository)
        {
            this._splitRepository = splitRepository;
            this._splitAreaRepository = splitAreaRepository;
            this._billRepository = billRepository;
            this._customerRepository = customerRepository;
            this._gaRepository = gaRepository;
        }

        #endregion

        #region Methods

        public virtual Split GetSplitById(int splitId)
        {
            if (splitId == 0)
                return null;

            return _splitRepository.GetById(splitId);
        }

        public IPagedList<Split> GetAllSplits(
            int splitId = 0, int customerId = 0, string customerName = null,
            string splitName = null, string fileNo = null,
            string serialNo = null, int pageIndex = 0, int pageSize = 2147483647)
        {
            var query = _splitRepository.Table;

            if (splitId > 0)
                query = query.Where(s => s.Id == splitId);

            if (customerId > 0)
                query = query.Where(s => s.Customer_Id == customerId);

            if (!String.IsNullOrWhiteSpace(splitName))
                query = query.Where(s => s.Title.Contains(splitName));

            if (!String.IsNullOrWhiteSpace(fileNo))
                query = query.Where(s => s.FileNo.Contains(fileNo));

            if (!String.IsNullOrWhiteSpace(serialNo))
                query = query.Where(s => s.SerialNo.Contains(serialNo));

            query = query.OrderBy(s => s.Id);

            var splits = new PagedList<Split>(query, pageIndex, pageSize);

            return splits;
        }

        public string GetCustomerName(int splitId)
        {
            int customerId = (from s in _splitRepository.Table
                              join c in _customerRepository.Table on s.Customer_Id equals c.Id
                              where s.Id == splitId
                              select c.Id).FirstOrDefault();

            var firstname = _gaRepository.Table.Where(z => z.KeyGroup == "Customer" &&
                        z.Key == SystemCustomerAttributeNames.FirstName &&
                        z.EntityId == customerId).Select(x => x.Value).FirstOrDefault();

            var lastname = _gaRepository.Table.Where(z => z.KeyGroup == "Customer" &&
                        z.Key == SystemCustomerAttributeNames.LastName &&
                        z.EntityId == customerId).Select(x => x.Value).FirstOrDefault();

            return firstname + " " + lastname;
        }

        public bool CheckExist(int splitId)
        {
            var query =  from s in _splitRepository.Table
                         where s.Id == splitId
                         select s;

            if (query.ToList().Count == 0)
                return false;
            else
                return true;
        }

        public SplitArea getSplitAreaBySplitId(int splitId)
        {
            var query = (from sa in _splitAreaRepository.Table
                         where sa.SplitID == splitId
                         select sa).FirstOrDefault();

            return query;
        }

        public IList<Split> GetSplitsByBillDate(int yy, int tt)
        {
            var query = from s in _splitRepository.Table
                        join b in _billRepository.Table on s.Id equals b.SplitID
                        where b.Year == yy && b.Term == tt && s.Active && b.Type != "archive"
                        select s;

            return query.ToList();
        }

        public void InsertSplit(Split split)
        {
            if (split == null)
                throw new ArgumentNullException("split");

            _splitRepository.Insert(split);
        }

        public void UpdateSplit(Split split)
        {
            if (split == null)
                throw new ArgumentNullException("split");

            _splitRepository.Update(split);
        }

        public void DeleteSplit(Split split)
        {
            if (split == null)
                throw new ArgumentNullException("split");

            _splitRepository.Delete(split);
        }

        #endregion
    }
}
