﻿using System.Collections.Generic;
using Energy.Core;
using Energy.Core.Domain.Splits;

namespace Energy.Services.Splits
{
    public partial interface ISplitService
    {
        Split GetSplitById(int splitId);

        IPagedList<Split> GetAllSplits(
            int splitId = 0, int customerId = 0, string customerName = null,
            string splitName = null, string fileNo = null,
            string serialNo = null, int pageIndex = 0, int pageSize = 2147483647);

        string GetCustomerName(int splitId);
        bool CheckExist(int splitId);
        SplitArea getSplitAreaBySplitId(int splitId);
        IList<Split> GetSplitsByBillDate(int yy, int tt);
        void InsertSplit(Split split);
        void UpdateSplit(Split split);
        void DeleteSplit(Split split);
    }
}
