using System.Collections.Generic;
using Energy.Core.Domain.Directory;

namespace Energy.Services.Directory
{
    public partial interface ICityService
    {
        void DeleteCity(City city);
        City GetCityById(int cityId);
        IList<City> GetCitiesByStateId(int stateId, bool showHidden = false);
        IList<City> GetCities(bool showHidden = false);
        void InsertCity(City city);
        void UpdateCity(City city);
    }
}
