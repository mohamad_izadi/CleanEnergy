using System;
using System.Collections.Generic;
using System.Linq;
using Energy.Core.Caching;
using Energy.Core.Data;
using Energy.Core.Domain.Directory;
using Energy.Services.Events;

namespace Energy.Services.Directory
{
    public partial class CityService : ICityService
    {
        #region Constants

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {1} : country ID
        /// </remarks>
        private const string CityS_ALL_KEY = "Energy.City.all-{0}";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string CityS_PATTERN_KEY = "Energy.City.";

        #endregion

        #region Fields

        private readonly IRepository<City> _cityRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;

        #endregion

        #region Ctor

        public CityService(ICacheManager cacheManager,
            IRepository<City> cityRepository,
            IEventPublisher eventPublisher)
        {
            _cacheManager = cacheManager;
            _cityRepository = cityRepository;
            _eventPublisher = eventPublisher;
        }

        #endregion

        #region Methods
        public virtual void DeleteCity(City city)
        {
            if (city == null)
                throw new ArgumentNullException("city");

            _cityRepository.Delete(city);

            _cacheManager.RemoveByPattern(CityS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityDeleted(city);
        }
        public virtual City GetCityById(int cityId)
        {
            if (cityId == 0)
                return null;

            return _cityRepository.GetById(cityId);
        }
        public virtual IList<City> GetCitiesByStateId(int stateId, bool showHidden = false)
        {
            string key = string.Format(CityS_ALL_KEY, stateId);
            return _cacheManager.Get(key, () =>
            {
                var query = from sp in _cityRepository.Table
                            orderby sp.DisplayOrder, sp.Name
                            where sp.State_Id == stateId &&
                            (showHidden || sp.Published)
                            select sp;
                var Citys = query.ToList();
                return Citys;
            });
        }
        public virtual IList<City> GetCities(bool showHidden = false)
        {
            var query = from sp in _cityRepository.Table
                        orderby sp.State_Id, sp.DisplayOrder, sp.Name
                where showHidden || sp.Published
                select sp;
            var cities = query.ToList();
            return cities;
        }
        public virtual void InsertCity(City city)
        {
            if (city == null)
                throw new ArgumentNullException("city");

            _cityRepository.Insert(city);

            _cacheManager.RemoveByPattern(CityS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(city);
        }
        public virtual void UpdateCity(City city)
        {
            if (city == null)
                throw new ArgumentNullException("city");

            _cityRepository.Update(city);

            _cacheManager.RemoveByPattern(CityS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(city);
        }

        #endregion
    }
}
