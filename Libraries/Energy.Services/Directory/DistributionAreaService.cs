using System;
using System.Collections.Generic;
using System.Linq;
using Energy.Core.Caching;
using Energy.Core.Data;
using Energy.Core.Domain.Directory;
using Energy.Services.Events;

namespace Energy.Services.Directory
{
    /// <summary>
    /// Distribution area service
    /// </summary>
    public partial class DistributionAreaService : IDistributionAreaService
    {
        #region Constants

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {1} : country ID
        /// </remarks>
        private const string DistributionAreaS_ALL_KEY = "Nop.DistributionArea.all-{0}";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string DistributionAreaS_PATTERN_KEY = "Nop.DistributionArea.";

        #endregion

        #region Fields

        private readonly IRepository<DistributionArea> _distributionAreaRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="DistributionAreaRepository">Distribution area repository</param>
        /// <param name="eventPublisher">Event published</param>
        public DistributionAreaService(ICacheManager cacheManager,
            IRepository<DistributionArea> distributionAreaRepository,
            IEventPublisher eventPublisher)
        {
            _cacheManager = cacheManager;
            _distributionAreaRepository = distributionAreaRepository;
            _eventPublisher = eventPublisher;
        }

        #endregion

        #region Methods
        /// <summary>
        /// Deletes a Distribution area
        /// </summary>
        /// <param name="DistributionArea">The Distribution area</param>
        public virtual void DeleteDistributionArea(DistributionArea distributionArea)
        {
            if (distributionArea == null)
                throw new ArgumentNullException("distributionArea");

            _distributionAreaRepository.Delete(distributionArea);

            _cacheManager.RemoveByPattern(DistributionAreaS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityDeleted(distributionArea);
        }

        /// <summary>
        /// Gets a Distribution area
        /// </summary>
        /// <param name="DistributionAreaId">The Distribution area identifier</param>
        /// <returns>Distribution area</returns>
        public virtual DistributionArea GetDistributionAreaById(int distributionAreaId)
        {
            if (distributionAreaId == 0)
                return null;

            return _distributionAreaRepository.GetById(distributionAreaId);
        }
        
        /// <summary>
        /// Gets a Distribution area collection by country identifier
        /// </summary>
        /// <param name="countryId">Region identifier</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Distribution area collection</returns>
        public virtual IList<DistributionArea> GetDistributionAreasByRegionId(int regionId, bool showHidden = false)
        {
            string key = string.Format(DistributionAreaS_ALL_KEY, regionId);
            return _cacheManager.Get(key, () =>
            {
                var query = from sp in _distributionAreaRepository.Table
                            orderby sp.DisplayOrder, sp.Name
                            where sp.RegionId == regionId &&
                            (showHidden || sp.Published)
                            select sp;
                var distributionAreas = query.ToList();
                return distributionAreas;
            });
        }

        /// <summary>
        /// Gets all distribution areas
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Distribution area collection</returns>
        public virtual IList<DistributionArea> GetDistributionAreas(bool showHidden = false)
        {
            var query = from sp in _distributionAreaRepository.Table
                        orderby sp.RegionId, sp.DisplayOrder, sp.Name
                where showHidden || sp.Published
                select sp;
            var DistributionAreas = query.ToList();
            return DistributionAreas;
        }

        /// <summary>
        /// Inserts a distribution area
        /// </summary>
        /// <param name="DistributionArea">Distribution area</param>
        public virtual void InsertDistributionArea(DistributionArea distributionArea)
        {
            if (distributionArea == null)
                throw new ArgumentNullException("distributionArea");

            _distributionAreaRepository.Insert(distributionArea);

            _cacheManager.RemoveByPattern(DistributionAreaS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(distributionArea);
        }

        /// <summary>
        /// Updates a distribution area
        /// </summary>
        /// <param name="distributionArea">Distribution area</param>
        public virtual void UpdateDistributionArea(DistributionArea distributionArea)
        {
            if (distributionArea == null)
                throw new ArgumentNullException("distributionArea");

            _distributionAreaRepository.Update(distributionArea);

            _cacheManager.RemoveByPattern(DistributionAreaS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(distributionArea);
        }

        #endregion
    }
}
