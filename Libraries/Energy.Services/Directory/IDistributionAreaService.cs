using System.Collections.Generic;
using Energy.Core.Domain.Directory;

namespace Energy.Services.Directory
{
    /// <summary>
    /// DistributionArea service interface
    /// </summary>
    public partial interface IDistributionAreaService
    {
        /// <summary>
        /// Deletes a distributionArea
        /// </summary>
        /// <param name="DistributionArea">The distribution area</param>
        void DeleteDistributionArea(DistributionArea DistributionArea);

        /// <summary>
        /// Gets a distribution area
        /// </summary>
        /// <param name="DistributionAreaId">The distribution area identifier</param>
        /// <returns>distribution area</returns>
        DistributionArea GetDistributionAreaById(int DistributionAreaId);
        
        /// <summary>
        /// Gets a distribution area collection by region identifier
        /// </summary>
        /// <param name="regionId">Region identifier</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>distribution area collection</returns>
        IList<DistributionArea> GetDistributionAreasByRegionId(int RegionId, bool showHidden = false);

        /// <summary>
        /// Gets all distribution areas
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>distribution area collection</returns>
        IList<DistributionArea> GetDistributionAreas(bool showHidden = false);

        /// <summary>
        /// Inserts a distribution area
        /// </summary>
        /// <param name="distributionArea">distribution area</param>
        void InsertDistributionArea(DistributionArea distributionArea);

        /// <summary>
        /// Updates a distribution area
        /// </summary>
        /// <param name="distributionArea">distribution area</param>
        void UpdateDistributionArea(DistributionArea distributionArea);
    }
}
