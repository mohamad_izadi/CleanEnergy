using System.Collections.Generic;
using Energy.Core.Domain.Directory;

namespace Energy.Services.Directory
{
    /// <summary>
    /// Country service interface
    /// </summary>
    public partial interface IStateService
    {
        void DeleteState(State state);
        IList<State> GetAllStates(bool showHidden = false);
        State GetStateById(int stateId);
        IList<State> GetStatesByIds(int[] stateIds);
        void InsertState(State state);
        void UpdateState(State state);
    }
}