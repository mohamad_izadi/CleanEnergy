using System;
using System.Collections.Generic;
using System.Linq;
using Energy.Core;
using Energy.Core.Caching;
using Energy.Core.Data;
using Energy.Core.Domain.Directory;
using Energy.Core.Domain.Stores;
using Energy.Services.Events;

namespace Energy.Services.Directory
{
    public partial class StateService : IStateService
    {
        #region Constants

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : show hidden records?
        /// </remarks>
        private const string COUNTRIES_ALL_KEY = "Nop.state.all-{0}";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string COUNTRIES_PATTERN_KEY = "Nop.state.";

        #endregion
        
        #region Fields

        private readonly IRepository<State> _stateRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;

        #endregion

        #region Ctor
        public StateService(ICacheManager cacheManager,
            IRepository<State> stateRepository,
            IEventPublisher eventPublisher)
        {
            this._cacheManager = cacheManager;
            this._stateRepository = stateRepository;
            this._eventPublisher = eventPublisher;
        }

        #endregion

        #region Methods

        public virtual void DeleteState(State state)
        {
            if (state == null)
                throw new ArgumentNullException("state");

            _stateRepository.Delete(state);

            _cacheManager.RemoveByPattern(COUNTRIES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityDeleted(state);
        }
        public virtual IList<State> GetAllStates(bool showHidden = false)
        {
            string key = string.Format(COUNTRIES_ALL_KEY, showHidden);
            return _cacheManager.Get(key, () =>
            {
                var query = _stateRepository.Table;
                if (!showHidden)
                    query = query.Where(c => c.Published);
                query = query.OrderBy(c => c.DisplayOrder).ThenBy(c => c.Name);

                if (!showHidden)
                {
                    //only distinct entities (group by ID)
                    query = from c in query
                            group c by c.Id
                                into cGroup
                                orderby cGroup.Key
                                select cGroup.FirstOrDefault();
                    query = query.OrderBy(c => c.DisplayOrder).ThenBy(c => c.Name);
                }

                var countries = query.ToList();
                return countries;
            });
        }
        public virtual State GetStateById(int stateId)
        {
            if (stateId == 0)
                return null;

            return _stateRepository.GetById(stateId);
        }
        public virtual IList<State> GetStatesByIds(int[] stateIds)
        {
            if (stateIds == null || stateIds.Length == 0)
                return new List<State>();

            var query = from c in _stateRepository.Table
                        where stateIds.Contains(c.Id)
                        select c;
            var states = query.ToList();
            //sort by passed identifiers
            var sortedStates = new List<State>();
            foreach (int id in stateIds)
            {
                var state = states.Find(x => x.Id == id);
                if (state != null)
                    sortedStates.Add(state);
            }
            return sortedStates;
        }
        public virtual void InsertState(State state)
        {
            if (state == null)
                throw new ArgumentNullException("state");

            _stateRepository.Insert(state);

            _cacheManager.RemoveByPattern(COUNTRIES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(state);
        }
        public virtual void UpdateState(State state)
        {
            if (state == null)
                throw new ArgumentNullException("state");

            _stateRepository.Update(state);

            _cacheManager.RemoveByPattern(COUNTRIES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(state);
        }

        #endregion
    }
}