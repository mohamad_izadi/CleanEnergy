using System.Collections.Generic;
using Energy.Core.Domain.Directory;

namespace Energy.Services.Directory
{
    /// <summary>
    /// Region service interface
    /// </summary>
    public partial interface IRegionService
    {
        /// <summary>
        /// Deletes a region
        /// </summary>
        /// <param name="region">Region</param>
        void DeleteRegion(Region Region);

        /// <summary>
        /// Gets all regions
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>regions</returns>
        IList<Region> GetAllRegions(bool showHidden = false);

        /// <summary>
        /// Gets a region 
        /// </summary>
        /// <param name="RegionId">Region identifier</param>
        /// <returns>Region</returns>
        Region GetRegionById(int regionId);

        /// <summary>
        /// Get regions by identifiers
        /// </summary>
        /// <param name="RegionIds">Region identifiers</param>
        /// <returns>Regions</returns>
        IList<Region> GetRegionsByIds(int[] regionIds);

        /// <summary>
        /// Inserts a Region
        /// </summary>
        /// <param name="region">Region</param>
        void InsertRegion(Region region);

        /// <summary>
        /// Updates the Region
        /// </summary>
        /// <param name="region">Region</param>
        void UpdateRegion(Region region);
    }
}