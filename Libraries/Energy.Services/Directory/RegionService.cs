using System;
using System.Collections.Generic;
using System.Linq;
using Energy.Core;
using Energy.Core.Caching;
using Energy.Core.Data;
using Energy.Core.Domain.Directory;
using Energy.Core.Domain.Stores;
using Energy.Services.Events;

namespace Energy.Services.Directory
{
    /// <summary>
    /// Country service
    /// </summary>
    public partial class RegionService : IRegionService
    {
        #region Constants

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : show hidden records?
        /// </remarks>
        private const string COUNTRIES_ALL_KEY = "Nop.country.all-{0}";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string COUNTRIES_PATTERN_KEY = "Nop.country.";

        #endregion
        
        #region Fields

        private readonly IRepository<Region> _regionRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// /// <param name="cacheManager">Cache manager</param>
        /// <param name="regionRepository">Region repository</param>
        /// <param name="eventPublisher">Event published</param>
        public RegionService(ICacheManager cacheManager,
            IRepository<Region> regionRepository,
            IEventPublisher eventPublisher)
        {
            this._cacheManager = cacheManager;
            this._regionRepository = regionRepository;
            this._eventPublisher = eventPublisher;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Deletes a region
        /// </summary>
        /// <param name="region">Region</param>
        public virtual void DeleteRegion(Region region)
        {
            if (region == null)
                throw new ArgumentNullException("region");

            _regionRepository.Delete(region);

            _cacheManager.RemoveByPattern(COUNTRIES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityDeleted(region);
        }

        /// <summary>
        /// Gets all regions
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Regions</returns>
        public virtual IList<Region> GetAllRegions(bool showHidden = false)
        {
            string key = string.Format(COUNTRIES_ALL_KEY, showHidden);
            return _cacheManager.Get(key, () =>
            {
                var query = _regionRepository.Table;
                if (!showHidden)
                    query = query.Where(c => c.Published);
                query = query.OrderBy(c => c.DisplayOrder).ThenBy(c => c.Name);

                if (!showHidden)
                {
                    //only distinct entities (group by ID)
                    query = from c in query
                            group c by c.Id
                                into cGroup
                                orderby cGroup.Key
                                select cGroup.FirstOrDefault();
                    query = query.OrderBy(c => c.DisplayOrder).ThenBy(c => c.Name);
                }

                var countries = query.ToList();
                return countries;
            });
        }


        /// <summary>
        /// Gets a region 
        /// </summary>
        /// <param name="regionId">Region identifier</param>
        /// <returns>Region</returns>
        public virtual Region GetRegionById(int regionId)
        {
            if (regionId == 0)
                return null;

            return _regionRepository.GetById(regionId);
        }

        /// <summary>
        /// Get countries by identifiers
        /// </summary>
        /// <param name="countryIds">Country identifiers</param>
        /// <returns>Countries</returns>
        public virtual IList<Region> GetRegionsByIds(int[] regionIds)
        {
            if (regionIds == null || regionIds.Length == 0)
                return new List<Region>();

            var query = from c in _regionRepository.Table
                        where regionIds.Contains(c.Id)
                        select c;
            var regions = query.ToList();
            //sort by passed identifiers
            var sortedRegions = new List<Region>();
            foreach (int id in regionIds)
            {
                var region = regions.Find(x => x.Id == id);
                if (region != null)
                    sortedRegions.Add(region);
            }
            return sortedRegions;
        }


        /// <summary>
        /// Inserts a region
        /// </summary>
        /// <param name="region">Region</param>
        public virtual void InsertRegion(Region region)
        {
            if (region == null)
                throw new ArgumentNullException("region");

            _regionRepository.Insert(region);

            _cacheManager.RemoveByPattern(COUNTRIES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(region);
        }

        /// <summary>
        /// Updates the region
        /// </summary>
        /// <param name="region">Region</param>
        public virtual void UpdateRegion(Region region)
        {
            if (region == null)
                throw new ArgumentNullException("region");

            _regionRepository.Update(region);

            _cacheManager.RemoveByPattern(COUNTRIES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(region);
        }

        #endregion
    }
}