﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Energy.Data;
using Energy.Core;
using Energy.Core.Data;
using Energy.Core.Domain.Bills;
using Energy.Core.Domain.Splits;
using Energy.Core.Domain.Tariffs;

namespace Energy.Services.Tariffs
{
    public class TariffService : ITariffService
    {
        #region fields

        public IRepository<TariffArea> _tariffAreaRepository;
        public IRepository<Tariff> _tariffRepository;
        public IRepository<TariffRate> _tariffRateRepository;
        public IRepository<SplitArea> _splitAreaRepository;

        #endregion

        #region Ctr
        public TariffService(
            IRepository<TariffArea> tariffAreaRepository,
            IRepository<Tariff> tariffRepository,
            IRepository<TariffRate> tariffRateRepository,
            IRepository<SplitArea> splitAreaRepository)
        {
            this._tariffAreaRepository = tariffAreaRepository;
            this._tariffRepository = tariffRepository;
            this._tariffRateRepository = tariffRateRepository;
            this._splitAreaRepository = splitAreaRepository;
        }

        #endregion

        public SplitArea GetTariffBySplitId(int splitId)
        {
            var query = (from sa in _splitAreaRepository.Table
                         where sa.SplitID == splitId
                         join ta in _tariffAreaRepository.Table on sa.TariffAreaID equals ta.Id
                         join t in _tariffRepository.Table on ta.Tariff_Id equals t.Id
                         select sa).FirstOrDefault();

            return query;

        }


        public TariffArea getTariffAreaById(int taId)
        {
            var query = _tariffAreaRepository.GetById(taId);

            return query;
        }


        public IList<SplitArea> getNormalTariffBySplitId(int splitId)
        {
            var query = (from sa in _splitAreaRepository.Table
                         where sa.TariffAreaID != 1 && sa.TariffAreaID != 83
                         join ta in _tariffAreaRepository.Table on sa.TariffAreaID equals ta.Id
                         join t in _tariffRepository.Table on ta.Tariff_Id equals t.Id
                         where sa.SplitID == splitId
                         select sa).ToList();

            return query;
        }

        public Tariff GetById(int Id)
        {
            var query = _tariffRepository.GetById(Id);

            return query;
        }


        public IList<TariffRate> getTariffRate(int tariffId, double used)
        {
            var query = (from tr in _tariffRateRepository.Table
                         where tr.TariffID == tariffId && tr.FromKwhm < used
                         select tr).ToList();

            return query;
        }


        public IPagedList<TariffArea> getAllTarrifArea(int CTariff, int pageIndex = 0, int pageSize = 2147483647)
        {
            var query = _tariffAreaRepository.Table.Include("Tariff");

            if (CTariff > 0)
                query = query.Where(t => t.Tariff.CTariff == CTariff);

            query = query.OrderBy(s => s.Id);

            var results = new PagedList<TariffArea>(query, pageIndex, pageSize);

            return results;
        }


        public void InsertSplitArea(SplitArea sa)
        {
            if (sa == null)
                throw new ArgumentNullException("sa");

            _splitAreaRepository.Insert(sa);
        }

        public void UpdateSplitArea(SplitArea sa)
        {
            if (sa == null)
                throw new ArgumentNullException("sa");

            _splitAreaRepository.Update(sa);
        }

        public void DeleteSplitArea(SplitArea sa)
        {
            if (sa == null)
                throw new ArgumentNullException("sa");

            _splitAreaRepository.Delete(sa);
        }

        public IList<SplitArea> GetSplitAreasBySplit(int splitId)
        {
            var query = from sa in _splitAreaRepository.Table
                        where sa.SplitID == splitId
                        select sa;

            return query.ToList();
        }


        public void UpdateTariffArea(TariffArea ta)
        {
            if (ta == null)
                throw new ArgumentNullException("ta");

            _tariffAreaRepository.Update(ta);
        }


        public IPagedList<Tariff> getAllTarrif(int CTariff = 0, int pageIndex = 0, int pageSize = 2147483647)
        {
            var query = _tariffRepository.Table;

            if (CTariff > 0)
                query = query.Where(t => t.CTariff == CTariff);

            query = query.OrderBy(s => s.Id);

            var results = new PagedList<Tariff>(query, pageIndex, pageSize);

            return results;
        }


        public void UpdateTariff(Tariff t)
        {
            if (t == null)
                throw new ArgumentNullException("t");

            _tariffRepository.Update(t);
        }


        public IPagedList<TariffRate> getAllTarrifNormal(int CTariff = 0, int pageIndex = 0, int pageSize = 2147483647)
        {
            var query = _tariffRateRepository.Table;

            if (CTariff > 0)
                if (CTariff == 1)
                    query = query.Where(t => t.TariffID == 1 || t.TariffID == 2 || t.TariffID == 3 || t.TariffID == 4 || t.TariffID == 5);
                else if (CTariff == 5)
                    query = query.Where(t => t.TariffID == 7 || t.TariffID == 8);

            query = query.OrderBy(s => s.Id);

            var results = new PagedList<TariffRate>(query, pageIndex, pageSize);

            return results;
        }
    }
}
