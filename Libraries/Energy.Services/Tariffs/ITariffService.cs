﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Energy.Data;
using Energy.Core;
using Energy.Core.Data;
using Energy.Core.Domain.Bills;
using Energy.Core.Domain.Splits;
using Energy.Core.Domain.Tariffs;

namespace Energy.Services.Tariffs
{
    public partial interface ITariffService
    {


        SplitArea GetTariffBySplitId(int splitId);

        TariffArea getTariffAreaById(int taId);

        IList<SplitArea> getNormalTariffBySplitId(int splitId);

        IList<TariffRate> getTariffRate(int tariffId, double used);

        IList<SplitArea> GetSplitAreasBySplit(int splitId);

        //new structure
        Tariff GetById(int Id);
        IPagedList<TariffArea> getAllTarrifArea(int CTariff = 0,
            int pageIndex = 0, int pageSize = 2147483647);

        IPagedList<Tariff> getAllTarrif(int CTariff = 0,
            int pageIndex = 0, int pageSize = 2147483647);

        IPagedList<TariffRate> getAllTarrifNormal(int CTariff = 0,
            int pageIndex = 0, int pageSize = 2147483647);

        void InsertSplitArea(SplitArea sa);
        void UpdateSplitArea(SplitArea sa);
        void DeleteSplitArea(SplitArea sa);

        void UpdateTariffArea(TariffArea ta);

        void UpdateTariff(Tariff t);
    }
}
