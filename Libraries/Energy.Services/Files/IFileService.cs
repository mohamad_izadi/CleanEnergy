﻿using System.Collections.Generic;
using Energy.Core;
using Energy.Core.Domain.Files;

namespace Energy.Services.Files
{
    /// <summary>
    /// File service interface
    /// </summary>
    public partial interface IFileService
    {

        string GetFileUrl(int fileId,
            string fileExtension,
            byte[] fileBinary,
            string storeLocation = null);

        string GetFileUrl(File file,
            string fileExtension,
            byte[] fileBinary,
            string storeLocation = null);

        string GetFilePath(int fileId);

        File GetFileById(int fileId);

        void DeleteFile(File file);

        File InsertFile(byte[] fileBinary, string fileExtension, string seoFilename, bool validateBinary = true);

        File SetFilename(int fileId, string seoFilename);

        IList<File> GetFilesByRequestId(int requst, int recordsToReturn = 0);

        string GetFileDescription(int fileId);
    }
}
