﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Energy.Core;
using Energy.Core.Data;
using Energy.Core.Domain.Files;
using Energy.Core.Domain.Bills;
using Energy.Core.Domain.Requests;
using Energy.Services.Configuration;
using Energy.Services.Events;
using Energy.Services.Logging;
using Energy.Services.Seo;

namespace Energy.Services.Files
{
    public class FileService : IFileService
    {
        #region Fields

        private static readonly object s_lock = new object();

        private readonly IRepository<File> _fileRepository;
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;
        private readonly ILogger _logger;
        private readonly IEventPublisher _eventPublisher;
        private readonly FileSetting _fileSettings;
        private readonly IRepository<RequestFile> _requestFileRepository;

        #endregion

        #region Ctor
        public FileService(IRepository<File> fileRepository,
            ISettingService settingService,
            IWebHelper webHelper,
            ILogger logger,
            IEventPublisher eventPublisher,
            FileSetting fileSettings,
            IRepository<RequestFile> requestFileRepository)
        {
            this._fileRepository = fileRepository;
            this._settingService = settingService;
            this._webHelper = webHelper;
            this._logger = logger;
            this._eventPublisher = eventPublisher;
            this._fileSettings = fileSettings;
            this._requestFileRepository = requestFileRepository;
        }

        #endregion

        #region Utilities

        protected virtual string GetAttachmentLocalPath(string FileName)
        {
            var filesDirectoryPath = _webHelper.MapPath("~/content/files");
            //var filesDirectoryPath = _fileSettings.LocalPath;

            var thumbFilePath = System.IO.Path.Combine(filesDirectoryPath, FileName);
            return thumbFilePath;
        }

        protected virtual string GetAttachmentUrl(string FileName, string storeLocation = null)
        {
            storeLocation = !String.IsNullOrEmpty(storeLocation)
                                    ? storeLocation
                                    : _webHelper.GetStoreLocation();
            var url = storeLocation + "content/files/";

            url = url + FileName;
            return url;
        }

        #endregion

        #region Getting file local path/URL methods

        public string GetFileUrl(int fileId, string fileExtension, byte[] fileBinary, string storeLocation = null)
        {
            var file = GetFileById(fileId);
            return GetFileUrl(file, fileExtension, fileBinary, storeLocation);
        }

        public string GetFileUrl(File file, string fileExtension, byte[] fileBinary, string storeLocation = null)
        {
            string url = string.Empty;
            string lastPart = fileExtension;
            string FileName;

            lock (s_lock)
            {
                string seoFileName = file.FileName;

                FileName = !String.IsNullOrEmpty(seoFileName) ?
                    string.Format("{0}_{1}.{2}", file.Id.ToString("0000000"), seoFileName, lastPart) :
                    string.Format("{0}.{1}", file.Id.ToString("0000000"), lastPart);
                var FilePath = GetAttachmentLocalPath(FileName);
                if (!System.IO.File.Exists(FilePath))
                {
                    System.IO.File.WriteAllBytes(FilePath, fileBinary);
                }
            }
            url = GetAttachmentUrl(FileName, storeLocation);
            return url;
        }

        public string GetFilePath(int fileId)
        {
            var file = GetFileById(fileId);
            return GetFilePath(file);
        }

        public string GetFilePath(File file)
        {
            string url = string.Empty;
            string lastPart = file.FileType;
            string FileName;

            string seoFileName = file.FileName;

            FileName = !String.IsNullOrEmpty(seoFileName) ?
                string.Format("{0}_{1}.{2}", file.Id.ToString("0000000"), seoFileName, lastPart) :
                string.Format("{0}.{1}", file.Id.ToString("0000000"), lastPart);

            return GetAttachmentLocalPath(FileName);
        }

        public IList<File> GetFilesByRequestId(int requestId, int recordsToReturn = 0)
        {
            if (requestId == 0)
                return new List<File>();

            var query = from f in _fileRepository.Table
                        join rf in _requestFileRepository.Table on f.Id equals rf.File_Id
                        where rf.Request_Id == requestId
                        select f;

            if (recordsToReturn > 0)
                query = query.Take(recordsToReturn);

            var files = query.ToList();
            return files;
        }

        public string GetFileDescription(int fileId)
        {
            var query = (from rf in _requestFileRepository.Table
                         where rf.File_Id == fileId
                         select rf.Description).FirstOrDefault();

            return query;
        }

        #endregion

        #region CRUD methods

        public File GetFileById(int fileId)
        {
            if (fileId == 0)
                return null;

            return _fileRepository.GetById(fileId);
        }

        public File InsertFile(byte[] fileBinary, string fileExtension, string fileName, bool validateBinary = true)
        {
            fileExtension = CommonHelper.EnsureNotNull(fileExtension);
            fileExtension = CommonHelper.EnsureMaximumLength(fileExtension, 20);

            fileName = CommonHelper.EnsureMaximumLength(fileName, 100);

            var file = new File
            {
                FileType = fileExtension,
                FileName = fileName.Substring(0,fileName.LastIndexOf('.'))
            };

            _fileRepository.Insert(file);

            //event notification
            _eventPublisher.EntityInserted(file);

            return file;
        }


        public File SetFilename(int fileId, string seoFilename)
        {
            throw new NotImplementedException();
        }


        public void DeleteFile(File file)
        {
            //if (file == null)
            //    throw new ArgumentNullException("picture");

            ////delete thumbs
            //DeletePictureThumbs(picture);

            ////delete from file system
            //if (!this.StoreInDb)
            //    DeletePictureOnFileSystem(picture);

            ////delete from database
            //_pictureRepository.Delete(picture);

            ////event notification
            //_eventPublisher.EntityDeleted(picture);
        }

        //public IList<File> GetFilesByDocumentId(int documentId, int recordsToReturn = 0)
        //{
        //    if (documentId == 0)
        //        return new List<File>();

        //    var query = from f in _fileRepository.Table
        //                join df in _documentFileRepository.Table on f.Id equals df.FileId
        //                orderby df.DisplayOrder
        //                where df.DocumentId == documentId
        //                select f;

        //    if (recordsToReturn > 0)
        //        query = query.Take(recordsToReturn);

        //    var files = query.ToList();
        //    return files;
        //}

        //public string GetFileDescription(int fileId)
        //{
        //    var query = (from df in _documentFileRepository.Table
        //                 where df.FileId == fileId
        //                 select df.Description).FirstOrDefault();

        //    return query;
        //}

        #endregion
    }
}
