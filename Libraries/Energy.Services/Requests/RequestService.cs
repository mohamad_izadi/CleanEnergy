﻿using System.Collections.Generic;
using Energy.Core.Domain.Requests;
using System;
using System.Linq;
using Energy.Core.Data;
using Energy.Services.Events;
using Energy.Core.Caching;
using Energy.Core;

namespace Energy.Services.Requests
{
    public partial class RequestService : IRequestService
    {
        #region Constants

        /// <summary>
        /// Key for caching
        /// </summary>
        private const string REQUESTTYPE_ALL_KEY = "energy.requesttype.all";

        #endregion

        #region Fields

        private readonly IRepository<Request> _requestRepository;
        private readonly IRepository<RequestFile> _requestFileRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;

        #endregion

        #region Ctor

        public RequestService(ICacheManager cacheManager,
            IRepository<Request> requestRepository,
            IRepository<RequestFile> requestFileRepository,
            IEventPublisher eventPublisher)
        {
            this._cacheManager = cacheManager;
            this._requestRepository = requestRepository;
            this._requestFileRepository = requestFileRepository;
            this._eventPublisher = eventPublisher;
        }

        #endregion

        #region Nested classes

        [Serializable]
        public class RequestTypeForCaching
        {
            public int Id { get; set; }
            public string SystemKeyword { get; set; }
            public string Name { get; set; }
            public bool Enabled { get; set; }
        }

        #endregion

        #region request
        public Request GetRequestById(int requestId)
        {
            if (requestId == 0)
                return null;

            return _requestRepository.GetById(requestId);
        }

        public virtual IPagedList<Request> GetAllRequests(DateTime? createdOnFrom,
             DateTime? createdOnTo, int? customerId, int RequestTypeId,
             int pageIndex, int pageSize)
        {
            var query = _requestRepository.Table;
            if (createdOnFrom.HasValue)
                query = query.Where(al => createdOnFrom.Value <= al.CreateDate);
            if (createdOnTo.HasValue)
                query = query.Where(al => createdOnTo.Value >= al.CreateDate);
            if (RequestTypeId > 0)
                query = query.Where(al => RequestTypeId == al.RequestType_Id);
            if (customerId.HasValue)
                query = query.Where(al => customerId.Value == al.Customer_Id);

            query = query.OrderByDescending(al => al.CreateDate);

            var request = new PagedList<Request>(query, pageIndex, pageSize);
            return request;
        }

        public void DeleteRequest(Request request)
        {
            throw new System.NotImplementedException();
        }
        public void InsertRequest(Request request)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            _requestRepository.Insert(request);

            //event notification
            _eventPublisher.EntityInserted(request);
        }
        public void UpdateRequest(Request request)
        {
            throw new System.NotImplementedException();
        }

        #endregion

        #region requestFiles

        public void InsertRequestFile(RequestFile requestFile)
        {
            if (requestFile == null)
                throw new ArgumentNullException("requestFile");

            _requestFileRepository.Insert(requestFile);

            //event notification
            _eventPublisher.EntityInserted(requestFile);
        }

        public IList<RequestFile> GetRequestFilesByrequestId(int requestId)
        {
            var query = from rf in _requestFileRepository.Table
                        where rf.Request_Id == requestId
                        select rf;
            var requestFiles = query.ToList();
            return requestFiles;
        }

        public RequestFile GetRequestFileById(int requestFileId)
        {
            if (requestFileId == 0)
                return null;

            return _requestFileRepository.GetById(requestFileId);
        }

        public void DeleteRequestFile(RequestFile requestFile)
        {
            if (requestFile == null)
                throw new ArgumentNullException("requestFile");

            _requestFileRepository.Delete(requestFile);

            //event notification
            _eventPublisher.EntityDeleted(requestFile);
        }

        #endregion



        
    }
}
