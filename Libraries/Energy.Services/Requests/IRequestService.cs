﻿using Energy.Core;
using Energy.Core.Domain.Requests;
using System;
using System.Collections.Generic;

namespace Energy.Services.Requests
{
    public partial interface IRequestService
    {
        
        Request GetRequestById(int requestId);
        void DeleteRequest(Request request);
        void InsertRequest(Request request);
        void UpdateRequest(Request request);
        void InsertRequestFile(RequestFile requestFile);
        IList<RequestFile> GetRequestFilesByrequestId(int requestId);
        RequestFile GetRequestFileById(int requestFileId);
        void DeleteRequestFile(RequestFile requestFile);
        IPagedList<Request> GetAllRequests(DateTime? createdOnFrom,
             DateTime? createdOnTo, int? customerId, int RequestTypeId,
             int pageIndex, int pageSize);
    }
}
