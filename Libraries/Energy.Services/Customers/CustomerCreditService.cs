﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Energy.Core;
using Energy.Core.Data;
using Energy.Core.Domain.Common;
using Energy.Core.Domain.Customers;
using Energy.Core.Caching;
using Energy.Data;


namespace Energy.Services.Customers
{
    public partial class CustomerCreditService : ICustomerCreditService
    {
        #region Constants

        private const string CUSTOMERCREDITS_BY_CODE_KEY = "Energy.customercredit.code-{0}";

        #endregion

        #region Fields

        private readonly IRepository<CustomerCredit> _customerCreditRepository;
        private readonly ICacheManager _cacheManager;
        private readonly IDbContext _dbContext;

        #endregion

        #region Ctor

        public CustomerCreditService(
            IRepository<CustomerCredit> customerCreditRepository,
            ICacheManager cacheManager,
            IDbContext dbContext)
        {
            this._customerCreditRepository = customerCreditRepository;
            this._cacheManager = cacheManager;
            this._dbContext = dbContext;
        }

        #endregion

        #region Methods

        public virtual CustomerCredit GetCustomerCreditById(int customerCreditId)
        {
            if (customerCreditId == 0)
                return null;

            return _customerCreditRepository.GetById(customerCreditId);
        }

        public IPagedList<CustomerCredit> GetAllCustomerCredits(
            string Guid = "",
            int pageIndex = 0,
            int pageSize = int.MaxValue,
            GeneralSortingEnum orderBy = GeneralSortingEnum.Default)
        {
            var query = _customerCreditRepository.Table;

            if (!String.IsNullOrWhiteSpace(Guid))
                query = query.Where(m => m.Code.ToString().Contains(Guid));

            //only distinct items (group by ID)
            query = from n in query
                    group n by n.Id
                        into nGroup
                        orderby nGroup.Key
                        select nGroup.FirstOrDefault();

            query = query.OrderByDescending(n => n.Id);

            var credits = new PagedList<CustomerCredit>(query, pageIndex, pageSize);
            return credits;
        }

        public virtual CustomerCredit GetCustomerCreditByCode(string code)
        {
            if (String.IsNullOrWhiteSpace(code))
                return null;

            string key = string.Format(CUSTOMERCREDITS_BY_CODE_KEY, code);
            return _cacheManager.Get(key, () =>
            {
                var query = from cc in _customerCreditRepository.Table
                            orderby cc.Id
                            where cc.Code.ToString().Equals(code, StringComparison.InvariantCultureIgnoreCase)
                            select cc;
                var customerCredit = query.FirstOrDefault();
                return customerCredit;
            });
        }

        public virtual CustomerCredit InsertGiftCustomerCredit()
        {
            var customerCredit = new CustomerCredit()
            {
                Code = Guid.NewGuid(),
                DownloadCredit = 10,
                IsGift = true                
            };
            _customerCreditRepository.Insert(customerCredit);

            return customerCredit;
        }

        public void InsertCustomerCredit(CustomerCredit customerCredit)
        {
            if (customerCredit == null)
                throw new ArgumentNullException("customerCredit");

            customerCredit.Code = Guid.NewGuid();
            customerCredit.IsGift = false;

            _customerCreditRepository.Insert(customerCredit);
        }

        public IEnumerable<UsedCustomerCredit> GetUsedCredits(IList<int> ids = null)
        {
            var where = "";
            if (ids != null && ids.Count > 0)
            {
                var distict = ids.Distinct().ToList();
                where = string.Join(",", distict);
                where = "WHERE t1.Id in (" + where + ")";
            }

            return _dbContext.SqlQuery<UsedCustomerCredit>(string.Format(@"select t1.Id, case when exists
                    (select 1
                     from [Nop_DB].[dbo].[Customer_CustomerCredit_Mapping] as t2
                     where t2.CustomerCredit_Id = t1.Id)
                     then convert(bit,1) else convert(bit,0) end as IsUsed
                     from [Nop_DB].[dbo].[CustomerCredit] As t1 
                     {0}", where));
        }

        public IList<CustomerCredit> GetCustomerCreditsByIds(int[] creditIds)
        {
            if (creditIds == null || creditIds.Length == 0)
                return new List<CustomerCredit>();

            var query = from c in _customerCreditRepository.Table
                        where creditIds.Contains(c.Id)
                        select c;
            var credits = query.ToList();
            //sort by passed identifiers
            var sortedCredits = new List<CustomerCredit>();
            foreach (int id in creditIds)
            {
                var credit = credits.Find(x => x.Id == id);
                if (credit != null)
                    sortedCredits.Add(credit);
            }
            return sortedCredits;
        }

        #endregion
    }
}
