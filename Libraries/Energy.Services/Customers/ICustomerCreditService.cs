﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Energy.Core;
using Energy.Core.Domain.Common;
using Energy.Core.Domain.Customers;

namespace Energy.Services.Customers
{
    public partial interface ICustomerCreditService
    {
        CustomerCredit GetCustomerCreditById(int customerCreditId);
        IPagedList<CustomerCredit> GetAllCustomerCredits(string Guid = "",
            int pageIndex = 0, int pageSize = int.MaxValue,
            GeneralSortingEnum orderBy = GeneralSortingEnum.Default);
        CustomerCredit GetCustomerCreditByCode(string code);
        CustomerCredit InsertGiftCustomerCredit();
        void InsertCustomerCredit(CustomerCredit customerCredit);
        IEnumerable<UsedCustomerCredit> GetUsedCredits(IList<int> ids = null);
        IList<CustomerCredit> GetCustomerCreditsByIds(int[] creditIds);
    }
}
