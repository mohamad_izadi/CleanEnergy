﻿using Energy.Services.ImenSms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Energy.Services.Messages
{
    public partial class SMSService : ISMSService
    {
        public int SendSMS(string mobileNo, string mstg)
        {
            if (string.IsNullOrEmpty(mobileNo) || string.IsNullOrEmpty(mstg))
            {
                return 2;
            }
            else
            {
                try
                {
                    var proxy = new SMSServiceSoapClient();
                    var mob = mobileNo.Replace("-", "").Replace(" ", "");

                    var result = proxy.SendOneSMS(mob, mstg, @"jcffaWECPv0=");
                    return result;
                }
                catch (Exception e)
                {
                    return -1;
                }
            }
        }
    }
}
