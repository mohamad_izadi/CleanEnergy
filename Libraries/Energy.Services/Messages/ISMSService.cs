﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Energy.Services.Messages
{
    public partial interface ISMSService
    {
        int SendSMS(string mobileNo, string mstg);
    }
}
