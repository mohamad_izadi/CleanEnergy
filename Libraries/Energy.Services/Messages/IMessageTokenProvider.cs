﻿using System.Collections.Generic;
using Energy.Core.Domain.Customers;
using Energy.Core.Domain.Messages;
using Energy.Core.Domain.News;
using Energy.Core.Domain.Stores;

namespace Energy.Services.Messages
{
    public partial interface IMessageTokenProvider
    {
        void AddStoreTokens(IList<Token> tokens, Store store, EmailAccount emailAccount);

        void AddCustomerTokens(IList<Token> tokens, Customer customer);

        void AddNewsLetterSubscriptionTokens(IList<Token> tokens, NewsLetterSubscription subscription);

        void AddNewsCommentTokens(IList<Token> tokens, NewsComment newsComment);

        string[] GetListOfCampaignAllowedTokens();

        string[] GetListOfAllowedTokens();
    }
}
