using System.Collections.Generic;
using Energy.Core.Domain.Customers;
using Energy.Core.Domain.Security;

namespace Energy.Services.Security
{
    /// <summary>
    /// Standard permission provider
    /// </summary>
    public partial class StandardPermissionProvider : IPermissionProvider
    {
        //admin area permissions
        public static readonly PermissionRecord AccessAdminPanel = new PermissionRecord { Name = "Access admin area", SystemName = "AccessAdminPanel", Category = "Standard" };
        //public static readonly PermissionRecord AllowCustomerImpersonation = new PermissionRecord { Name = "Admin area. Allow Customer Impersonation", SystemName = "AllowCustomerImpersonation", Category = "Customers" };
        public static readonly PermissionRecord ManageCustomers = new PermissionRecord { Name = "Admin area. Manage Customers", SystemName = "ManageCustomers", Category = "Customers" };
        //public static readonly PermissionRecord ManageNewsletterSubscribers = new PermissionRecord { Name = "Admin area. Manage Newsletter Subscribers", SystemName = "ManageNewsletterSubscribers", Category = "Promo" };
        public static readonly PermissionRecord ManageNews = new PermissionRecord { Name = "Admin area. Manage News", SystemName = "ManageNews", Category = "Content Management" };
        public static readonly PermissionRecord ManageWidgets = new PermissionRecord { Name = "Admin area. Manage Widgets", SystemName = "ManageWidgets", Category = "Content Management" };
        public static readonly PermissionRecord ManageTopics = new PermissionRecord { Name = "Admin area. Manage Topics", SystemName = "ManageTopics", Category = "Content Management" };
        public static readonly PermissionRecord ManageMessageTemplates = new PermissionRecord { Name = "Admin area. Manage Message Templates", SystemName = "ManageMessageTemplates", Category = "Content Management" };
        public static readonly PermissionRecord ManageRegions = new PermissionRecord { Name = "Admin area. Manage Regions", SystemName = "ManageRegions", Category = "Configuration" };
        public static readonly PermissionRecord ManageRequests = new PermissionRecord { Name = "Admin area. Manage Requests", SystemName = "ManageRequests", Category = "Requests" };
        public static readonly PermissionRecord SendRequests = new PermissionRecord { Name = "Admin area. Send Requests", SystemName = "SendRequests", Category = "Requests" };
        //public static readonly PermissionRecord ManageLanguages = new PermissionRecord { Name = "Admin area. Manage Languages", SystemName = "ManageLanguages", Category = "Configuration" };
        //public static readonly PermissionRecord ManageSettings = new PermissionRecord { Name = "Admin area. Manage Settings", SystemName = "ManageSettings", Category = "Configuration" };
        //public static readonly PermissionRecord ManageExternalAuthenticationMethods = new PermissionRecord { Name = "Admin area. Manage External Authentication Methods", SystemName = "ManageExternalAuthenticationMethods", Category = "Configuration" };
        public static readonly PermissionRecord ManageActivityLog = new PermissionRecord { Name = "Admin area. Manage Activity Log", SystemName = "ManageActivityLog", Category = "Configuration" };
        public static readonly PermissionRecord ManageAcl = new PermissionRecord { Name = "Admin area. Manage ACL", SystemName = "ManageACL", Category = "Configuration" };
        public static readonly PermissionRecord ManageEmailAccounts = new PermissionRecord { Name = "Admin area. Manage Email Accounts", SystemName = "ManageEmailAccounts", Category = "Configuration" };
        //public static readonly PermissionRecord ManageStores = new PermissionRecord { Name = "Admin area. Manage Stores", SystemName = "ManageStores", Category = "Configuration" };
        //public static readonly PermissionRecord ManagePlugins = new PermissionRecord { Name = "Admin area. Manage Plugins", SystemName = "ManagePlugins", Category = "Configuration" };
        //public static readonly PermissionRecord ManageSystemLog = new PermissionRecord { Name = "Admin area. Manage System Log", SystemName = "ManageSystemLog", Category = "Configuration" };
        public static readonly PermissionRecord ManageMessageQueue = new PermissionRecord { Name = "Admin area. Manage Message Queue", SystemName = "ManageMessageQueue", Category = "Configuration" };
        public static readonly PermissionRecord ManageMaintenance = new PermissionRecord { Name = "Admin area. Manage Maintenance", SystemName = "ManageMaintenance", Category = "Configuration" };
        //public static readonly PermissionRecord HtmlEditorManagePictures = new PermissionRecord { Name = "Admin area. HTML Editor. Manage pictures", SystemName = "HtmlEditor.ManagePictures", Category = "Configuration" };
        //public static readonly PermissionRecord ManageScheduleTasks = new PermissionRecord { Name = "Admin area. Manage Schedule Tasks", SystemName = "ManageScheduleTasks", Category = "Configuration" };

        public static readonly PermissionRecord ManageSplits = new PermissionRecord { Name = "Admin area. Manage Splits", SystemName = "ManageSplits", Category = "Splits" };
        public static readonly PermissionRecord ManageBills = new PermissionRecord { Name = "Admin area. Manage Bills", SystemName = "ManageBills", Category = "Bills" };
        public static readonly PermissionRecord ManageHandheld = new PermissionRecord { Name = "Admin area. Manage Handheld", SystemName = "ManageHandheld", Category = "Handheld" };
        public static readonly PermissionRecord ManageAccountant = new PermissionRecord { Name = "Admin area. Manage Accountant", SystemName = "ManageAccountant", Category = "Accountant" };
        public static readonly PermissionRecord ManageReports = new PermissionRecord { Name = "Admin area. Manage Reports", SystemName = "ManageReports", Category = "Reports" };
        public static readonly PermissionRecord ManageTariffs = new PermissionRecord { Name = "Admin area. Manage Tariffs", SystemName = "ManageTariffs", Category = "Tariffs" };
        public static readonly PermissionRecord viewBills = new PermissionRecord { Name = "Admin area. View Bills", SystemName = "ViewBills", Category = "Bills" };
        public static readonly PermissionRecord ManageMessages = new PermissionRecord { Name = "Admin area. View Messages", SystemName = "ManageMessages", Category = "Messages" };

        //public store permissions
        public static readonly PermissionRecord PublicStoreAllowNavigation = new PermissionRecord { Name = "Public store. Allow navigation", SystemName = "PublicStoreAllowNavigation", Category = "PublicStore" };


        public virtual IEnumerable<PermissionRecord> GetPermissions()
        {
            return new[] 
            {
                AccessAdminPanel,
                //AllowCustomerImpersonation,
                ManageCustomers,
                //ManageNewsletterSubscribers,
                ManageNews,
                ManageWidgets,
                ManageTopics,
                //ManageMessageTemplates,
                //ManageCountries,
                //ManageLanguages,
                //ManageSettings,
                //ManageExternalAuthenticationMethods,
                ManageActivityLog,
                ManageAcl,
                //ManageEmailAccounts,
                //ManageStores,
                //ManagePlugins,
                //ManageSystemLog,
                ManageMessageQueue,
                ManageMaintenance,
                //HtmlEditorManagePictures,
                //ManageScheduleTasks,

                ManageSplits,
                ManageBills,
                ManageHandheld,
                ManageAccountant,
                ManageReports,
                ManageTariffs,
                viewBills,
                ManageMessages,

                PublicStoreAllowNavigation,
            };
        }

        public virtual IEnumerable<DefaultPermissionRecord> GetDefaultPermissions()
        {
            return new[] 
            {
                new DefaultPermissionRecord 
                {
                    CustomerRoleSystemName = SystemCustomerRoleNames.Administrators,
                    PermissionRecords = new[] 
                    {
                        AccessAdminPanel,
                        //AllowCustomerImpersonation,
                        ManageCustomers,
                        //ManageNewsletterSubscribers,
                        ManageNews,
                        ManageWidgets,
                        ManageTopics,
                        //ManageMessageTemplates,
                        //ManageCountries,
                        //ManageLanguages,
                        //ManageSettings,
                        //ManageExternalAuthenticationMethods,
                        ManageActivityLog,
                        ManageAcl,
                        //ManageEmailAccounts,
                        //ManageStores,
                        //ManagePlugins,
                        //ManageSystemLog,
                        ManageMessageQueue,
                        ManageMaintenance,
                        //HtmlEditorManagePictures,
                        //ManageScheduleTasks,
                        
                        ManageSplits,
                        ManageBills,
                        ManageHandheld,
                        ManageAccountant,
                        ManageReports,
                        ManageTariffs,
                        viewBills,
                        ManageMessages,

                        PublicStoreAllowNavigation,
                    }
                },
                new DefaultPermissionRecord 
                {
                    CustomerRoleSystemName = SystemCustomerRoleNames.Guests,
                    PermissionRecords = new[] 
                    {
                        PublicStoreAllowNavigation,
                    }
                },
                new DefaultPermissionRecord 
                {
                    CustomerRoleSystemName = SystemCustomerRoleNames.Registered,
                    PermissionRecords = new[] 
                    {
                        PublicStoreAllowNavigation,
                    }
                }
            };
        }
    }
}