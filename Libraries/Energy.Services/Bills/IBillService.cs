﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Energy.Core.Domain.Bills;
using Energy.Core;
using Energy.Core.Domain.Tariffs;

namespace Energy.Services.Bills
{
    public partial interface IBillService
    {
        Bill getById(int billId);
        Bill getBillaSpiltById(int billId);
        Bill findPreviousBill(int billId, int splitId);
        Bill findLastBill(int splitId);
        Bill findPreviousArchiveBill(int billId, int splitId);
        IList<Bill> GetBillsBySplitId(string splitId);
        IList<Bill> GetBills_Splits(int yy, int tt);
        IList<Bill> GetBillsByDate(int yy, int tt);
        bool CheckExist(int splitId, int yy, int tt, string type);
        Bill GetBillBysyt(int splitId, int yy, int tt);
        Bill GetArchiveBillBysyt(int splitId, int yy, int tt);
        Bill GetCorrectiveBillBysyt(int splitId, int yy, int tt);
        IList<Bill> GetBills();
        void DeleteBill(Bill bill);
        void InsertBill(Bill bill);
        void UpdateBill(Bill bill);
        Bill GetBill_SplitsByBillId(int billId);
        int PaymentDelayDays(int splitId, DateTime LastPaymentDate);
        Bill getLastBill();
        IPagedList<Bill> GetAllBills(int splitId = 0,
            int year = 0, int month = 0,
            int pageIndex = 0, int pageSize = 2147483647);

        IEnumerable<Bill> GetBillsByFilter1(int? fyy, int? ftt, bool domain,
            int? toyy, int? tott,
            int? region_Id, int? distribution_Id, int? cTariff, int splitId = 0, bool includeAll = false);
    }
}
