﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Energy.Data;
using Energy.Core;
using Energy.Core.Data;
using Energy.Core.Domain.Bills;
using Energy.Core.Domain.Splits;
using Energy.Core.Domain.Constant;
using Energy.Core.Domain.Tariffs;

namespace Energy.Services.Bills
{
    public class BillService : IBillService
    {
        #region fields

        public IRepository<Bill> _billRepository;
        public IRepository<Split> _splitRepository;
        public IRepository<Tariff> _tariffRepository;
        public IRepository<BillNormal> _billNormalRepository;

        #endregion

        #region Ctor
        public BillService(
            IRepository<Bill> billRepository,
            IRepository<Split> splitRepository,
            IRepository<Tariff> tariffRepository,
            IRepository<BillNormal> billNormalRepository)
        {
            this._billRepository = billRepository;
            this._splitRepository = splitRepository;
            this._tariffRepository = tariffRepository;
            this._billNormalRepository = billNormalRepository;
        }

        #endregion

        #region Methods
        public IList<Bill> GetBillsBySplitId(string splitId)
        {
            int id = Convert.ToInt32(splitId);
            var query = from c in _billRepository.Table
                        where c.SplitID == id
                        select c;

            return query.OrderByDescending(x => x.Id).ToList();
        }
        public IList<Bill> GetBills()
        {
            var query = from c in _billRepository.Table
                        select c;

            return query.ToList();
        }
        public IList<Bill> GetBills_Splits(int yy, int tt)
        {
            var query = from b in _billRepository.Table
                        join s in _splitRepository.Table on b.SplitID equals s.Id
                        where b.Year == yy && b.Term == tt && s.Active && b.Type != "archive"
                        select b;

            return query.ToList();
        }
        public IList<Bill> GetBillsByDate(int yy, int tt)
        {
            var query = from b in _billRepository.Table
                        where b.Year == yy && b.Term == tt && b.Type != "archive"
                        select b;

            return query.ToList();
        }
        public void DeleteBill(Bill bill)
        {
            if (bill == null)
                throw new ArgumentNullException("bill");

            _billRepository.Delete(bill);
        }
        public void InsertBill(Bill bill)
        {
            if (bill == null)
                throw new ArgumentNullException("bill");

            _billRepository.Insert(bill);
        }
        public bool CheckExist(int splitId, int yy, int tt, string type)
        {
            var query = (from b in _billRepository.Table
                         where b.Year == yy && b.Term == tt && b.SplitID == splitId && b.Type == type
                         select b).ToList();

            if (query.Count == 0)
                return false;

            return true;
        }
        public Bill GetBill_SplitsByBillId(int billId)
        {
            var query = from b in _billRepository.Table
                        join s in _splitRepository.Table on b.SplitID equals s.Id
                        where b.Id == billId
                        select b;

            return query.FirstOrDefault();
        }
        public Bill findLastBill(int splitId)
        {
            var lastBillId = _billRepository.Table.Where(b => b.SplitID == splitId && b.Type != "archive")
                .Select(x => x.Id).DefaultIfEmpty(0).Max();

            return _billRepository.GetById(lastBillId);
        }
        public Bill getBillaSpiltById(int billId)
        {
            var query = (from b in _billRepository.Table
                         where b.Id == billId
                         join s in _splitRepository.Table
                         on b.SplitID equals s.Id
                         select b).FirstOrDefault();

            return query;
        }
        public Bill findPreviousArchiveBill(int billId, int splitId)
        {
            var previousArchiveBillId = _billRepository.Table.Where(b => b.SplitID == splitId && b.Type == "archive")
                .Select(x => x.Id).DefaultIfEmpty(0).Max();

            return _billRepository.GetById(previousArchiveBillId);
        }
        public void UpdateBill(Bill bill)
        {
            if (bill == null)
                throw new ArgumentNullException("bills");

            _billRepository.Update(bill);

        }
        public Bill getById(int billId)
        {
            var query = _billRepository.GetById(billId);

            return query;
        }
        public Bill GetBillBysyt(int splitId, int yy, int tt)
        {
            var query = (from b in _billRepository.Table
                         where b.Year == yy && b.Term == tt && b.SplitID == splitId && b.Type != "archive"
                         select b).FirstOrDefault();

            return query;
        }

        public Bill GetArchiveBillBysyt(int splitId, int yy, int tt)
        {
            var query = (from b in _billRepository.Table
                         where b.Year == yy && b.Term == tt && b.SplitID == splitId && b.Type == "archive"
                         select b).FirstOrDefault();

            return query;
        }

        public Bill GetCorrectiveBillBysyt(int splitId, int yy, int tt)
        {
            var query = (from b in _billRepository.Table
                         where b.Year == yy && b.Term == tt && b.SplitID == splitId && b.Type == "corrective"
                         select b).FirstOrDefault();

            return query;
        }

        public int PaymentDelayDays(int splitId, DateTime LastPaymentDate)
        {
            var query = (from b in _billRepository.Table
                         where b.SplitID == splitId && b.ExpDate > LastPaymentDate
                         select b).FirstOrDefault();

            TimeSpan PaymentDelay = DateTime.Now - (DateTime)query.ExpDate;

            return PaymentDelay.Days;
        }
        public Bill getLastBill()
        {
            int lastBillId = _billRepository.Table.Select(x => x.Id).Max();
            return _billRepository.GetById(lastBillId);
        }
        public Bill findPreviousBill(int billId, int splitId)
        {
            int previviousBillId;
            Bill currentBill = _billRepository.GetById(billId);

            if (currentBill.Type == "corrective")
            {
                previviousBillId = _billRepository.Table.Where(b =>
                    (b.SplitID == splitId) &&
                    (b.Year == currentBill.Year) && (b.Term == (currentBill.Term - 1)))
                    .Select(b => b.Id).DefaultIfEmpty(0).Max();

                if (currentBill.Term == 1)
                {
                    previviousBillId = _billRepository.Table.Where(b =>
                    (b.SplitID == splitId) &&
                    (b.Year == (currentBill.Year - 1)) && (b.Term == 12))
                    .Select(b => b.Id).DefaultIfEmpty(0).Max();
                }
            }
            else
            {
                previviousBillId = _billRepository.Table.Where(b => b.SplitID == splitId &&
                          b.DateRead < currentBill.DateRead).OrderByDescending(x => x.DateRead).Select(x => x.Id).FirstOrDefault();
            }

            return _billRepository.GetById(previviousBillId);
        }

        //in method ra badan jaigozine tamame methodhaye bala kon
        public IPagedList<Bill> GetAllBills(
            int splitId = 0,
            int year = 0, int month = 0,
            int pageIndex = 0, int pageSize = 2147483647)
        {
            var query = _billRepository.Table;

            if (splitId > 0)
                query = query.Where(s => s.SplitID == splitId);

            query = query.OrderByDescending(s => s.Year).ThenByDescending(s => s.Term).ThenByDescending(S => S.DateRead);

            var bills = new PagedList<Bill>(query, pageIndex, pageSize);

            return bills;
        }

        public IEnumerable<Bill> GetBillsByFilter1(int? fyy, int? ftt, bool domain, int? toyy, int? tott,
            int? region_Id, int? distribution_Id, int? cTariff, int splitId = 0, bool includeAll = false)
        {
            var query = from b in _billRepository.Table
                        where b.Type != "archive" && b.Type != "zero"
                        join s in _splitRepository.Table
                        on b.SplitID equals s.Id
                        where !includeAll ? s.Active == true : true
                        select b;

            if (region_Id != 0)
                query = query.Where(b => b.Split.Region_Id == region_Id);

            if (distribution_Id != 0)
                query = query.Where(b => b.Split.DistributionArea_Id == distribution_Id);

            if (cTariff != 0)
                query = query.Where(b => (int)b.Split.CTariffId == cTariff).Select(b => b);

            if (splitId != 0)
                query = query.Where(b => b.SplitID == splitId);

            if (domain)
            {
                int fromTime = Convert.ToInt32(fyy.ToString() + ftt.ToString().Trim().PadLeft(2, '0'));
                int toTime = Convert.ToInt32(toyy.ToString() + tott.ToString().Trim().PadLeft(2, '0'));

                return query.ToList().Where(b => ((Convert.ToInt32(b.Year.ToString() + b.Term.ToString().Trim().PadLeft(2, '0')) >= fromTime)
                            && (Convert.ToInt32(b.Year.ToString() + b.Term.ToString().Trim().PadLeft(2, '0')) <= toTime))).Select(b => b);
            }
            else
            {
                return query.ToList().Where(b => b.Year == fyy && b.Term == ftt).Select(b => b);
            }
        }

        #endregion

    }
}
