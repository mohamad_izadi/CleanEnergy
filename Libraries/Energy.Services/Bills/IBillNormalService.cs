﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Energy.Data;
using Energy.Core;
using Energy.Core.Data;
using Energy.Core.Domain.Bills;
using Energy.Core.Domain.Tariffs;

namespace Energy.Services.Bills
{
    public partial interface IBillNormalService
    {
        void DeleteBillNormal(BillNormal billNormal);

        IList<BillNormal> GetByBillId(int billId);

        void InsertBilln(BillNormal billNormal);

        IList<TariffRate> GetTariffRates(int tariffId);
        

    }
}
