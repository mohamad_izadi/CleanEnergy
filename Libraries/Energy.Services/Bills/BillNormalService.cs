﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Energy.Data;
using Energy.Core;
using Energy.Core.Data;
using Energy.Core.Domain.Bills;
using Energy.Core.Domain.Tariffs;

namespace Energy.Services.Bills
{
    public class BillNormalService : IBillNormalService
    {
        public IRepository<BillNormal> _billNormalRepository;
        public IRepository<TariffRate> _tariffRateRepository;

        public BillNormalService(
            IRepository<BillNormal> billNormalRepository,
            IRepository<TariffRate> tariffRateRepository)
        {
            this._billNormalRepository = billNormalRepository;
            this._tariffRateRepository = tariffRateRepository;
        }
        public void DeleteBillNormal(BillNormal billNormal)
        {
            if (billNormal == null)
                throw new ArgumentNullException("billNormal");

            _billNormalRepository.Delete(billNormal);
        }


        public IList<BillNormal> GetByBillId(int billId)
        {
            var query = from bn in _billNormalRepository.Table
                        where bn.BillID == billId
                        select bn;

            return query.ToList();
        }

        public void InsertBilln(BillNormal billNormal)
        {
            if (billNormal == null)
                throw new ArgumentNullException("billNormal");

            _billNormalRepository.Insert(billNormal);

        }

        public IList<TariffRate> GetTariffRates(int tariffId)
        {
            var query = (from tr in _tariffRateRepository.Table
                         where tr.TariffID == tariffId
                         select tr).ToList();

            return query;
        }
    }
}
