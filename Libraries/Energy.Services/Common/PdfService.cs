﻿// RTL Support provided by Credo inc (www.credo.co.il  ||   info@credo.co.il)

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Energy.Core;
using Energy.Core.Domain.Common;
using Energy.Core.Domain.Directory;
using Energy.Core.Domain.Localization;
using Energy.Core.Domain.Bills;
using Energy.Core.Html;
using Energy.Services.Configuration;
using Energy.Services.Directory;
using Energy.Services.Helpers;
using Energy.Services.Localization;
using Energy.Services.Media;
using Energy.Services.Stores;
using Energy.Services.Bills;
using Energy.Core.Domain.Constant;

namespace Energy.Services.Common
{
    /// <summary>
    /// PDF service
    /// </summary>
    public partial class PdfService : IPdfService
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly ILanguageService _languageService;
        private readonly IWorkContext _workContext;
        private readonly IBillService _billService;
        private readonly IBillNormalService _billNormalService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IPictureService _pictureService;
        private readonly IDistributionAreaService _distributionAreaService;
        private readonly IStoreService _storeService;
        private readonly IStoreContext _storeContext;
        private readonly ISettingService _settingContext;
        private readonly IWebHelper _webHelper;

        private readonly PdfSettings _pdfSettings;

        #endregion

        #region Ctor

        public PdfService(ILocalizationService localizationService,
            ILanguageService languageService,
            IWorkContext workContext,
            IBillService billService,
            IBillNormalService billNormalService,
            IDateTimeHelper dateTimeHelper,
            IPictureService pictureService,
            IDistributionAreaService distributionAreaService,
            IStoreService storeService,
            IStoreContext storeContext,
            ISettingService settingContext,
            IWebHelper webHelper,
            PdfSettings pdfSettings)
        {
            this._localizationService = localizationService;
            this._languageService = languageService;
            this._workContext = workContext;
            this._billService = billService;
            this._billNormalService = billNormalService;
            this._dateTimeHelper = dateTimeHelper;
            this._pictureService = pictureService;
            this._distributionAreaService = distributionAreaService;
            this._storeService = storeService;
            this._storeContext = storeContext;
            this._settingContext = settingContext;
            this._webHelper = webHelper;
            this._pdfSettings = pdfSettings;
        }

        #endregion

        #region Utilities

        protected virtual Font GetFont()
        {
            //nopCommerce supports unicode characters
            //nopCommerce uses Free Serif font by default (~/App_Data/Pdf/FreeSerif.ttf file)
            //It was downloaded from http://savannah.gnu.org/projects/freefont
            string fontPath = Path.Combine(_webHelper.MapPath("~/Themes/PowerHub/Content/fonts"), "BMitra.ttf");
            var baseFont = BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            var font = new Font(baseFont, 10, Font.NORMAL);
            return font;
        }

        protected virtual Font GetEnglishFont()
        {
            //nopCommerce supports unicode characters
            //nopCommerce uses Free Serif font by default (~/App_Data/Pdf/FreeSerif.ttf file)
            //It was downloaded from http://savannah.gnu.org/projects/freefont
            string fontPath = Path.Combine(_webHelper.MapPath("~/Themes/PowerHub/Content/fonts"), "tahoma.ttf");
            var baseFont = BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            var font = new Font(baseFont, 10, Font.NORMAL);
            return font;
        }

        /// <summary>
        /// Get font direction
        /// </summary>
        /// <param name="lang">Language</param>
        /// <returns>Font direction</returns>
        protected virtual int GetDirection(Language lang)
        {
            return lang.Rtl ? PdfWriter.RUN_DIRECTION_RTL : PdfWriter.RUN_DIRECTION_LTR;
        }

        /// <summary>
        /// Get element alignment
        /// </summary>
        /// <param name="lang">Language</param>
        /// <param name="isOpposite">Is opposite?</param>
        /// <returns>Element alignment</returns>
        protected virtual int GetAlignment(Language lang, bool isOpposite = false)
        {
            //if we need the element to be opposite, like logo etc`.
            if (!isOpposite)
                return lang.Rtl ? Element.ALIGN_RIGHT : Element.ALIGN_LEFT;

            return lang.Rtl ? Element.ALIGN_LEFT : Element.ALIGN_RIGHT;
        }

        #endregion

        #region Methods

        public void PrintBillToPdf(Stream stream, Bill bill, bool withDebit = true)
        {
            if (stream == null)
                throw new ArgumentNullException("stream");

            if (bill == null)
                throw new ArgumentNullException("bill");

            var bills = new List<Bill>();
            bills.Add(bill);
            PrintBillsToPdf(stream, bills, withDebit);
        }

        public void PrintBillsToPdf(Stream stream, IList<Bill> bills, bool withDebit = true)
        {
            if (stream == null)
                throw new ArgumentNullException("stream");

            if (bills == null)
                throw new ArgumentNullException("bills");

            var pageSize = PageSize.A4;

            if (_pdfSettings.LetterPageSizeEnabled)
            {
                pageSize = PageSize.LETTER;
            }

            #region initialize basic variables

            var doc = new Document(pageSize);
            doc.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
            var pdfWriter = PdfWriter.GetInstance(doc, stream);
            doc.SetMargins(15, 15, 15, 15);

            //fonts
            var titleFont = GetFont();
            //titleFont.SetStyle(Font.BOLD);
            titleFont.Size = 12;
            titleFont.Color = BaseColor.BLACK;

            var englishFont = GetEnglishFont();
            englishFont.SetStyle(Font.NORMAL);
            englishFont.Size = 10;
            englishFont.Color = BaseColor.BLACK;

            var HeaderFont = GetFont();
            HeaderFont.Color = BaseColor.BLACK;
            HeaderFont.SetStyle(Font.BOLD);
            HeaderFont.Size = 11;

            var SumFont = GetFont();
            SumFont.Color = BaseColor.BLACK;
            SumFont.SetStyle(Font.BOLD);
            SumFont.Size = 10;

            var font = GetFont();
            font.SetStyle(Font.NORMAL);
            font.Size = 11;
            font.Color = BaseColor.BLACK;

            var redFont = GetFont();
            redFont.SetStyle(Font.BOLD);
            redFont.Size = 11;
            redFont.Color = BaseColor.RED;

            var attributesFont = GetFont();
            attributesFont.SetStyle(Font.ITALIC);

            FontSelector fontSelector = new FontSelector();

            if (font.Familyname != "unknown")
            {
                fontSelector.AddFont(font);
            }

            if (englishFont.Familyname != "unknown")
            {
                fontSelector.AddFont(englishFont);
            }

            #endregion

            doc.Open();

            int billCount = bills.Count;
            int billNum = 0;

            foreach (var bill in bills)
            {
                //by default _pdfSettings contains settings for the current active store
                //and we need PdfSettings for the store which was used to place an order
                //so let's load it based on a store of the current order
                var pdfSettingsByStore = _settingContext.LoadSetting<PdfSettings>(_storeContext.CurrentStore.Id);

                var lang = _languageService.GetLanguageById(2);
                if (lang == null || !lang.Published)
                    lang = _workContext.WorkingLanguage;

                if (bill.Split.Demand == (int)CDemand.Demand)
                {
                    var mainTable = new PdfPTable(2);
                    mainTable.DefaultCell.Border = Rectangle.NO_BORDER;
                    mainTable.DefaultCell.CellEvent = new RoundedBorderMain();
                    mainTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    mainTable.SetWidths(new[] { 3, 97 });

                    //Main table
                    var table = new PdfPTable(11);
                    table.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    table.DefaultCell.BorderWidth = 0.01f;
                    table.DefaultCell.PaddingBottom = 5f;
                    table.WidthPercentage = 100f;
                    table.SetWidths(new[] { 12, 12, 6, 13, 9, 10, 10, 6, 8, 6, 8 });
                    table.DefaultCell.Border = Rectangle.NO_BORDER;

                    #region Header

                    //logo
                    //var logoPicture = _pictureService.GetPictureById(pdfSettingsByStore.LogoPictureId);
                    var logoPicture = _pictureService.GetPictureById(313);
                    var logoExists = logoPicture != null;

                    //header row 1 (with logo)
                    var logoFilePath = _pictureService.GetThumbLocalPath(logoPicture, 0, false);
                    var logo = Image.GetInstance(logoFilePath);
                    logo.Alignment = GetAlignment(lang, true);
                    logo.ScaleToFit(140f, 140f);

                    var cellLogo = new PdfPCell();
                    cellLogo.Colspan = 5;
                    cellLogo.Padding = 10f;
                    cellLogo.AddElement(logo);
                    cellLogo.Border = Rectangle.NO_BORDER;
                    table.AddCell(cellLogo);

                    PdfPTable nested = new PdfPTable(3);
                    nested.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    nested.DefaultCell.Border = Rectangle.NO_BORDER;
                    nested.SetWidths(new[] { 20, 10, 70 });

                    var cell = new PdfPCell(new Phrase(string.Format("مشترک محترم : {0}", bill.Split.Title), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    nested.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("کد پستی :", bill.Split.Postal), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    nested.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.Split.Postal);
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    nested.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("نشانی محل مصرف : {0}", bill.Split.Address), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    cell.Colspan = 3;
                    nested.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("نشانی مکاتباتی : {0}", bill.Split.AddressContact), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    cell.PaddingBottom = 5f;
                    cell.Colspan = 3;
                    nested.AddCell(cell);

                    PdfPCell nesthousing = new PdfPCell(nested);
                    nesthousing.Colspan = 6;
                    table.AddCell(nesthousing);

                    #endregion

                    #region  header row 2

                    nested = new PdfPTable(1);
                    nested.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    nested.DefaultCell.Border = Rectangle.NO_BORDER;
                    nested.SetWidths(new[] { 100 });

                    cell = new PdfPCell(new Phrase(string.Format("آدرس : تهران - صادقیه ، خیابان کوهدشت پلاک 12 واحد 5"), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    nested.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("واحد صدور صورتحساب : 44288947-021"), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    nested.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("منطقه برق :"), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    nested.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("واحد حوادث : 34623537-011"), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    cell.PaddingBottom = 10f;
                    nested.AddCell(cell);

                    nesthousing = new PdfPCell(nested);
                    nesthousing.Colspan = 5;
                    table.AddCell(nesthousing);

                    /////////////////////////

                    nested = new PdfPTable(2);
                    nested.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    nested.DefaultCell.Border = Rectangle.NO_BORDER;
                    nested.SetWidths(new[] { 50, 50 });

                    cell = new PdfPCell(new Phrase(string.Format("شماره انشعاب : {0}", bill.Split.Id), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    nested.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("رمز رایانه : {0}", bill.Split.DisPass), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    nested.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("پرونده : {0}", bill.Split.FileNo), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    nested.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("تاریخ نصب : {0}", bill.Split.InstallDate.HasValue ? bill.Split.InstallDate.Value.PersianDateN("/") : ""), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    nested.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("عنوان و کد تعرفه : {0} {1}", bill.Split.CTariffId.ToString().GetTariff(), bill.Title), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    nested.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("نوع فعالیت :"), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    nested.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("تاریخ انقضا پروانه : {0}", bill.Split.PowerTagDate.HasValue ? bill.Split.PowerTagDate.Value.PersianDateN("/") : ""), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    cell.PaddingBottom = 10f;
                    nested.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("ولتاژ تغذیه : {0}", bill.Split.Volt.ToString()), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingBottom = 10f;
                    nested.AddCell(cell);

                    nesthousing = new PdfPCell(nested);
                    nesthousing.Colspan = 6;
                    table.AddCell(nesthousing);

                    #endregion

                    #region Handheld Titles

                    cell = new PdfPCell(new Phrase("قدرت - کیلو وات", HeaderFont));
                    cell.Colspan = 4;
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BorderWidth = 0.01f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("شرح مصارف", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("رقم", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("شمارنده قبلی", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("شمارنده کنونی", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("مصرف کل", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("نرخ", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("مبلغ - ریال", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    #endregion

                    #region MidRead info row 1

                    cell = new PdfPCell(new Phrase("قراردادی", font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.Split.Power.ToString());
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("محاسبه شده", font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.Power90.ToString("#.###"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("میان باری", font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.Split.Timer.ToString());
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MidReadPre.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MidRead.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MidUsed.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MidRate.ToString());
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MidCost.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    #endregion

                    #region MaxRead info row 2

                    cell = new PdfPCell(new Phrase("پروانه مجاز", font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.Split.PowerTag != null ? bill.Split.PowerTag.ToString() : "");
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("کاهش یافته", font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process("");
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("اوج بار", font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.Split.Timer.ToString());
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MaxReadPre.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MaxRead.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MaxUsed.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MaxRate.ToString());
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MaxCost.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    #endregion

                    #region MinRead info row 3

                    cell = new PdfPCell(new Phrase("مصرفی", font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.PowerUsed.ToString("#.###"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("تجاوز از قدرت", font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.PowerUsed - bill.Split.Power > 0 ? (bill.PowerUsed - bill.Split.Power).ToString() : "0");
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("کم باری", font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.Split.Timer.ToString());
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MinReadPre.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MinRead.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MinUsed.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MinRate.ToString());
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MinCost.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    #endregion

                    #region Reactive info

                    cell = new PdfPCell(new Phrase("مشخصات کنتور", HeaderFont));
                    cell.Colspan = 4;
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BorderWidth = 0.01f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("راکتیو", font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.Split.Timer.ToString());
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.ReactReadPre.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.ReactRead.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.ReactUsed.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(String.Format("{0:#,0.000}", bill.ReactRate));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.ReactCost.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    table.AddCell(cell);

                    #endregion

                    #region bill info row 1

                    cell = new PdfPCell(new Phrase("شماره بدنه کنتور اکتیو", font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Colspan = 2;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.Split.SerialNo != null ? bill.Split.SerialNo.ToString() : "");
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Colspan = 2;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("از تاریخ {0}   تا تاریخ {1}", bill.DateReadPre.PersianDateN("/"), bill.DateRead.PersianDateN("/")), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Colspan = 3;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("تعداد روز : {0}", bill.AllDays.ToString()), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Colspan = 2;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("بهای انرژی", redFont));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthBottom = 0f;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.AllCost.ToString("#,##0"), redFont));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthBottom = 0f;
                    table.AddCell(cell);

                    #endregion

                    #region bill info row 2

                    cell = new PdfPCell(new Phrase("شماره بدنه کنتور راکتیو", font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Colspan = 2;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process("");
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Colspan = 2;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("تاریخ صدور صورتحساب : {0}", bill.issueDate.Value.PersianDateN("/")), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthTop = 0f;
                    cell.BorderWidthBottom = 0f;
                    cell.PaddingRight = 10f;
                    cell.Colspan = 5;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("بهای قدرت مصرفی", font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthTop = 0f;
                    cell.BorderWidthBottom = 0f;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.Power90Cost.ToString("#,##0"), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthTop = 0f;
                    cell.BorderWidthBottom = 0f;
                    table.AddCell(cell);

                    #endregion

                    #region bill info row 3

                    cell = new PdfPCell(new Phrase("ضریب کنتور", font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Colspan = 2;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.Split.Factor.ToString());
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Colspan = 2;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("براساس ضریب بدی مصرف محاسبه گردیده"), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthTop = 0f;
                    cell.BorderWidthBottom = 0f;
                    cell.PaddingRight = 10f;
                    cell.Colspan = 5;
                    table.AddCell(cell);

                    if (bill.Split.CTariffId == (int)Energy.Core.Domain.Constant.CTariff.Industry)
                    {
                        nested = new PdfPTable(1);
                        nested.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        nested.DefaultCell.Border = Rectangle.NO_BORDER;
                        nested.SetWidths(new[] { 100 });

                        cell = new PdfPCell(new Phrase("انشعاب آزاد", font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.BorderWidthTop = 0f;
                        cell.BorderWidthBottom = 0f;
                        nested.AddCell(cell);

                        cell = new PdfPCell(new Phrase("تخفیف صنعتی", font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.BorderWidthTop = 0f;
                        cell.BorderWidthBottom = 0f;
                        nested.AddCell(cell);

                        nesthousing = new PdfPCell(nested);
                        nesthousing.Border = Rectangle.NO_BORDER;
                        nesthousing.Colspan = 1;
                        table.AddCell(nesthousing);

                        /////////////////////////////

                        nested = new PdfPTable(1);
                        nested.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        nested.DefaultCell.Border = Rectangle.NO_BORDER;
                        nested.SetWidths(new[] { 100 });

                        cell = new PdfPCell(new Phrase(bill.FreelanceCost.ToString("#,##0"), font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.BorderWidthTop = 0f;
                        cell.BorderWidthBottom = 0f;
                        nested.AddCell(cell);

                        cell = new PdfPCell(new Phrase(bill.IndustDisCost.ToString("#,##0"), font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.BorderWidthTop = 0f;
                        cell.BorderWidthBottom = 0f;
                        nested.AddCell(cell);

                        nesthousing = new PdfPCell(nested);
                        nesthousing.Border = Rectangle.NO_BORDER;
                        nesthousing.Colspan = 1;
                        table.AddCell(nesthousing);
                    }
                    else
                    {
                        cell = new PdfPCell(new Phrase("انشعاب آزاد", font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.BorderWidthTop = 0f;
                        cell.BorderWidthBottom = 0f;
                        table.AddCell(cell);

                        cell = new PdfPCell(new Phrase(bill.FreelanceCost.ToString("#,##0"), font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.BorderWidthTop = 0f;
                        cell.BorderWidthBottom = 0f;
                        table.AddCell(cell);
                    }

                    #endregion

                    #region bill info row 4

                    cell = new PdfPCell(new Phrase("مشخصات ترانس", font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Colspan = 2;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process("");
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Colspan = 2;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("ضریب زیان بدی مصرف"), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_TOP;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 10f;
                    cell.Colspan = 2;
                    table.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(String.Format("{0:#,0.000}", bill.LossFactor));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_TOP;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 10f;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("تخفیفات : 0", font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_TOP;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 2;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("تجاوز از قدرت", font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthTop = 0f;
                    cell.BorderWidthBottom = 0f;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.PowerUsed - bill.Split.Power > 0 ? (bill.PowerUsed - bill.Split.Power).ToString() : "0", font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthTop = 0f;
                    cell.BorderWidthBottom = 0f;
                    table.AddCell(cell);

                    #endregion

                    #region bill info row 5

                    cell = new PdfPCell(new Phrase("دوره / سال", font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Colspan = 2;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("{0} / {1}", bill.Term.ToString(), bill.Year.ToString()), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Colspan = 2;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("مصرف کل - کیلووات ساعت : {0}", (bill.MidUsed + bill.MaxUsed + bill.MinUsed).ToString()), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_TOP;
                    cell.BorderWidthTop = 0f;
                    cell.BorderWidthBottom = 0f;
                    cell.PaddingRight = 10f;
                    cell.Colspan = 5;
                    table.AddCell(cell);

                    if (bill.Split.NetworkCost)
                    {
                        nested = new PdfPTable(1);
                        nested.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        nested.DefaultCell.Border = Rectangle.NO_BORDER;
                        nested.SetWidths(new[] { 100 });

                        cell = new PdfPCell(new Phrase("هزینه نگهداری شبکه", font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.BorderWidthTop = 0f;
                        cell.BorderWidthBottom = 0f;
                        nested.AddCell(cell);

                        cell = new PdfPCell(new Phrase("بهای فصل", font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.BorderWidthTop = 0f;
                        cell.BorderWidthBottom = 0f;
                        nested.AddCell(cell);

                        cell = new PdfPCell(new Phrase("آبونمان", font));
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.BorderWidthTop = 0f;
                        cell.BorderWidthBottom = 0f;
                        nested.AddCell(cell);

                        nesthousing = new PdfPCell(nested);
                        nesthousing.Border = Rectangle.NO_BORDER;
                        nesthousing.Colspan = 1;
                        table.AddCell(nesthousing);

                        /////////////////////////////

                        nested = new PdfPTable(1);
                        nested.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        nested.DefaultCell.Border = Rectangle.NO_BORDER;
                        nested.SetWidths(new[] { 100 });

                        cell = new PdfPCell(new Phrase(bill.NetworkCost.ToString("#,##0"), font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.BorderWidthTop = 0f;
                        cell.BorderWidthBottom = 0f;
                        nested.AddCell(cell);

                        cell = new PdfPCell(new Phrase(bill.WarmCost.ToString("#,##0"), font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.BorderWidthTop = 0f;
                        cell.BorderWidthBottom = 0f;
                        nested.AddCell(cell);

                        var subscription = (long)((bill.AllDays / 30) * 90000);
                        cell = new PdfPCell(new Phrase(subscription.ToString("#,##0"), font));
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.BorderWidthTop = 0f;
                        cell.BorderWidthBottom = 0f;
                        nested.AddCell(cell);

                        nesthousing = new PdfPCell(nested);
                        nesthousing.Border = Rectangle.NO_BORDER;
                        nesthousing.Colspan = 1;
                        table.AddCell(nesthousing);
                    }
                    else
                    {
                        nested = new PdfPTable(1);
                        nested.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        nested.DefaultCell.Border = Rectangle.NO_BORDER;
                        nested.SetWidths(new[] { 100 });

                        cell = new PdfPCell(new Phrase("بهای فصل", font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.BorderWidthTop = 0f;
                        cell.BorderWidthBottom = 0f;
                        nested.AddCell(cell);

                        cell = new PdfPCell(new Phrase("آبونمان", font));
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.BorderWidthTop = 0f;
                        cell.BorderWidthBottom = 0f;
                        nested.AddCell(cell);

                        nesthousing = new PdfPCell(nested);
                        nesthousing.Border = Rectangle.NO_BORDER;
                        nesthousing.Colspan = 1;
                        table.AddCell(nesthousing);

                        /////////////////////////////

                        nested = new PdfPTable(1);
                        nested.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        nested.DefaultCell.Border = Rectangle.NO_BORDER;
                        nested.SetWidths(new[] { 100 });

                        cell = new PdfPCell(new Phrase(bill.WarmCost.ToString("#,##0"), font));
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.BorderWidthTop = 0f;
                        cell.BorderWidthBottom = 0f;
                        nested.AddCell(cell);

                        var subscription = (long)((bill.AllDays / 30) * 90000);
                        cell = new PdfPCell(new Phrase(subscription.ToString("#,##0"), font));
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.BorderWidthTop = 0f;
                        cell.BorderWidthBottom = 0f;
                        nested.AddCell(cell);

                        nesthousing = new PdfPCell(nested);
                        nesthousing.Border = Rectangle.NO_BORDER;
                        nesthousing.Colspan = 1;
                        table.AddCell(nesthousing);
                    }

                    #endregion

                    #region footer info row 1

                    cell = new PdfPCell(new Phrase("مشترک گرامی : ", font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_TOP;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 2;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.Description, font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_TOP;
                    cell.BorderWidthRight = 0f;
                    cell.Colspan = 7;
                    table.AddCell(cell);

                    nested = new PdfPTable(1);
                    nested.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    nested.DefaultCell.Border = Rectangle.NO_BORDER;
                    nested.SetWidths(new[] { 100 });

                    cell = new PdfPCell(new Phrase("بهای برق دوره", redFont));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthBottom = 0f;
                    nested.AddCell(cell);

                    cell = new PdfPCell(new Phrase("مالیات برارزش افزوده", font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthTop = 0f;
                    cell.BorderWidthBottom = 0f;
                    nested.AddCell(cell);

                    cell = new PdfPCell(new Phrase("عوارض", font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthTop = 0f;
                    cell.BorderWidthBottom = 0f;
                    nested.AddCell(cell);

                    cell = new PdfPCell(new Phrase("بدهی قبلی", font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthTop = 0f;
                    cell.BorderWidthBottom = 0f;
                    nested.AddCell(cell);

                    nesthousing = new PdfPCell(nested);
                    nesthousing.Border = Rectangle.NO_BORDER;
                    nesthousing.Colspan = 1;
                    table.AddCell(nesthousing);

                    /////////////////////////////

                    nested = new PdfPTable(1);
                    nested.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    nested.DefaultCell.Border = Rectangle.NO_BORDER;
                    nested.SetWidths(new[] { 100 });

                    cell = new PdfPCell(new Phrase(bill.Total.ToString("#,##0"), redFont));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthBottom = 0f;
                    nested.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.Tax.ToString("#,##0"), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthTop = 0f;
                    cell.BorderWidthBottom = 0f;
                    nested.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.Toll.ToString("#,##0"), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthTop = 0f;
                    cell.BorderWidthBottom = 0f;
                    nested.AddCell(cell);

                    if (withDebit == true)
                    {
                        cell = new PdfPCell(new Phrase(bill.Debit.ToString("#,##0"), font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.BorderWidthTop = 0f;
                        cell.BorderWidthBottom = 0f;
                        nested.AddCell(cell);
                    }
                    else
                    {
                        cell = new PdfPCell(new Phrase("0", font));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.BorderWidthTop = 0f;
                        cell.BorderWidthBottom = 0f;
                        nested.AddCell(cell);
                    }

                    nesthousing = new PdfPCell(nested);
                    nesthousing.Border = Rectangle.NO_BORDER;
                    nesthousing.Colspan = 1;
                    table.AddCell(nesthousing);

                    #endregion

                    #region footer info row 2

                    nested = new PdfPTable(2);
                    nested.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    nested.DefaultCell.Border = Rectangle.NO_BORDER;
                    nested.SetWidths(new[] { 65, 35 });

                    if (withDebit == true)
                    {
                        cell = new PdfPCell(new Phrase(string.Format("شناسه واریز : {0}", bill.PaymentID), font));
                        cell.PaddingBottom = 5f;
                        cell.PaddingRight = 10f;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        cell.VerticalAlignment = Element.ALIGN_TOP;
                        cell.Border = Rectangle.NO_BORDER;
                        cell.Border = Rectangle.TOP_BORDER;
                        nested.AddCell(cell);
                    }
                    else
                    {
                        cell = new PdfPCell(new Phrase(string.Format("شناسه واریز : {0}", bill.PaymentIdWD), font));
                        cell.PaddingBottom = 5f;
                        cell.PaddingRight = 10f;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        cell.VerticalAlignment = Element.ALIGN_TOP;
                        cell.Border = Rectangle.NO_BORDER;
                        cell.Border = Rectangle.TOP_BORDER;
                        nested.AddCell(cell);
                    }

                    cell = new PdfPCell(new Phrase(string.Format("شماره انشعاب : {0}", bill.Split.Id), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_TOP;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BorderWidthRight = 0f;
                    nested.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("شماره حساب - بانک ملت : 4299775653"), font));
                    cell.PaddingTop = 5f;
                    cell.PaddingRight = 10f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_TOP;
                    cell.Border = Rectangle.NO_BORDER;
                    nested.AddCell(cell);

                    cell = new PdfPCell();
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_TOP;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BorderWidthRight = 0f;
                    var link = new Chunk("www.clean-energy.ir", englishFont);
                    link.SetAnchor("http://clean-energy.ir/");
                    cell.AddElement(link);
                    nested.AddCell(cell);

                    nesthousing = new PdfPCell(nested);
                    nesthousing.Border = Rectangle.NO_BORDER;
                    nesthousing.Colspan = 9;
                    table.AddCell(nesthousing);

                    /////////////////////////////////
                    nested = new PdfPTable(1);
                    nested.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    nested.DefaultCell.Border = Rectangle.NO_BORDER;
                    nested.SetWidths(new[] { 100 });

                    cell = new PdfPCell(new Phrase("جریمه دیرکرد", font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthTop = 0f;
                    cell.BorderWidthBottom = 0f;
                    nested.AddCell(cell);

                    cell = new PdfPCell(new Phrase("کسر هزار ریال", font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthTop = 0f;
                    cell.BorderWidthBottom = 0f;
                    cell.PaddingBottom = 5f;
                    nested.AddCell(cell);

                    cell = new PdfPCell(new Phrase("قابل پرداخت", redFont));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_TOP;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.PaddingBottom = 5f;
                    nested.AddCell(cell);

                    cell = new PdfPCell(new Phrase("مهلت پرداخت", redFont));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthTop = 0f;
                    cell.BorderWidthBottom = 0f;
                    cell.PaddingBottom = 7f;
                    nested.AddCell(cell);

                    nesthousing = new PdfPCell(nested);
                    nesthousing.Border = Rectangle.NO_BORDER;
                    nesthousing.Colspan = 1;
                    table.AddCell(nesthousing);

                    /////////////////////////////

                    nested = new PdfPTable(1);
                    nested.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    nested.DefaultCell.Border = Rectangle.NO_BORDER;
                    nested.SetWidths(new[] { 100 });

                    cell = new PdfPCell(new Phrase(bill.LatePenalty.ToString("#,##0"), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthTop = 0f;
                    cell.BorderWidthBottom = 0f;
                    nested.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.Round.ToString("#,##0"), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.PaddingBottom = 5f;
                    cell.BorderWidthTop = 0f;
                    cell.BorderWidthBottom = 0f;
                    nested.AddCell(cell);

                    if (withDebit)
                    {
                        cell = new PdfPCell(new Phrase(bill.Payable.ToString("#,##0"), redFont));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.BackgroundColor = new BaseColor(240, 240, 240);
                        cell.PaddingBottom = 5f;
                        nested.AddCell(cell);
                    }
                    else
                    {
                        var payable = bill.Payable - bill.Debit;
                        cell = new PdfPCell(new Phrase(payable.ToString("#,##0"), redFont));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.BackgroundColor = new BaseColor(240, 240, 240);
                        cell.PaddingBottom = 5f;
                        nested.AddCell(cell);
                    }

                    cell = new PdfPCell(new Phrase(bill.ExpDate.HasValue ? bill.ExpDate.Value.PersianDateNormal("/") : "", redFont));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthTop = 0f;
                    cell.BorderWidthBottom = 0f;
                    cell.PaddingBottom = 7f;
                    nested.AddCell(cell);

                    nesthousing = new PdfPCell(nested);
                    nesthousing.Border = Rectangle.NO_BORDER;
                    nesthousing.Colspan = 1;
                    table.AddCell(nesthousing);

                    #endregion

                    #region Financial code

                    cell = new PdfPCell(new Phrase("کد اقتصادی : 411395346864", HeaderFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_TOP;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Rotation = 90;
                    cell.PaddingTop = 20f;
                    table.AddCell(cell);

                    #endregion

                    mainTable.AddCell(table);
                    mainTable.AddCell(cell);

                    doc.Add(mainTable);

                    if (bill.Debit > bill.AllCost && withDebit)
                    {
                        var descriptionTable = new PdfPTable(1);
                        descriptionTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        descriptionTable.DefaultCell.Border = PdfPCell.NO_BORDER;
                        descriptionTable.WidthPercentage = 100f;
                        descriptionTable.DefaultCell.Padding = 0f;
                        descriptionTable.SpacingBefore = 0.5f;

                        String text = "مشترک گرامی، در صورت عدم پرداخت صورتحساب تا مهلت مقرر، نسبت به قطع برق انشعاب، اقدام خواهد گردید";
                        cell = new PdfPCell(new Phrase(text, redFont));
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.Border = Rectangle.NO_BORDER;
                        cell.PaddingRight = 85f;
                        descriptionTable.AddCell(cell);
                        doc.Add(descriptionTable);
                    }
                }
                else if (bill.Split.Demand == (int)CDemand.Normal)
                {
                    var mainTable = new PdfPTable(2);
                    mainTable.DefaultCell.Border = Rectangle.NO_BORDER;
                    mainTable.DefaultCell.CellEvent = new RoundedBorderMain();
                    mainTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    mainTable.SetWidths(new[] { 3, 97 });

                    //Main table
                    var table = new PdfPTable(3);
                    table.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    table.DefaultCell.BorderWidth = 0.01f;
                    table.DefaultCell.PaddingBottom = 5f;
                    table.WidthPercentage = 100f;
                    table.SetWidths(new[] { 65, 20, 15 });
                    table.DefaultCell.Border = Rectangle.NO_BORDER;

                    #region Header

                    //logo
                    //var logoPicture = _pictureService.GetPictureById(pdfSettingsByStore.LogoPictureId);
                    var logoPicture = _pictureService.GetPictureById(313);
                    var logoExists = logoPicture != null;

                    //header row 1 (with logo)
                    var logoFilePath = _pictureService.GetThumbLocalPath(logoPicture, 0, false);
                    var logo = Image.GetInstance(logoFilePath);
                    logo.Alignment = GetAlignment(lang, true);
                    logo.ScaleToFit(140f, 140f);

                    var cellLogo = new PdfPCell();
                    cellLogo.Colspan = 2;
                    cellLogo.Padding = 10f;
                    cellLogo.AddElement(logo);
                    cellLogo.Border = Rectangle.NO_BORDER;
                    table.AddCell(cellLogo);

                    //PdfPTable nested = new PdfPTable(1);
                    //nested.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    //nested.DefaultCell.Border = Rectangle.NO_BORDER;

                    //PdfPCell nesthousing = new PdfPCell(nested);
                    //nesthousing.BorderWidth = 0f;
                    //nesthousing.Colspan = 8;
                    //table.AddCell(nesthousing);

                    #endregion

                    #region Handheld Titles

                    var nestedTable = new PdfPTable(8);
                    nestedTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    nestedTable.DefaultCell.BorderWidth = 0.01f;
                    nestedTable.DefaultCell.PaddingBottom = 5f;
                    nestedTable.WidthPercentage = 100f;
                    nestedTable.SetWidths(new[] { 12, 20, 10, 10, 10, 10, 13, 15 });
                    nestedTable.DefaultCell.Border = Rectangle.NO_BORDER;

                    var cell = new PdfPCell(new Phrase("m", HeaderFont));
                    cell.Colspan = 8;
                    cell.Padding = 1f;
                    cell.BorderWidth = 0f;
                    cell.PaddingBottom = 5f;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("شرح مصارف", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BorderWidth = 0.01f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("تاریخ", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("میان باری", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("اوج بار", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("کم باری", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("دوره/سال", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("عنوان", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("مصرف", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    PdfPCell nesthousing = new PdfPCell(nestedTable);
                    nesthousing.BorderWidth = 0f;
                    table.AddCell(nesthousing);

                    #endregion

                    #region HH info row 1

                    nestedTable = new PdfPTable(8);
                    nestedTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    nestedTable.DefaultCell.BorderWidth = 0.01f;
                    nestedTable.DefaultCell.PaddingBottom = 5f;
                    nestedTable.WidthPercentage = 100f;
                    nestedTable.SetWidths(new[] { 12, 20, 10, 10, 10, 10, 13, 15 });
                    nestedTable.DefaultCell.Border = Rectangle.NO_BORDER;

                    cell = new PdfPCell(new Phrase(string.Format("مشترک محترم : {0}", bill.Split.Title), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.PaddingRight = 5f;
                    cell.PaddingBottom = 5f;
                    cell.BorderWidth = 0.01f;
                    cell.BorderWidthBottom = 0f;
                    cell.Colspan = 2;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase("قرائت کنونی", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BorderWidth = 0.01f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.DateRead.PersianDateNormal("/"), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MidRead.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MaxRead.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MinRead.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("{0} / {1}", bill.Term.ToString(), bill.Year.ToString()), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("بهای انرژی"), redFont));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    cell.PaddingBottom = 5f;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.AllCost.ToString("#,##0"), redFont));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthBottom = 0f;
                    nestedTable.AddCell(cell);

                    nesthousing = new PdfPCell(nestedTable);
                    nesthousing.BorderWidth = 0f;
                    table.AddCell(nesthousing);

                    #endregion

                    #region HH info row 2

                    cell = new PdfPCell(new Phrase(string.Format("نشانی محل مصرف : {0}", bill.Split.Address), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.PaddingRight = 5f;
                    cell.PaddingBottom = 5f;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 2;
                    table.AddCell(cell);

                    nestedTable = new PdfPTable(8);
                    nestedTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    nestedTable.DefaultCell.BorderWidth = 0.01f;
                    nestedTable.DefaultCell.PaddingBottom = 5f;
                    nestedTable.WidthPercentage = 100f;
                    nestedTable.SetWidths(new[] { 12, 20, 10, 10, 10, 10, 13, 15 });
                    nestedTable.DefaultCell.Border = Rectangle.NO_BORDER;

                    cell = new PdfPCell(new Phrase("قرائت پیشین", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BorderWidth = 0.01f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.DateReadPre.PersianDateNormal("/"), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MidReadPre.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MaxReadPre.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MinReadPre.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format(""), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("جریمه مصرف اوج بار"), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    cell.PaddingBottom = 5f;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.FineCost.ToString("#,##0"), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthTop = 0f;
                    cell.BorderWidthBottom = 0f;
                    nestedTable.AddCell(cell);

                    nesthousing = new PdfPCell(nestedTable);
                    nesthousing.BorderWidth = 0f;
                    table.AddCell(nesthousing);

                    #endregion

                    #region HH info row 3

                    cell = new PdfPCell(new Phrase(string.Format("کد پستی :", bill.Split.Postal), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.PaddingRight = 5f;
                    cell.PaddingBottom = 5f;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 2;
                    table.AddCell(cell);

                    nestedTable = new PdfPTable(8);
                    nestedTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    nestedTable.DefaultCell.BorderWidth = 0.01f;
                    nestedTable.DefaultCell.PaddingBottom = 5f;
                    nestedTable.WidthPercentage = 100f;
                    nestedTable.SetWidths(new[] { 12, 20, 10, 10, 10, 10, 13, 15 });
                    nestedTable.DefaultCell.Border = Rectangle.NO_BORDER;

                    cell = new PdfPCell(new Phrase("مصرف", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BorderWidth = 0.01f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("kwh", englishFont));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MidUsed.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MaxUsed.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.MinUsed.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format(""), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("تخفیف مصرف کم باری"), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BorderWidthBottom = 0.01f;
                    cell.PaddingRight = 5f;
                    cell.PaddingBottom = 5f;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.DiscountCost.ToString("#,##0"), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthTop = 0f;
                    cell.BorderWidthBottom = 0f;
                    nestedTable.AddCell(cell);

                    nesthousing = new PdfPCell(nestedTable);
                    nesthousing.BorderWidth = 0f;
                    table.AddCell(nesthousing);

                    #endregion

                    #region HH info row 4

                    cell = new PdfPCell(new Phrase(string.Format("شماره انشعاب : {0}", bill.Split.Id), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("پرونده : {0}", bill.Split.FileNo), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    table.AddCell(cell);

                    nestedTable = new PdfPTable(8);
                    nestedTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    nestedTable.DefaultCell.BorderWidth = 0.01f;
                    nestedTable.DefaultCell.PaddingBottom = 5f;
                    nestedTable.WidthPercentage = 100f;
                    nestedTable.SetWidths(new[] { 12, 20, 10, 10, 10, 10, 13, 15 });
                    nestedTable.DefaultCell.Border = Rectangle.NO_BORDER;

                    cell = new PdfPCell(new Phrase("مصرف کل دوره", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BorderWidth = 0.01f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("kwh", englishFont));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.AllUsed.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Colspan = 2;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("تعداد روز"), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.PaddingRight = 5f;
                    cell.PaddingBottom = 5f;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(bill.AllDays.ToString());
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("انشعاب آزاد"), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BorderWidthLeft = 0.01f;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.FreelanceCost.ToString("#,##0"), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthBottom = 0f;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BorderWidthTop = 0.01f;
                    nestedTable.AddCell(cell);

                    nesthousing = new PdfPCell(nestedTable);
                    nesthousing.BorderWidth = 0f;
                    table.AddCell(nesthousing);

                    #endregion

                    #region HH info row 5

                    cell = new PdfPCell(new Phrase(string.Format("رمز رایانه : {0}", bill.Split.DisPass), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BorderWidthBottom = 0.01f;
                    cell.PaddingRight = 5f;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("تعداد خانوار : "), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BorderWidthBottom = 0.01f;
                    cell.PaddingRight = 5f;
                    table.AddCell(cell);

                    nestedTable = new PdfPTable(8);
                    nestedTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    nestedTable.DefaultCell.BorderWidth = 0.01f;
                    nestedTable.DefaultCell.PaddingBottom = 5f;
                    nestedTable.WidthPercentage = 100f;
                    nestedTable.SetWidths(new[] { 12, 20, 10, 10, 10, 10, 13, 15 });
                    nestedTable.DefaultCell.Border = Rectangle.NO_BORDER;

                    cell = new PdfPCell(new Phrase("متوسط مصرف در 30 روز", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BorderWidth = 0.01f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Colspan = 2;
                    nestedTable.AddCell(cell);

                    var allUsedAvg = (bill.AllUsed / bill.AllDays) * 30;
                    cell = new PdfPCell();
                    cell.Phrase = fontSelector.Process(allUsedAvg.ToString("#,##0"));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Colspan = 2;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format(""), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    cell.PaddingBottom = 5f;
                    cell.Colspan = 2;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("بهای فصل"), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BorderWidthRight = 0.01f;
                    nestedTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.WarmCost.ToString("#,##0"), font));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.BorderWidthBottom = 0f;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BorderWidthRight = 0.01f;
                    nestedTable.AddCell(cell);

                    nesthousing = new PdfPCell(nestedTable);
                    nesthousing.BorderWidth = 0f;
                    table.AddCell(nesthousing);

                    #endregion

                    #region Consuming table

                    var splitInfoTable = new PdfPTable(2);
                    splitInfoTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    splitInfoTable.DefaultCell.BorderWidth = 0.01f;
                    splitInfoTable.DefaultCell.PaddingBottom = 5f;
                    splitInfoTable.WidthPercentage = 100f;
                    splitInfoTable.SetWidths(new[] { 55, 45 });
                    splitInfoTable.DefaultCell.Border = Rectangle.NO_BORDER;

                    cell = new PdfPCell(new Phrase(string.Format("تعرفه مصرف : {0}", GetTariff(bill.Split.CTariffId.ToString())), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    cell.Colspan = 2;
                    splitInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("نوع فعالیت : {0}", ""), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    splitInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("انقضا پروانه : {0}", bill.Split.PowerTagDate.HasValue ? bill.Split.PowerTagDate.Value.PersianDateN("/") : ""), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    splitInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("شماره بدنه کنتور : {0}", bill.Split.SerialNo), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    cell.Colspan = 2;
                    splitInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("ارقام / ضریب : {0}", bill.Split.Timer), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    cell.Colspan = 2;
                    splitInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("فاز / آمپر : {0} / {1}", bill.Split.Phase, bill.Split.Amp), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BorderWidthBottom = 0.01f;
                    cell.PaddingRight = 5f;
                    cell.PaddingBottom = 5f;
                    splitInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("تاریخ نصب : {0}", bill.Split.InstallDate.HasValue ? bill.Split.InstallDate.Value.PersianDateN("/") : ""), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BorderWidthBottom = 0.01f;
                    cell.PaddingRight = 5f;
                    cell.PaddingBottom = 5f;
                    splitInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("تاریخ صدور : {0}", bill.issueDate.HasValue ? bill.issueDate.Value.PersianDateN("/") : ""), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.PaddingRight = 5f;
                    cell.Colspan = 2;
                    splitInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("آدرس : تهران - صادقیه ، خیابان کوهدشت پلاک 12 واحد 5"), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    cell.Colspan = 2;
                    splitInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("واحد صورتحساب"), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    cell.Colspan = 2;
                    splitInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("واحد حوادث : 34623537-011"), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    cell.Colspan = 2;
                    splitInfoTable.AddCell(cell);

                    var infoCell = new PdfPCell(splitInfoTable);
                    infoCell.BorderWidth = 0f;
                    infoCell.Colspan = 2;
                    table.AddCell(infoCell);

                    nestedTable = new PdfPTable(8);
                    nestedTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    nestedTable.DefaultCell.BorderWidth = 0.01f;
                    nestedTable.DefaultCell.PaddingBottom = 5f;
                    nestedTable.WidthPercentage = 100f;
                    nestedTable.SetWidths(new[] { 12, 20, 10, 10, 10, 10, 13, 15 });
                    nestedTable.DefaultCell.Border = Rectangle.NO_BORDER;

                    var list1 = new List<int> { 10, 12, 14, 16, 18, 19, 22, 26 };
                    var list2 = new List<int> { 1, 2, 3, 4, 5, 7, 8 };

                    var consumeTable = new PdfPTable(4);
                    consumeTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    consumeTable.DefaultCell.BorderWidth = 0.01f;
                    consumeTable.DefaultCell.PaddingBottom = 5f;
                    consumeTable.WidthPercentage = 100f;
                    if (list1.Contains(bill.TariffID))
                        consumeTable.SetWidths(new[] { 30, 17, 23, 30 });
                    else if (list2.Contains(bill.TariffID))
                        consumeTable.SetWidths(new[] { 30, 15, 15, 40 });
                    consumeTable.DefaultCell.Border = Rectangle.NO_BORDER;

                    if (list1.Contains(bill.TariffID))
                    {
                        //Header
                        cell = new PdfPCell(new Phrase("مبلغ - ریال", HeaderFont));
                        cell.Padding = 1f;
                        cell.PaddingBottom = 5f;
                        cell.BorderWidth = 0.01f;
                        cell.BackgroundColor = new BaseColor(240, 240, 240);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        consumeTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase("مصرف", HeaderFont));
                        cell.Padding = 1f;
                        cell.PaddingBottom = 5f;
                        cell.BackgroundColor = new BaseColor(240, 240, 240);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        consumeTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase("نرخ - ریال", HeaderFont));
                        cell.Padding = 1f;
                        cell.PaddingBottom = 5f;
                        cell.BackgroundColor = new BaseColor(240, 240, 240);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        consumeTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase("زمان مصرف", HeaderFont));
                        cell.Padding = 1f;
                        cell.PaddingBottom = 5f;
                        cell.BackgroundColor = new BaseColor(240, 240, 240);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        consumeTable.AddCell(cell);

                        //Mid
                        cell = new PdfPCell(new Phrase("میان باری", HeaderFont));
                        cell.Padding = 1f;
                        cell.PaddingBottom = 5f;
                        cell.BorderWidth = 0.01f;
                        cell.BackgroundColor = new BaseColor(247, 247, 222);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        consumeTable.AddCell(cell);

                        cell = new PdfPCell();
                        cell.Phrase = fontSelector.Process(bill.MidRate.ToString("#,##0"));
                        cell.PaddingBottom = 5f;
                        cell.BackgroundColor = new BaseColor(247, 247, 222);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        consumeTable.AddCell(cell);

                        cell = new PdfPCell();
                        cell.Phrase = fontSelector.Process(bill.MidUsed.ToString("#,##0"));
                        cell.PaddingBottom = 5f;
                        cell.BackgroundColor = new BaseColor(247, 247, 222);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        consumeTable.AddCell(cell);

                        cell = new PdfPCell();
                        cell.Phrase = fontSelector.Process(bill.MidCost.ToString("#,##0"));
                        cell.PaddingBottom = 5f;
                        cell.BackgroundColor = new BaseColor(247, 247, 222);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        consumeTable.AddCell(cell);

                        //Max
                        cell = new PdfPCell(new Phrase("اوج بار", HeaderFont));
                        cell.Padding = 1f;
                        cell.PaddingBottom = 5f;
                        cell.BorderWidth = 0.01f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        consumeTable.AddCell(cell);

                        cell = new PdfPCell();
                        cell.Phrase = fontSelector.Process(bill.MaxRate.ToString("#,##0"));
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        consumeTable.AddCell(cell);

                        cell = new PdfPCell();
                        cell.Phrase = fontSelector.Process(bill.MaxUsed.ToString("#,##0"));
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        consumeTable.AddCell(cell);

                        cell = new PdfPCell();
                        cell.Phrase = fontSelector.Process(bill.MaxCost.ToString("#,##0"));
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        consumeTable.AddCell(cell);

                        //Min
                        cell = new PdfPCell(new Phrase("کم باری", HeaderFont));
                        cell.Padding = 1f;
                        cell.PaddingBottom = 5f;
                        cell.BorderWidth = 0.01f;
                        cell.BackgroundColor = new BaseColor(247, 247, 222);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        consumeTable.AddCell(cell);

                        cell = new PdfPCell();
                        cell.Phrase = fontSelector.Process(bill.MinRate.ToString("#,##0"));
                        cell.PaddingBottom = 5f;
                        cell.BackgroundColor = new BaseColor(247, 247, 222);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        consumeTable.AddCell(cell);

                        cell = new PdfPCell();
                        cell.Phrase = fontSelector.Process(bill.MinUsed.ToString("#,##0"));
                        cell.PaddingBottom = 5f;
                        cell.BackgroundColor = new BaseColor(247, 247, 222);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        consumeTable.AddCell(cell);

                        cell = new PdfPCell();
                        cell.Phrase = fontSelector.Process(bill.MinCost.ToString("#,##0"));
                        cell.PaddingBottom = 5f;
                        cell.BackgroundColor = new BaseColor(247, 247, 222);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        consumeTable.AddCell(cell);
                    }
                    else if (list2.Contains(bill.TariffID))
                    {
                        //Header
                        cell = new PdfPCell(new Phrase("پله های مصرف 30 روزه", HeaderFont));
                        cell.Padding = 1f;
                        cell.PaddingBottom = 5f;
                        cell.BorderWidth = 0.01f;
                        cell.BackgroundColor = new BaseColor(240, 240, 240);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        consumeTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase("نرخ", HeaderFont));
                        cell.Padding = 1f;
                        cell.PaddingBottom = 5f;
                        cell.BackgroundColor = new BaseColor(240, 240, 240);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        consumeTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase("مصرف", HeaderFont));
                        cell.Padding = 1f;
                        cell.PaddingBottom = 5f;
                        cell.BackgroundColor = new BaseColor(240, 240, 240);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        consumeTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase("بهای 30 روزه", HeaderFont));
                        cell.Padding = 1f;
                        cell.PaddingBottom = 5f;
                        cell.BackgroundColor = new BaseColor(240, 240, 240);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        consumeTable.AddCell(cell);

                        var billNormal = _billNormalService.GetByBillId(bill.Id);
                        var tariffRates = _billNormalService.GetTariffRates(bill.TariffID);

                        for (int i = 0; i < billNormal.Count; i++)
                        {
                            if (i % 2 == 0)
                            {
                                cell = new PdfPCell();
                                cell.Phrase = fontSelector.Process(billNormal[i].Description);
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell.FixedHeight = 20f;
                                consumeTable.AddCell(cell);

                                cell = new PdfPCell();
                                cell.Phrase = fontSelector.Process(billNormal[i].Cost.ToString());
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell.FixedHeight = 20f;
                                consumeTable.AddCell(cell);

                                cell = new PdfPCell();
                                cell.Phrase = fontSelector.Process(billNormal[i].Used.ToString("#.###"));
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell.FixedHeight = 20f;
                                consumeTable.AddCell(cell);

                                cell = new PdfPCell();
                                cell.Phrase = fontSelector.Process(billNormal[i].UsedCost.ToString("#.###"));
                                cell.PaddingBottom = 5f;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell.FixedHeight = 20f;
                                consumeTable.AddCell(cell);
                            }
                            else
                            {
                                cell = new PdfPCell();
                                cell.Phrase = fontSelector.Process(billNormal[i].Description);
                                cell.PaddingBottom = 5f;
                                cell.BackgroundColor = new BaseColor(247, 247, 222);
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell.FixedHeight = 20f;
                                consumeTable.AddCell(cell);

                                cell = new PdfPCell();
                                cell.Phrase = fontSelector.Process(billNormal[i].Cost.ToString());
                                cell.PaddingBottom = 5f;
                                cell.BackgroundColor = new BaseColor(247, 247, 222);
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell.FixedHeight = 20f;
                                consumeTable.AddCell(cell);

                                cell = new PdfPCell();
                                cell.Phrase = fontSelector.Process(billNormal[i].Used.ToString("#.###"));
                                cell.PaddingBottom = 5f;
                                cell.BackgroundColor = new BaseColor(247, 247, 222);
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell.FixedHeight = 20f;
                                consumeTable.AddCell(cell);

                                cell = new PdfPCell();
                                cell.Phrase = fontSelector.Process(billNormal[i].UsedCost.ToString("#.###"));
                                cell.PaddingBottom = 5f;
                                cell.BackgroundColor = new BaseColor(247, 247, 222);
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell.FixedHeight = 20f;
                                consumeTable.AddCell(cell);
                            }
                        }
                    }

                    var nestingCell = new PdfPCell(consumeTable);
                    nestingCell.BorderWidth = 0f;
                    nestingCell.Colspan = 5;
                    nestedTable.AddCell(nestingCell);

                    var formulaTable = new PdfPTable(1);
                    formulaTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    formulaTable.DefaultCell.BorderWidth = 0.01f;
                    formulaTable.DefaultCell.PaddingBottom = 5f;
                    formulaTable.WidthPercentage = 100f;
                    formulaTable.SetWidths(new[] { 100 });
                    formulaTable.DefaultCell.Border = Rectangle.NO_BORDER;

                    cell = new PdfPCell(new Phrase("فرمول", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    formulaTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("مبلغ ماهانه" + "\r\n" + "*" + "\r\n" + " تعداد روز" + "\r\n" + "/" + "\r\n" + "30"), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    formulaTable.AddCell(cell);

                    var formulaCell = new PdfPCell(formulaTable);
                    nesthousing.BorderWidth = 0f;
                    nestedTable.AddCell(formulaCell);

                    var costTable = new PdfPTable(1);
                    costTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    costTable.DefaultCell.BorderWidth = 0.01f;
                    costTable.DefaultCell.PaddingBottom = 5f;
                    costTable.WidthPercentage = 100f;
                    costTable.SetWidths(new[] { 100 });
                    costTable.DefaultCell.Border = Rectangle.NO_BORDER;

                    if (bill.Split.NetworkCost)
                    {
                        cell = new PdfPCell(new Phrase(string.Format("هزینه نگهداری شبکه"), font));
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.Border = Rectangle.NO_BORDER;
                        costTable.AddCell(cell);
                    }

                    cell = new PdfPCell(new Phrase(string.Format("آبونمان"), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    costTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("بهای برق دوره"), redFont));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.PaddingBottom = 7f;
                    costTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("تخفیف"), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    costTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("مالیات"), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    costTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("عوارض"), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    costTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("بدهی قبلی"), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    costTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("جریمه دیرکرد"), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    costTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("کسرهزارریال"), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    costTable.AddCell(cell);

                    var costCell = new PdfPCell(costTable);
                    costCell.BorderWidth = 0f;
                    nestedTable.AddCell(costCell);

                    costTable = new PdfPTable(1);
                    costTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    costTable.DefaultCell.BorderWidth = 0.01f;
                    costTable.DefaultCell.PaddingBottom = 5f;
                    costTable.WidthPercentage = 100f;
                    costTable.SetWidths(new[] { 100 });
                    costTable.DefaultCell.Border = Rectangle.NO_BORDER;

                    if (bill.Split.NetworkCost)
                    {
                        cell = new PdfPCell(new Phrase(bill.NetworkCost.ToString("#,##0"), font));
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.Border = Rectangle.NO_BORDER;
                        cell.BorderWidthRight = 0.01f;
                        costTable.AddCell(cell);
                    }

                    var subscription = (long)((bill.AllDays / 30) * 10000);
                    cell = new PdfPCell(new Phrase(subscription.ToString("#,##0"), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BorderWidthRight = 0.01f;
                    costTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.Total.ToString("#,##0"), font));
                    cell.PaddingBottom = 7f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    costTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.Discount.ToString("#,##0"), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BorderWidthRight = 0.01f;
                    costTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.Tax.ToString("#,##0"), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BorderWidthRight = 0.01f;
                    costTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.Toll.ToString("#,##0"), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BorderWidthRight = 0.01f;
                    costTable.AddCell(cell);

                    if (withDebit == true)
                    {
                        cell = new PdfPCell(new Phrase(bill.Debit.ToString("#,##0"), font));
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.Border = Rectangle.NO_BORDER;
                        cell.BorderWidthRight = 0.01f;
                        costTable.AddCell(cell);
                    }
                    else
                    {
                        cell = new PdfPCell(new Phrase("0", font));
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.Border = Rectangle.NO_BORDER;
                        cell.BorderWidthRight = 0.01f;
                        costTable.AddCell(cell);
                    }

                    cell = new PdfPCell(new Phrase(bill.LatePenalty.ToString("#,##0"), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BorderWidthRight = 0.01f;
                    costTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.Round.ToString("#,##0"), font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BorderWidthRight = 0.01f;
                    costTable.AddCell(cell);

                    costCell = new PdfPCell(costTable);
                    costCell.BorderWidth = 0f;
                    nestedTable.AddCell(costCell);

                    nesthousing = new PdfPCell(nestedTable);
                    nesthousing.BorderWidth = 0f;
                    table.AddCell(nesthousing);

                    #endregion

                    #region footer info row 1

                    splitInfoTable = new PdfPTable(2);
                    splitInfoTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    splitInfoTable.DefaultCell.BorderWidth = 0.01f;
                    splitInfoTable.DefaultCell.PaddingBottom = 5f;
                    splitInfoTable.WidthPercentage = 100f;
                    splitInfoTable.SetWidths(new[] { 50, 50 });
                    splitInfoTable.DefaultCell.Border = Rectangle.NO_BORDER;

                    var distributionArea = _distributionAreaService.GetDistributionAreaById(bill.Split.DistributionArea_Id).Name;
                    cell = new PdfPCell(new Phrase(string.Format("منطقه برق : {0}", distributionArea), titleFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.PaddingRight = 5f;
                    cell.PaddingBottom = 5f;
                    cell.Colspan = 2;
                    splitInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("شناسه انشعاب", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    splitInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("شناسه واریز", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    splitInfoTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.SplitID.ToString(), font));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    splitInfoTable.AddCell(cell);

                    if (withDebit == true)
                    {
                        cell = new PdfPCell(new Phrase(bill.PaymentID, font));
                        cell.Padding = 1f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        splitInfoTable.AddCell(cell);
                    }
                    else
                    {
                        cell = new PdfPCell(new Phrase(bill.PaymentIdWD, font));
                        cell.Padding = 1f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        splitInfoTable.AddCell(cell);
                    }

                    infoCell = new PdfPCell(splitInfoTable);
                    infoCell.BorderWidth = 0f;
                    infoCell.Colspan = 2;
                    table.AddCell(infoCell);

                    //discount and penalty table
                    nestedTable = new PdfPTable(8);
                    nestedTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    nestedTable.DefaultCell.BorderWidth = 0.01f;
                    nestedTable.DefaultCell.PaddingBottom = 5f;
                    nestedTable.WidthPercentage = 100f;
                    nestedTable.SetWidths(new[] { 12, 20, 10, 10, 10, 10, 13, 15 });
                    nestedTable.DefaultCell.Border = Rectangle.NO_BORDER;

                    var footerTable = new PdfPTable(5);
                    footerTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    footerTable.DefaultCell.BorderWidth = 0.01f;
                    footerTable.DefaultCell.PaddingBottom = 5f;
                    footerTable.WidthPercentage = 100f;
                    footerTable.SetWidths(new[] { 15, 25, 13, 13, 34 });

                    //rows number 1
                    cell = new PdfPCell(new Phrase("شرح مصارف", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 7f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    footerTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("نرخ", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 7f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    footerTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("مصرف", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 7f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    footerTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("شرح مبلغ", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 7f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    footerTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("مبلغ", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 7f;
                    cell.BackgroundColor = new BaseColor(240, 240, 240);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    footerTable.AddCell(cell);

                    //rows number 2
                    cell = new PdfPCell(new Phrase("میزان مصرف در اوج بار", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    footerTable.AddCell(cell);

                    var maxrate = bill.FineCost / bill.NormalMaxUsed;
                    cell = new PdfPCell(new Phrase(maxrate.ToString("#,##0"), font));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    footerTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.NormalMaxUsed.ToString("#,##0"), font));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    footerTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("جریمه اوج بار", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    footerTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.FineCost.ToString("#,##0"), font));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    footerTable.AddCell(cell);

                    //rows number 3
                    cell = new PdfPCell(new Phrase("میزان مصرف در غیر اوج بار", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    footerTable.AddCell(cell);

                    var mindRate = bill.DiscountCost / bill.NormalMindUsed;
                    cell = new PdfPCell(new Phrase(mindRate.ToString("#,##0"), font));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    footerTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.NormalMindUsed.ToString("#,##0"), font));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    footerTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase("تخفیف غیر اوج بار", HeaderFont));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    footerTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(bill.DiscountCost.ToString("#,##0"), font));
                    cell.Padding = 1f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    footerTable.AddCell(cell);

                    nestingCell = new PdfPCell(footerTable);
                    nestingCell.BorderWidth = 0f;
                    nestingCell.Colspan = 6;
                    nestedTable.AddCell(nestingCell);

                    //paymentTable column 1
                    costTable = new PdfPTable(1);
                    costTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    costTable.DefaultCell.BorderWidth = 0.01f;
                    costTable.DefaultCell.PaddingBottom = 5f;
                    costTable.WidthPercentage = 100f;
                    costTable.SetWidths(new[] { 100 });
                    costTable.DefaultCell.Border = Rectangle.NO_BORDER;

                    cell = new PdfPCell(new Phrase(string.Format("m"), redFont));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    costTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("قابل پرداخت"), redFont));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    costTable.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("مهلت پرداخت"), redFont));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    costTable.AddCell(cell);

                    var paymentCell = new PdfPCell(costTable);
                    paymentCell.BorderWidth = 0f;
                    nestedTable.AddCell(paymentCell);

                    //paymentTable column 2
                    costTable = new PdfPTable(1);
                    costTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    costTable.DefaultCell.BorderWidth = 0.01f;
                    costTable.DefaultCell.PaddingBottom = 5f;
                    costTable.WidthPercentage = 100f;
                    costTable.SetWidths(new[] { 100 });
                    costTable.DefaultCell.Border = Rectangle.NO_BORDER;

                    cell = new PdfPCell(new Phrase(string.Format("m"), redFont));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BorderWidthRight = 0.01f;
                    costTable.AddCell(cell);

                    if (withDebit)
                    {
                        cell = new PdfPCell(new Phrase(bill.Payable.ToString("#,##0"), redFont));
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        costTable.AddCell(cell);
                    }
                    else
                    {
                        var payableValue = bill.Payable - bill.Debit;
                        cell = new PdfPCell(new Phrase(payableValue.ToString("#,##0"), redFont));
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        costTable.AddCell(cell);
                    }

                    cell = new PdfPCell(new Phrase(bill.ExpDate.HasValue ? bill.ExpDate.Value.PersianDateNormal("/") : "", font));
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    costTable.AddCell(cell);

                    paymentCell = new PdfPCell(costTable);
                    paymentCell.BorderWidth = 0f;
                    nestedTable.AddCell(paymentCell);

                    var footerCell = new PdfPCell(nestedTable);
                    footerCell.BorderWidth = 0f;
                    table.AddCell(footerCell);

                    #endregion

                    #region last row

                    cell = new PdfPCell(new Phrase(string.Format("شماره حساب - بانک ملت : 4299775653"), font));
                    cell.PaddingTop = 5f;
                    cell.PaddingRight = 10f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_TOP;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Colspan = 2;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("مشترک گرامی : {0}", bill.Description), font));
                    cell.PaddingBottom = 5f;
                    cell.PaddingRight = 7f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_TOP;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.BorderWidthRight = 0.01f;
                    table.AddCell(cell);

                    #endregion

                    #region Financial code

                    cell = new PdfPCell(new Phrase("کد اقتصادی : 411395346864", HeaderFont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_TOP;
                    cell.Border = Rectangle.NO_BORDER;
                    cell.Rotation = 90;
                    cell.PaddingTop = 20f;
                    table.AddCell(cell);

                    #endregion

                    mainTable.AddCell(table);
                    mainTable.AddCell(cell);

                    doc.Add(mainTable);

                    if (bill.Debit > bill.AllCost && withDebit)
                    {
                        var descriptionTable = new PdfPTable(1);
                        descriptionTable.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                        descriptionTable.DefaultCell.Border = PdfPCell.NO_BORDER;
                        descriptionTable.WidthPercentage = 100f;
                        descriptionTable.DefaultCell.Padding = 0f;
                        descriptionTable.SpacingBefore = 0.5f;

                        String text = "مشترک گرامی، در صورت عدم پرداخت صورتحساب تا مهلت مقرر، نسبت به قطع برق انشعاب، اقدام خواهد گردید";
                        cell = new PdfPCell(new Phrase(text, redFont));
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.Border = Rectangle.NO_BORDER;
                        cell.PaddingRight = 85f;
                        descriptionTable.AddCell(cell);
                        doc.Add(descriptionTable);
                    }
                }


                billNum++;
                if (billNum < billCount)
                {
                    doc.NewPage();
                }

            }
            doc.Close();
        }

        public string GetTariff(string txt)
        {
            string ret;
            switch (txt)
            {
                case "1":
                    ret = "مصارف خانگی";
                    break;
                case "2":
                    ret = "مصارف عمومی";
                    break;
                case "3":
                    ret = "مصارف کشاورزی";
                    break;
                case "4":
                    ret = "مصارف صنعتی";
                    break;
                case "5":
                    ret = "سایر مصارف";
                    break;
                default:
                    ret = "";
                    break;
            }
            return ret;
        }

        #endregion
    }

    class RoundedBorderMain : IPdfPCellEvent
    {
        public void CellLayout(PdfPCell cell, Rectangle rect, PdfContentByte[] canvas)
        {
            PdfContentByte cb = canvas[PdfPTable.BACKGROUNDCANVAS];
            cb.RoundRectangle(
              rect.Left + 1.5f,
              rect.Bottom + 1.5f,
              rect.Width - 3,
              rect.Height - 3, 4
            );
            cb.Stroke();
        }
    }
}