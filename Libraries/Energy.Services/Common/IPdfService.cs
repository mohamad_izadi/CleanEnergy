using System.Collections.Generic;
using System.IO;
using Energy.Core.Domain.Bills;

namespace Energy.Services.Common
{
    /// <summary>
    /// Pdf service interface
    /// </summary>
    public partial interface IPdfService
    {
        /// <summary>
        /// Print a bill to PDF
        /// </summary>
        /// <param name="bill">Bill</param>
        /// <returns>A path of generated file</returns>
        void PrintBillToPdf(Stream stream, Bill bill, bool withDebit = true);

        /// <summary>
        /// Print orders to PDF
        /// </summary>
        /// <param name="stream">Stream</param>
        /// <param name="bills">Bills</param>
        void PrintBillsToPdf(Stream stream, IList<Bill> bills, bool withDebit = true);
    }
}