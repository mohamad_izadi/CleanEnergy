﻿
namespace Energy.Core
{
    public partial interface ICreditContext
    {
        bool HasCredit { get; }
        int Credit { get; }
        CreditType CreditType { get; }
        bool AuthorizeDownload { get; }

        void ResetCache();
    }
    public enum CreditType
    {
        IpCustomer = 0,
        HasCredit = 1,
        NotRegistred = 2,
        NoCredit = 3,
    }
}
