﻿using System;
using System.Globalization;

namespace Energy.Core
{
    public static class DateTimeExtension
    {
        public static string ToPerianDateTimeFormat(this DateTime date, string format = "yyyy/MM/dd")
        {
            return new PersianDateTime(date).ToString(format);
        }
        public static string ToPersianLongDateTimeString(this DateTime date)
        {
            return new PersianDateTime(date).ToString("dddd، dd MMMM yyyy hh:mm:ss tt");
        }
        public static string ToPersianDateTimeString(this DateTime date)
        {
            return new PersianDateTime(date).ToString("yyyy/MM/dd hh:mm:ss tt");
        }

        public static DateTime ToDateTime(this string date)
        {
            PersianDateTime value;
            if (PersianDateTime.TryParse(date, out value))
                return value.ToDateTime();
            else
                throw new FormatException("DateTime format is invalid !");
        }
        public static PersianDateTime ToPersianDateTime(this DateTime date)
        {
            return new PersianDateTime(date);
        }

        public static string PersianDateN(this DateTime dt, string chr)
        {
            string ret = "";
            if (dt != null)
            {
                var t = (DateTime)dt;
                var pd = new PersianCalendar();
                int n = pd.GetYear(t);
                n = n * 100 + pd.GetMonth(t);
                n = n * 100 + pd.GetDayOfMonth(t);
                ret = n.ToString().Trim();
                ret = ret.Substring(ret.Length - 2) + chr +
                      ret.Substring(ret.Length - 4, 2) + chr +
                      ret.Substring(0, ret.Length - 4);
            };
            return ret;
        }

        public static string PersianDateNormal(this DateTime dt, string chr)
        {
            string ret = "";
            if (dt != null)
            {
                var t = (DateTime)dt;
                var pd = new PersianCalendar();
                int n = pd.GetYear(t);
                n = n * 100 + pd.GetMonth(t);
                n = n * 100 + pd.GetDayOfMonth(t);
                ret = n.ToString().Trim();
                ret = ret.Substring(0, ret.Length - 4) + chr +
                      ret.Substring(ret.Length - 4, 2) + chr +
                      ret.Substring(ret.Length - 2);
            };
            return ret;
        }

        public static string GetTariff(this string txt)
        {
            string ret;
            switch (txt)
            {
                case "1":
                    ret = "مصارف خانگی";
                    break;
                case "2":
                    ret = "مصارف عمومی";
                    break;
                case "3":
                    ret = "مصارف کشاورزی";
                    break;
                case "4":
                    ret = "مصارف صنعتی";
                    break;
                case "5":
                    ret = "سایر مصارف";
                    break;
                default:
                    ret = "";
                    break;
            }
            return ret;
        }
    }
}
