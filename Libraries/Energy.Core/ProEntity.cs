using System;

namespace Energy.Core
{
    /// <summary>
    /// Base class for entities
    /// </summary>
    public abstract partial class ProEntity
    {
        /// <summary>
        /// Gets or sets the entity identifier
        /// </summary>
        public virtual int Id { get; set; }

        public override bool Equals(object obj)
        {
            return Equals(obj as ProEntity);
        }

        private static bool IsTransient(ProEntity obj)
        {
            return obj != null && Equals(obj.Id, default(int));
        }

        private Type GetUnproxiedType()
        {
            return GetType();
        }

        public virtual bool Equals(ProEntity other)
        {
            if (other == null)
                return false;

            if (ReferenceEquals(this, other))
                return true;

            if (!IsTransient(this) &&
                !IsTransient(other) &&
                Equals(Id, other.Id))
            {
                var otherType = other.GetUnproxiedType();
                var thisType = GetUnproxiedType();
                return thisType.IsAssignableFrom(otherType) ||
                        otherType.IsAssignableFrom(thisType);
            }

            return false;
        }

        public override int GetHashCode()
        {
            if (Equals(Id, default(int)))
                return base.GetHashCode();
            return Id.GetHashCode();
        }

        public static bool operator ==(ProEntity x, ProEntity y)
        {
            return Equals(x, y);
        }

        public static bool operator !=(ProEntity x, ProEntity y)
        {
            return !(x == y);
        }
    }
}
