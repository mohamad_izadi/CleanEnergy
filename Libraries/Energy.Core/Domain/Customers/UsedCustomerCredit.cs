﻿namespace Energy.Core.Domain.Customers
{
    public partial class UsedCustomerCredit
    {
        public int Id { get; set; }
        public bool IsUsed { get; set; }
    }
}
