
namespace Energy.Core.Domain.Customers
{
    public static partial class SystemCustomerRoleNames
    {
        public static string Administrators { get { return "Administrators"; } }
        
        public static string Registered { get { return "Registered"; } }
        
        public static string Guests { get { return "Guests"; } }

        public static string Customers { get { return "Customer"; } }
    }
}