﻿using System;

namespace Energy.Core.Domain.Customers
{
    public partial class CustomerCredit : BaseEntity
    {
        public Guid Code { get; set; }
        public short DownloadCredit { get; set; }
        public bool IsGift { get; set; }
    }
}
