﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Energy.Core.Domain.Splits;

namespace Energy.Core.Domain.Tariffs
{
    class TariffDetail
    {
        [ForeignKey("TariffID")]
        public virtual Tariff Tariff { get; set; }
        public virtual Int32 TariffID { get; set; }

        [Display(Name = "از روز")]
        public virtual Int32 FromDay { get; set; }
        [Display(Name = " ماه")]
        public virtual Int32 FromMonth { get; set; }
        [Display(Name = "تا روز")]
        public virtual Int32 ToDay { get; set; }
        [Display(Name = " ماه")]
        public virtual Int32 ToMonth { get; set; }
        [Display(Name = "ضریب")]
        public virtual Double Factor { get; set; }
        public virtual ICollection<SplitArea> SplitAreas { get; set; }
        public virtual String Title { get; set; }
        public virtual String Description { get; set; }
    }
}
