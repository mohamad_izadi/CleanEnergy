﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Energy.Core.Domain.Constant;

namespace Energy.Core.Domain.Tariffs
{
    public class Tariff : BaseEntity
    {   // ID, Titleتعرفه, Descriptionعنوان

        public int CTariff { get; set; }

        [Display(Name = "حداکثر بهای انرژی")]
        public Int32 LimitCostKwh { get; set; }

        [Display(Name = "جریمه اوج بار")]
        public Double MaxFine { get; set; }
        [Display(Name = "تخفیف کم باری")]
        public Double MinDiscount { get; set; }
        [Display(Name = "تخفیف غیراوج بار")]
        public Double NoMaxDiscount { get; set; }
        [Display(Name = "ضریب جریمه و تخفیف ")]
        public Double Factor { get; set; }

        [Display(Name = "بهای قدرت")]
        public Double PowerCost { get; set; }

        [Display(Name = "مبلغ میان باری")]
        public Double MidCost { get; set; }
        [Display(Name = "مبلغ اوج بار")]
        public Double MaxCost { get; set; }
        [Display(Name = "مبلغ کم باری")]
        public Double MinCost { get; set; }

        public String Title { get; set; }

        public String Description { get; set; }



    }

}

