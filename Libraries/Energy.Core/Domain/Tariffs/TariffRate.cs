﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Energy.Core.Domain.Tariffs
{
    public class TariffRate : BaseEntity
    {   // ID, Title, Description
        [ForeignKey("TariffID")]
        public virtual Tariff Tariff { get; set; }
        public virtual Int32 TariffID { get; set; }

        [Display(Name = "از Kwh/m")]
        public virtual Int32 FromKwhm { get; set; }
        [Display(Name = "تا Kwh/m")]
        public virtual Int32 ToKwhm { get; set; }
        [Display(Name = "مبلغ")]
        public virtual Int32 Cost { get; set; }

        public virtual String Title { get; set; }

        public virtual String Description { get; set; }
    }
}
