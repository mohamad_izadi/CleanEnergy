﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Energy.Core.Domain.Splits;

namespace Energy.Core.Domain.Tariffs
{
    public partial class TariffArea : BaseEntity
    {
        public virtual Tariff Tariff { get; set; }
        public int Tariff_Id { get; set; }

        [Display(Name = "از روز")]
        public int FromDay { get; set; }
        [Display(Name = " ماه")]
        public int FromMonth { get; set; }
        [Display(Name = "تا روز")]
        public int ToDay { get; set; }
        [Display(Name = " ماه")]
        public int ToMonth { get; set; }
        [Display(Name = "ضریب")]
        public Double Factor { get; set; }
        public ICollection<SplitArea> SplitAreas { get; set; }
        public String Title { get; set; }
        public String Description { get; set; }

    }
}
