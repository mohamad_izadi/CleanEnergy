﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Energy.Core.Domain.Tariffs;

namespace Energy.Core.Domain.Splits
{
    public class SplitArea : BaseEntity
    {
        [ForeignKey("SplitID")]
        public virtual Split Split { get; set; }
        public virtual Int32 SplitID { get; set; }

        [ForeignKey("TariffAreaID")]
        public virtual TariffArea TariffArea { get; set; }
        public virtual Int32 TariffAreaID { get; set; }

        //- use tempraury for calculates -//
        [Display(Name = "روزهای گرم")]
        public virtual Double WarmDays { get; set; }
        [Display(Name = "ضریب روزهای گرم")]
        public virtual Double WarmDaysFactor { get; set; }
        [Display(Name = "تعداد روزهای گرم")]
        public virtual Double WarmDaysX { get; set; }
        [Display(Name = "مصرف روزهای گرم")]
        public virtual Double WarmUsed { get; set; }
        [Display(Name = "متوسط مصرف روزهای گرم")]
        public virtual Double WarmUsedAvrg { get; set; }
        [Display(Name = "متوسط مبلغ روزهای گرم")]
        public virtual Double WarmCostAvrg { get; set; }
        [Display(Name = " مبلغ روزهای گرم")]
        public virtual Double WarmCost { get; set; }
    }
}
