﻿using System;
using System.Collections.Generic;
using Energy.Core.Domain.Constant;
using Energy.Core.Domain.Directory;


namespace Energy.Core.Domain.Splits
{
    public class Split : BaseEntity
    {
        public int Customer_Id { get; set; }
        public String AccountNo { get; set; }
        public int Apply { get; set; }
        public int Legal { get; set; }
        public int CTariffId { get; set; }
        public int Permanent { get; set; }
        public DateTime? TempFromDate { get; set; }
        public DateTime? TempToDate { get; set; }
        public bool ActivityPermission { get; set; }
        public int Demand { get; set; }
        public int Volt { get; set; }
        public int Phase { get; set; }
        public int Amp { get; set; }
        public int Power { get; set; }
        public String PowerTag { get; set; }
        public DateTime? PowerTagDate { get; set; }
        public bool IsFreelance { get; set; }
        public String FileNo { get; set; }
        public String DisPass { get; set; }
        public int SeqNo { get; set; }
        public int Urban { get; set; }
        public String Address { get; set; }
        public String AddressContact { get; set; }
        public String Postal { get; set; }
        //- کنتور -//
        public String SerialNo { get; set; }
        public int AD { get; set; }
        public String Maker { get; set; }
        public String VFactor { get; set; }
        public String AFactor { get; set; }
        public int Factor { get; set; }
        public int Timer { get; set; }
        public int Digits { get; set; }
        public DateTime? InstallDate { get; set; }
        public bool Active { get; set; }
        public bool NetworkCost { get; set; }
        public DateTime? CreateDate { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Region_Id { get; set; }
        public int DistributionArea_Id { get; set; }

        public bool IncludeLateFees { get; set; }

    }
}
