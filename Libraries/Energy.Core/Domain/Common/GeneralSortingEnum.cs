﻿namespace Energy.Core.Domain.Common
{
    /// <summary>
    /// Represents the general sorting
    /// </summary>
    public enum GeneralSortingEnum
    {
        /// <summary>
        /// Default (none)
        /// </summary>
        Default = 0,
        /// <summary>
        /// Name: A to Z
        /// </summary>
        NameAsc = 1,
        /// <summary>
        /// Name: Z to A
        /// </summary>
        NameDesc = 2,
    }
}