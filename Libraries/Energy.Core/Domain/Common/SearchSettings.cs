﻿using Energy.Core.Configuration;

namespace Energy.Core.Domain.Common
{
    public partial class SearchSettings : ISettings
    {
        public bool AutoCompleteEnabled { get; set; }
        public int AutoCompleteMinimumLength { get; set; }
        public int RequiredMinimumLength { get; set; }
    }
}
