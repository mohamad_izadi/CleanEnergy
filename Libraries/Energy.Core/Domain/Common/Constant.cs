﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Energy.Core.Domain.Constant
{
    public enum CApply
    {
        [Display(Name = "تامین انشعاب")]
        Split=1,
        [Display(Name = "تامین برق")]
        Energy=2
    }
    public enum Clegal
    {
        [Display(Name = "حقیقی")]
        Person = 1,
        [Display(Name = "حقوقی")]
        Company= 2
    }
    public enum CTariff
    {
        [Display(Name = "مصارف خانگی")]
        Home = 1,
        [Display(Name = "مصارف عمومی")]
        General = 2,
        [Display(Name = "مصارف کشاورزی")]
        Agriculture = 3,
        [Display(Name = "مصارف صنعتی")]
        Industry = 4,
        [Display(Name = "سایر مصارف")]
        Other = 5
    }
    public enum CDemand
    {  
        [Display(Name = "عادی")]
        Normal = 1,
        [Display(Name = "دیماندی")]
        Demand = 2
    }
    public enum CPermanent
    {
        [Display(Name = "دائمی")]
        Permanent = 1,
        [Display(Name = "موقت")]
        Tempraury = 2
    }
    public enum CVolt
    {
        [Display(Name = "ثانویه 220 ولت ")]   //-Normal
        V220 = 1,
        [Display(Name = "ثانویه 400 ولت ")]   //-Normal / Demand
        V400 = 2,
        [Display(Name = "اولیه 11 کیلوولت ")]    //-Demand
        V11Kp = 3,
        [Display(Name = "ثانویه 11 کیلوولت ")]    //-Demand
        V11Ks = 4,
        [Display(Name = "اولیه 20 کیلوولت ")]    //-Demand
        V20Kp = 5,
        [Display(Name = "ثانویه 20 کیلوولت ")]    //-Demand
        V20Ks = 6
    }
    public enum CPhase
    {
        [Display(Name = "تک فاز")]
        P1 = 1,
        [Display(Name = "سه فاز")]
        P3 = 3
    }
    public enum CUrban
    {
        [Display(Name = "ضهری")]
        Urban = 1,
        [Display(Name = "روستایی")]
        Rural = 2
    }
    public enum CAD
    {
        [Display(Name = "آنالوگ")]
        Analog = 1,
        [Display(Name = "دیجیتال")]
        Digital = 2
    }
    public enum CTimer
    {
        [Display(Name = "تک زمانه")]
        Singletime = 1,
        [Display(Name = "دو زمانه")]
        Doubletime = 2,
        [Display(Name = "سه زمانه")]
        Tripletime = 3
    }

    public enum Cclass
    {
        Admin,
        Basic,
        Billing,
        Dept,
        Filing,
        Portal,
        Message,
        Split,
    }

    public  enum CFileTypes
    {
        View = 0,
        Update = 1,
        Insert = 2,
        Delete = -1
    }

}


