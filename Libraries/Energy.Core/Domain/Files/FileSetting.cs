﻿using Energy.Core.Configuration;

namespace Energy.Core.Domain.Files
{
    public class FileSetting : ISettings
    {
        public int MaximumFileSize { get; set; }
        public string LocalPath { get; set; }
    }
}
