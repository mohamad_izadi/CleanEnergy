﻿using Energy.Core.Domain.Requests;
using System.Collections.Generic;

namespace Energy.Core.Domain.Files
{
    public partial class File : BaseEntity
    {
        private ICollection<RequestFile> _requestFiles;
        public string FileType { get; set; }
        public string FileName { get; set; }

        public virtual ICollection<RequestFile> RequestFiles
        {
            get { return _requestFiles ?? (_requestFiles = new List<RequestFile>()); }
            protected set { _requestFiles = value; }
        }
    }
}
