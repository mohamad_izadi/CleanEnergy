﻿using Energy.Core.Domain.Constant;
using Energy.Core.Domain.Customers;
using System;
using System.Collections.Generic;

namespace Energy.Core.Domain.Requests
{
    public partial class Request : BaseEntity
    {
        private ICollection<RequestFile> _requestFiles;
        public int Customer_Id { get; set; }
        public virtual Customer Customer { get; set; }
        public int RequestType_Id { get; set; }
        public virtual RequestType RequestType { get; set; }
        public Nullable<int> Split_Id { get; set; }
        public int DistributionArea_Id { get; set; }
        public String Address { get; set; }
        public String AddressContact { get; set; }
        public String SplitPostalCode { get; set; }
        public int tariff_Id { get; set; }
        public CVolt Volt { get; set; }
        public CPhase Phase { get; set; }
        public int Amp { get; set; }
        public Int32 Power { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual ICollection<RequestFile> RequestFiles
        {
            get { return _requestFiles ?? (_requestFiles = new List<RequestFile>()); }
            protected set { _requestFiles = value; }
        }
    }
}
