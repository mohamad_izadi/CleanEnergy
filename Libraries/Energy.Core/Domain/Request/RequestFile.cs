﻿using Energy.Core.Domain.Files;
using Energy.Core.Domain.Requests;

namespace Energy.Core.Domain.Requests
{
    /// <summary>
    /// Represents a request files mapping
    /// </summary>
    public partial class RequestFile : BaseEntity
    {
        public int Request_Id { get; set; }
        public int File_Id { get; set; }
        public int? FileType { get; set; }
        public string Description { get; set; }
        public virtual File File { get; set; }
        public virtual Request Request { get; set; }
    }

}
