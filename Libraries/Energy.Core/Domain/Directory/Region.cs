using System.Collections.Generic;
using Energy.Core.Domain.Localization;
using Energy.Core.Domain.Stores;

namespace Energy.Core.Domain.Directory
{
    /// <summary>
    /// Represents a country
    /// </summary>
    public partial class Region : BaseEntity, ILocalizedEntity
    {
        private ICollection<DistributionArea> _distributionAreas;

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }

        /// </summary>
        public bool Published { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        public int DisplayOrder { get; set; }
       
        /// <summary>
        /// Gets or sets the state/provinces
        /// </summary>
        public virtual ICollection<DistributionArea> DistributionAreas
        {
            get { return _distributionAreas ?? (_distributionAreas = new List<DistributionArea>()); }
            protected set { _distributionAreas = value; }
        }
    }

}
