using Energy.Core;
using System.Collections.Generic;

namespace Energy.Core.Domain.Directory
{
    /// <summary>
    /// Represents a state
    /// </summary>
    public partial class State : BaseEntity
    {
        private ICollection<City> _cities;

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity is published
        /// </summary>
        public bool Published { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        public int DisplayOrder { get; set; }
       
        /// <summary>
        /// Gets or sets the cities
        /// </summary>
        public virtual ICollection<City> Cities
        {
            get { return _cities ?? (_cities = new List<City>()); }
            protected set { _cities = value; }
        }

    }

}
