
using Energy.Core.Domain.Localization;

namespace Energy.Core.Domain.Directory
{
    /// <summary>
    /// Represents a state/province
    /// </summary>
    public partial class DistributionArea : BaseEntity, ILocalizedEntity
    {
        /// <summary>
        /// Gets or sets the region identifier
        /// </summary>
        public int RegionId { get; set; }

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether the entity is published
        /// </summary>
        public bool Published { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Gets or sets the region
        /// </summary>
        public virtual Region Region { get; set; }
    }

}
