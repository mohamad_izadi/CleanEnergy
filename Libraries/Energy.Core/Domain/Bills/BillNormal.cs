﻿using System;
using Energy.Core;

namespace Energy.Core.Domain.Bills
{
    public class BillNormal : BaseEntity
    {   // ID, Title, Description
        public Int32 BillID { get; set; }
        public Int32 TariffID { get; set; }
        public Int32 FromKwhm { get; set; }
        public Int32 ToKwhm { get; set; }
        public Int32 Cost { get; set; }

        public Double Used { get; set; }
        public Double UsedCost { get; set; }
        public String Description { get; set; }

    }
}
