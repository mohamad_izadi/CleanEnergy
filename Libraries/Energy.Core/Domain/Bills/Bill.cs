﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Web.Mvc;
using Energy.Core.Domain.Splits;
using Energy.Core.Domain.Tariffs;


namespace Energy.Core.Domain.Bills
{
    public partial class Bill : BaseEntity
    {
        [ForeignKey("SplitID")]
        public virtual Split Split { get; set; }

        [AllowHtml]
        public virtual Int32 SplitID { get; set; }

        [AllowHtml]
        [Display(Name = "دوره")]
        public virtual Int32 Term { get; set; }

        [AllowHtml]
        [Display(Name = "سال")]
        public virtual Int32 Year { get; set; }
        public virtual DateTime DateReadPre { get; set; }
        public virtual Double MidReadPre { get; set; }
        public virtual Double MaxReadPre { get; set; }
        public virtual Double MinReadPre { get; set; }
        public virtual Double ReactReadPre { get; set; }
        public virtual Double PowerReadPre { get; set; }

        public virtual Int32 TariffID { get; set; }
        public virtual Int32 TariffAreaID { get; set; }

        [AllowHtml]
        [Display(Name = "تاریخ قرائت")]
        public virtual DateTime DateRead { get; set; }

        [AllowHtml]
        [Display(Name = "میان باری قرائت شده")]
        public virtual Double MidRead { get; set; }
        [Display(Name = "اوج بار قرائت شده")]
        public virtual Double MaxRead { get; set; }
        [Display(Name = "کم باری قرائت شده")]
        public virtual Double MinRead { get; set; }
        [Display(Name = "راکتیو قرائت شده")]
        public virtual Double ReactRead { get; set; }
        [Display(Name = "قدرت قرائت شده")]
        public virtual Double PowerRead { get; set; }
        [Display(Name = "جمعه باری قرائت شده")]
        public virtual Double FridayRead { get; set; }

        [Display(Name = "تعداد روزها")]
        public virtual Double AllDays { get; set; }
        [Display(Name = "مصرف میان باری")]
        public virtual Double MidUsed { get; set; }
        [Display(Name = "مصرف اوج بار")]
        public virtual Double MaxUsed { get; set; }
        [Display(Name = "مصرف کم باری")]
        public virtual Double MinUsed { get; set; }
        [Display(Name = "مصرف راکتیو ")]
        public virtual Double ReactUsed { get; set; }
        [Display(Name = "مصرف جمعه ")]
        public virtual Double FridayUsed { get; set; }
        [Display(Name = "جمع مصرف")]
        public virtual Double AllUsed { get; set; }
        [Display(Name = "پیک قدرت مصرفی")]
        public virtual Double PowerUsed { get; set; }

        [Display(Name = "جمع بها")]
        public virtual Int64 Total { get; set; }
        [Display(Name = "تخفیف")]
        public virtual Int64 Discount { get; set; }
        [Display(Name = "مالیات برارزش افزوده")]
        public virtual Int64 Tax { get; set; }
        [Display(Name = "عوارض")]
        public virtual Int64 Toll { get; set; }
        [Display(Name = "کسر 1000 ریال")]
        public virtual Int64 Round { get; set; }
        [Display(Name = "قابل پرداخت")]
        public virtual Int64 Payable { get; set; }
        [Display(Name = "مهلت پرداخت")]
        public virtual DateTime? ExpDate { get; set; }
        [Display(Name = "پرداخت شده")]
        public virtual Int64 Paid { get; set; }
        [Display(Name = "بدهی قبلی")]
        public virtual Int64 Debit { get; set; }
        [Display(Name = "تاریخ پرداخت")]
        public virtual DateTime? PaidDate { get; set; }

        [Display(Name = "شناسه پرداخت")]
        public virtual String PaymentID { get; set; }

        [Display(Name = "شناسه پرداخت")]
        public virtual String PaymentIdWD { get; set; }

        public virtual DateTime? EditDate { get; set; }

        public virtual DateTime? issueDate { get; set; }
        public virtual bool Fixed { get; set; }

        //---------------------- Bill out -----------------------//
        [Display(Name = "روزهای عادی")]
        public virtual Double NormalDays { get; set; }
        [Display(Name = "روزهای گرم")]
        public virtual Double WarmDays { get; set; }
        [Display(Name = "ضریب روزهای گرم")]
        public virtual Double WarmDaysFactor { get; set; }
        [Display(Name = "تعداد روزهای گرم")]
        public virtual Double WarmDaysX { get; set; }
        [Display(Name = "تعداد روز")]
        public virtual Double AllDaysX { get; set; }
        [Display(Name = "مصرف روزهای عادی")]
        public virtual Double NormalUsed { get; set; }
        [Display(Name = "مصرف روزهای گرم")]
        public virtual Double WarmUsed { get; set; }
        [Display(Name = "متوسط مصرف روزهای عادی")]
        public virtual Double NormalUsedAvrg { get; set; }
        [Display(Name = "متوسط مصرف روزهای گرم")]
        public virtual Double WarmUsedAvrg { get; set; }
        [Display(Name = "متوسط بهای روزهای عادی")]
        public virtual Double NormalCostAvrg { get; set; }
        [Display(Name = "متوسط بهای روزهای گرم")]
        public virtual Double WarmCostAvrg { get; set; }
        [Display(Name = " بهای روزهای عادی")]
        public virtual Double NormalCost { get; set; }
        [Display(Name = " بهای روزهای گرم")]
        public virtual Double WarmCost { get; set; }
        [Display(Name = " جمع بها ")]
        public virtual Int64 AllCost { get; set; }

        //-Fine:  if Timer > 1 -//
        [Display(Name = "مصرف اوج بارگرم")]
        public virtual Double WarmMaxUsed { get; set; }
        [Display(Name = "بهای جریمه اوج بارگرم")]
        public virtual Double WarmMaxFineCost { get; set; }

        [Display(Name = "مصرف اوج بارعادی")]
        public virtual Double NormalMaxUsed { get; set; }
        [Display(Name = "بهای جریمه اوج بارعادی")]
        public virtual Double NormalMaxFineCost { get; set; }

        [Display(Name = "بهای جریمه")]
        public virtual Int64 FineCost { get; set; }

        //-Discount: if Timer=2 : Mid , Timer=3 : Min
        [Display(Name = "مصرف کم باری گرم")]
        public virtual Double WarmMindUsed { get; set; }
        [Display(Name = "بهای تخفیف کم باری گرم")]
        public virtual Double WarmMindDiscountCost { get; set; }

        [Display(Name = "مصرف کم باری عادی")]
        public virtual Double NormalMindUsed { get; set; }
        [Display(Name = "بهای تخفیف کم باری عادی")]
        public virtual Double NormalMindDiscountCost { get; set; }

        [Display(Name = "بهای تخفیف")]
        public virtual Int64 DiscountCost { get; set; }

        //---------------------- Bill out 30kv ---------------------//
        [Display(Name = "نرخ میان باری")]
        public virtual Double MidRate { get; set; }
        [Display(Name = "نرخ اوج بار")]
        public virtual Double MaxRate { get; set; }
        [Display(Name = "نرخ کم باری")]
        public virtual Double MinRate { get; set; }
        [Display(Name = "نرخ راکتیو")]
        public virtual Double ReactRate { get; set; }

        [Display(Name = "بهای میان باری")]
        public virtual Double MidCost { get; set; }
        [Display(Name = "بهای اوج بار")]
        public virtual Double MaxCost { get; set; }
        [Display(Name = "بهای کم باری")]
        public virtual Double MinCost { get; set; }
        [Display(Name = "بهای راکتیو")]
        public virtual Double ReactCost { get; set; }

        [Display(Name = "بهای قدرت")]
        public virtual Double PowerCost { get; set; }
        [Display(Name = "قدرت مصرفی")]
        public virtual Double Power90 { get; set; }
        [Display(Name = "بهای قدرت مصرفی")]
        public virtual Int64 Power90Cost { get; set; }
        [Display(Name = "تخفیف صنعتی")]
        public virtual Int64 IndustDisCost { get; set; }
        [Display(Name = "بهای انشعاب آزاد")]
        public virtual Int64 FreelanceCost { get; set; }
        [Display(Name = "جریمه تجاوز از قدرت")]
        public virtual Int64 ExceedCost { get; set; }
        [Display(Name = "هزینه نگهداری شبکه")]
        public virtual Int64 NetworkCost { get; set; }
        [Display(Name = "جریمه مصرف غیرصنعتی")]
        public virtual Int64 UnIndustCost { get; set; }
        [Display(Name = "جریمه انقضای اعتبار پروانه")]
        public virtual Int64 TagExpireCost { get; set; }
        [Display(Name = "ضریب قدرت")]
        public virtual Double PowerFactor { get; set; }
        [Display(Name = "ضریب بدی مصرف")]
        public virtual Double LossFactor { get; set; }

        public virtual string Type { get; set; }

        [Display(Name = "جریمه دیرکرد")]
        public virtual double LatePenalty { get; set; }
        public virtual DateTime? LastPaymentDate1 { get; set; }
        public virtual Int64 LastPayment1 { get; set; }
        public virtual DateTime? LastPaymentDate2 { get; set; }
        public virtual Int64 LastPayment2 { get; set; }
        public virtual bool IsLastTerm { get; set; }
        public virtual int PictureId { get; set; }
        public virtual string PictureUrl { get; set; }
        public virtual String Title { get; set; }
        public virtual String Description { get; set; }
        public virtual String Emergency { get; set; }

        ////-- Use for Normal and Demand--//
        ////[Display(Name = "روزهای فصل")]
        ////public virtual Double WarmDays { get; set; }
        ////[Display(Name = " بهای فصل")]
        ////public virtual Double WarmCost { get; set; }

    }
}
