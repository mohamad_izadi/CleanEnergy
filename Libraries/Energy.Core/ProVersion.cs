﻿
namespace Energy.Core
{
    public static class ProVersion
    {
        /// <summary>
        /// Gets or sets the store version
        /// </summary>
        public static string CurrentVersion 
        {
            get
            {
                return "1.0";
            }
        }
    }
}
