﻿using System.Data.Entity.ModelConfiguration;
using Energy.Core.Domain.Files;

namespace Energy.Data.Mapping.Files
{
    public partial class FileMap : EntityTypeConfiguration<File>
    {
        public FileMap()
        {
            this.ToTable("File");
            this.HasKey(p => p.Id);
            this.Property(p => p.FileType).IsRequired().HasMaxLength(40);
            this.Property(p => p.FileName).HasMaxLength(300);
        }
    }
}