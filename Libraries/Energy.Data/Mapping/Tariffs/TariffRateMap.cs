﻿using System.Data.Entity.ModelConfiguration;
using Energy.Core.Domain.Tariffs;

namespace Energy.Data.Mapping.Tariffs
{
    public partial class TariffRateMap : EntityTypeConfiguration<TariffRate>
    {
        public TariffRateMap()
        {
            this.ToTable("TariffRates");
            this.HasKey(c => c.Id);
        }
    }
}
