﻿using System.Data.Entity.ModelConfiguration;
using Energy.Core.Domain.Tariffs;

namespace Energy.Data.Mapping.Tariffs
{
    public partial class TariffAreaMap : EntityTypeConfiguration<TariffArea>
    {
        public TariffAreaMap()
        {
            this.ToTable("TariffAreas");
            this.HasKey(c => c.Id);
            this.HasRequired(i => i.Tariff)
                .WithMany()
                .HasForeignKey(i => i.Tariff_Id);
        }
    }
}
