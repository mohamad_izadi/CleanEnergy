﻿using System.Data.Entity.ModelConfiguration;
using Energy.Core.Domain.Tariffs;

namespace Energy.Data.Mapping.Tariffs
{
    public partial class TariffMap : EntityTypeConfiguration<Tariff>
    {
        public TariffMap()
        {
            this.ToTable("Tariffs");
            this.HasKey(c => c.Id);
        }
    }
}
