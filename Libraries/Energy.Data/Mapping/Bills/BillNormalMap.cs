﻿using System.Data.Entity.ModelConfiguration;
using Energy.Core.Domain.Bills;

namespace Energy.Data.Mapping.Bills
{
    public partial class BillNormalMap : EntityTypeConfiguration<BillNormal>
    {
        public BillNormalMap()
        {
            this.ToTable("BillNormals");
            this.HasKey(c => c.Id);
        }
    }
}
