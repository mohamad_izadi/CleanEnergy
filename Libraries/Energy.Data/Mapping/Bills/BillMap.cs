﻿using System.Data.Entity.ModelConfiguration;
using Energy.Core.Domain.Bills;

namespace Energy.Data.Mapping.Bills
{
    public partial class BillMap : EntityTypeConfiguration<Bill>
    {
        public BillMap()
        {
            this.ToTable("Bills");
            this.HasKey(c => c.Id);
            this.Ignore(c => c.IsLastTerm);
            this.Ignore(c => c.PictureUrl);
        }
    }
}
