﻿using System.Data.Entity.ModelConfiguration;
using Energy.Core.Domain.Requests;

namespace Energy.Data.Mapping.Requests
{
    public partial class RequestFileMap : EntityTypeConfiguration<RequestFile>
    {
        public RequestFileMap()
        {
            this.ToTable("RequestFile_Mapping");
            this.HasKey(pp => pp.Id);

            this.HasRequired(pp => pp.File)
                .WithMany(p => p.RequestFiles)
                .HasForeignKey(pp => pp.File_Id);


            this.HasRequired(pp => pp.Request)
                .WithMany(p => p.RequestFiles)
                .HasForeignKey(pp => pp.Request_Id);
        }
    }
}