﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Energy.Core.Domain.Requests;

namespace Energy.Data.Mapping.Requests
{
    public partial class RequestTypeMap : EntityTypeConfiguration<RequestType>
    {
        public RequestTypeMap()
        {
            this.ToTable("RequestType");
            this.HasKey(alt => alt.Id);

            this.Property(alt => alt.SystemKeyword).IsRequired().HasMaxLength(100);
            this.Property(alt => alt.Name).IsRequired().HasMaxLength(200);
        }
    }
}
