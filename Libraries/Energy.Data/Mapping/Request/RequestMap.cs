﻿using System.Data.Entity.ModelConfiguration;
using Energy.Core.Domain.Requests;

namespace Energy.Data.Mapping.Requests
{
    public partial class RequestMap : EntityTypeConfiguration<Request>
    {
        public RequestMap()
        {
            this.ToTable("Request");
            this.HasKey(c => c.Id);

            this.HasRequired(al => al.RequestType)
                .WithMany()
                .HasForeignKey(al => al.RequestType_Id);

            this.HasRequired(al => al.Customer)
                .WithMany()
                .HasForeignKey(al => al.Customer_Id);
        }
    }
}
