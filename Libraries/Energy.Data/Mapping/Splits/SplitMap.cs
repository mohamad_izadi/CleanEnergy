﻿using System.Data.Entity.ModelConfiguration;
using Energy.Core.Domain.Splits;

namespace Energy.Data.Mapping.Splits
{
    public partial class SplitMap : EntityTypeConfiguration<Split>
    {
        public SplitMap()
        {
            this.ToTable("Splits");
            this.HasKey(c => c.Id);
            this.Property(c => c.InstallDate).IsRequired();
        }
    }
}
