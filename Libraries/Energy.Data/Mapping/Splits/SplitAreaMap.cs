﻿using System.Data.Entity.ModelConfiguration;
using Energy.Core.Domain.Splits;

namespace Energy.Data.Mapping.Splits
{
    public partial class SplitAreaMap : EntityTypeConfiguration<SplitArea>
    {
        public SplitAreaMap()
        {
            this.ToTable("SplitAreas");
            this.HasKey(c => c.Id);
        }
    }
}
