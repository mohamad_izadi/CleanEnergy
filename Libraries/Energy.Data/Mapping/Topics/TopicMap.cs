﻿using System.Data.Entity.ModelConfiguration;
using Energy.Core.Domain.Topics;

namespace Energy.Data.Mapping.Topics
{
    public class TopicMap : EntityTypeConfiguration<Topic>
    {
        public TopicMap()
        {
            this.ToTable("Topic");
            this.HasKey(t => t.Id);
        }
    }
}
