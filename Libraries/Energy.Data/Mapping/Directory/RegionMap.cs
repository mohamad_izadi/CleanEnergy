using Energy.Core.Domain.Directory;
using System.Data.Entity.ModelConfiguration;

namespace Nop.Data.Mapping.Directory
{
    public partial class RegionMap : EntityTypeConfiguration<Region>
    {
        public RegionMap()
        {
            this.ToTable("Region");
            this.HasKey(c =>c.Id);
            this.Property(c => c.Name).IsRequired().HasMaxLength(100);
        }
    }
}