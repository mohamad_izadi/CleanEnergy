using Energy.Core.Domain.Directory;
using System.Data.Entity.ModelConfiguration;

namespace Energy.Data.Mapping.Directory
{
    public partial class StateMap : EntityTypeConfiguration<State>
    {
        public StateMap()
        {
            this.ToTable("State");
            this.HasKey(c =>c.Id);
            this.Property(c => c.Name).IsRequired().HasMaxLength(100);
        }
    }
}