using Energy.Core.Domain.Directory;
using System.Data.Entity.ModelConfiguration;

namespace Energy.Data.Mapping.Directory
{
    public partial class DistributionAreaMap : EntityTypeConfiguration<DistributionArea>
    {
        public DistributionAreaMap()
        {
            this.ToTable("DistributionArea");
            this.HasKey(sp => sp.Id);
            this.Property(sp => sp.Name).IsRequired().HasMaxLength(100);


            this.HasRequired(sp => sp.Region)
                .WithMany(c => c.DistributionAreas)
                .HasForeignKey(sp => sp.RegionId);
        }
    }
}