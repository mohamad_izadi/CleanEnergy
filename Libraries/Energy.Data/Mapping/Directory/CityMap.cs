using Energy.Core.Domain.Directory;
using System.Data.Entity.ModelConfiguration;

namespace Energy.Data.Mapping.Directory
{
    public partial class CityMap : EntityTypeConfiguration<City>
    {
        public CityMap()
        {
            this.ToTable("City");
            this.HasKey(sp => sp.Id);
            this.Property(sp => sp.Name).IsRequired().HasMaxLength(100);


            this.HasRequired(sp => sp.State)
                .WithMany(c => c.Cities)
                .HasForeignKey(sp => sp.State_Id);
        }
    }
}