﻿using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;
using Energy.Core.Domain.Common;
using Energy.Core.Domain.Customers;

namespace Energy.Data.Mapping.Customers
{
    public partial class CustomerCreditMap : EntityTypeConfiguration<CustomerCredit>
    {
        public CustomerCreditMap()
        {
            this.ToTable("CustomerCredit");
            this.HasKey(cc => cc.Id);
            this.Property(cc => cc.Code)
                .IsRequired()
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_CustomerCredit_Code") { IsUnique = true }))
                /*.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)*/;

            this.Property(cc => cc.DownloadCredit).IsRequired();
            this.Property(cc => cc.IsGift).IsRequired();
        }
    }
}
