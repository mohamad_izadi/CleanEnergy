﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

using Energy.Core;
using Energy.Core.Infrastructure;
using Energy.Services.Localization;
using System.ComponentModel;
using System;
using System.Reflection;
using System.Linq;

namespace Energy.Web.Framework.Mvc
{
    public class ProModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var model = base.BindModel(controllerContext, bindingContext);
            if (model is BaseProModel)
            {
                ((BaseProModel)model).BindModel(controllerContext, bindingContext);
            }
            return model;
        }

        protected override void BindProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, System.ComponentModel.PropertyDescriptor propertyDescriptor)
        {
            var persianDateProperty = TryFindPersianDateProperty(propertyDescriptor);
            if (persianDateProperty != null)
            {
                try
                {
                    var model = bindingContext.Model;
                    PropertyInfo property = model.GetType().GetProperty(propertyDescriptor.Name);

                    var value = bindingContext.ValueProvider.GetValue(propertyDescriptor.Name);

                    if (value != null)
                    {
                        if (!string.IsNullOrWhiteSpace(value.AttemptedValue))
                        {
                            PersianDateTime date;
                            if (PersianDateTime.TryParse(value.AttemptedValue, out date))
                                property.SetValue(model, date.ToDateTime(), null);
                            else
                            {
                                var localizationService = EngineContext.Current.Resolve<ILocalizationService>();
                                bindingContext.ModelState.AddModelError(propertyDescriptor.Name, localizationService.GetResource("Common.Fields.PersianDateTime.InvalidFormat"));
                            }
                        }
                        else
                        {
                            property.SetValue(model, null, null);
                        }
                    }
                    else
                        base.BindProperty(controllerContext, bindingContext, propertyDescriptor);
                }
                catch
                {
                    bindingContext.ModelState.AddModelError("", "Binding Persian DateTime cause error !");
                }
            }
            else
                base.BindProperty(controllerContext, bindingContext, propertyDescriptor);
        }

        UIHintAttribute TryFindPersianDateProperty(PropertyDescriptor propertyDescriptor)
        {
            return propertyDescriptor.Attributes
              .OfType<UIHintAttribute>()
              .Where(x => x.UIHint.StartsWith("PersianDate", StringComparison.InvariantCultureIgnoreCase))
              .FirstOrDefault();
        }

        protected override System.ComponentModel.PropertyDescriptorCollection GetModelProperties(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            bindingContext.PropertyFilter = new System.Predicate<string>(pred);
            var values = base.GetModelProperties(controllerContext, bindingContext);
            return values;
        }

        protected bool pred(string target)
        {
            return true;
        }
    }
}
