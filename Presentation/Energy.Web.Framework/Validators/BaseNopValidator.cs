using FluentValidation;

namespace Energy.Web.Framework.Validators
{
    public abstract class BaseProValidator<T> : AbstractValidator<T> where T : class
    {
        protected BaseProValidator()
        {
            PostInitialize();
        }

        /// <summary>
        /// Developers can override this method in custom partial classes
        /// in order to add some custom initialization code to constructors
        /// </summary>
        protected virtual void PostInitialize()
        {
            
        }
    }
}