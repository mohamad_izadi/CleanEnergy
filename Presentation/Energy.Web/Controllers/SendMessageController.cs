﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Energy.Core.Domain.Customers;
using Energy.Services.Splits;
using Energy.Services.Bills;
using Energy.Services.Security;
using Energy.Services.Customers;
using Energy.Services.Common;
using Energy.Services.Messages;
using Energy.Web.Tools;
using Energy.Web.Models.Messages;
using Energy.Core.Domain.Messages;

namespace Energy.Web.Controllers
{
    public class SendMessageController : BaseAdminController
    {
        #region Fields

        private readonly ISplitService _splitService;
        private readonly IBillService _billService;
        private readonly ICustomerService _customerService;
        private readonly IPermissionService _permissionService;
        private readonly ISMSService _smsService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly IEmailSender _emailSender;

        #endregion

        #region Constructors
        public SendMessageController(
            ISplitService splitService,
            IBillService billService,
            ICustomerService customerService,
            IPermissionService permissionService,
            ISMSService smsService,
            IEmailAccountService emailAccountService,
            EmailAccountSettings emailAccountSettings,
            IEmailSender emailSender)
        {
            this._splitService = splitService;
            this._billService = billService;
            this._customerService = customerService;
            this._permissionService = permissionService;
            this._smsService = smsService;
            this._emailAccountService = emailAccountService;
            this._emailAccountSettings = emailAccountSettings;
            this._emailSender = emailSender;
        }

        #endregion

        #region Utilities

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GetTermsByYear(string year)
        {
            //this action method gets called via an ajax request
            if (String.IsNullOrEmpty(year))
                throw new ArgumentNullException("year");

            var bills = _billService.GetBills();

            var terms = (from b in bills
                         where b.Year.ToString() == year
                         orderby b.Term
                         select b.Term).Distinct().ToList();

            return Json(terms, JsonRequestBehavior.AllowGet);
        }

        #endregion
        public ActionResult Index()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMessages))
                return AccessDeniedView();

            var model = new MessageModel();

            var bills = _billService.GetBills();
            var years = (from y in bills
                         orderby y.Year
                         select y.Year).Distinct();

            model.Years.Add(new SelectListItem { Text = "انتخاب", Value = "0" });
            foreach (var year in years)
                model.Years.Add(new SelectListItem { Text = year.ToString(), Value = year.ToString() });

            return View(model);
        }

        public ActionResult sendTextBySMS(string year, string term, string selectedIds, string msgText, bool sendText, bool sendSMS)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMessages))
                return AccessDeniedView();

            var customers = new List<Customer>();
            if (selectedIds != null)
            {
                var ids = selectedIds
                    .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => Convert.ToInt32(x))
                    .ToArray();
                customers.AddRange(_customerService.GetCustomersByIds(ids));
            }

            if (sendText)
            {
                if (sendSMS)
                {
                    try
                    {
                        foreach (var customer in customers)
                        {
                            _smsService.SendSMS(customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone), msgText);
                        }
                        return Json("پیام مورد نظر با موفقیت ارسال شد", JsonRequestBehavior.AllowGet);
                    }
                    catch
                    {
                        return Json("ارسال پیام با خطا مواجه شد", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    foreach (var customer in customers)
                    {
                        var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
                        if (emailAccount == null)
                            return Json("No email account found with the specified id", JsonRequestBehavior.AllowGet);
                        try
                        {
                            string subject = "شرکت انرژی پاک";
                            string body = "<div style='direction: ltr;text-align: right;font-family: tahoma;white-space: pre-line'>" +
                                    msgText +
                                    "</div>";
                            _emailSender.SendEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.DisplayName, customer.Email, null);
                        }
                        catch (Exception exc)
                        {
                            return Json("ارسال پیام با خطا مواجه شد", JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json("پیام مورد نظر با موفقیت ارسال شد", JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                if (sendSMS)
                {
                    if (string.IsNullOrEmpty(year) || string.IsNullOrEmpty(term))
                        return Json("سال و دوره را انتخاب کنید", JsonRequestBehavior.AllowGet);

                    int yy = Convert.ToInt32(year);
                    int tt = Convert.ToInt32(term);

                    foreach (var customer in customers)
                    {
                        var splits = _splitService.GetAllSplits(customerId: customer.Id);

                        foreach (var split in splits)
                        {
                            try
                            {
                                if (split.Active == true)
                                {
                                    var bill = _billService.GetBillBysyt(split.Id, yy, tt);
                                    if (bill != null)
                                    {
                                        var strMessage = "مشترک  گرامی : " + customer.GetFullName() + "\r" +
                                             "انشعاب :" + split.Title + "\r" +
                                             "صورتحساب دوره :" + term + " / " + year + "\r" +
                                             "به مبلغ :" + bill.Payable + "\r" +
                                             "شناسه واریز :" + bill.PaymentID + "\r" +
                                             "صادر شده است" + "\r" +
                                             "مهلت پرداخت :" + bill.ExpDate.PersianDateN("/") + "\r" +
                                             "شرکت انرژی پاک";

                                        _smsService.SendSMS(customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone), strMessage);
                                    }
                                }
                            }
                            catch
                            {
                                return Json("ارسال پیام با خطا مواجه شد", JsonRequestBehavior.AllowGet);
                            }
                        }
                    }

                    return Json("پیام مورد نظر با موفقیت ارسال شد", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (string.IsNullOrEmpty(year) || string.IsNullOrEmpty(term))
                        return Json("سال و دوره را انتخاب کنید", JsonRequestBehavior.AllowGet);

                    int yy = Convert.ToInt32(year);
                    int tt = Convert.ToInt32(term);

                    var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
                    if (emailAccount == null)
                        return Json("No email account found with the specified id", JsonRequestBehavior.AllowGet);

                    foreach (var customer in customers)
                    {
                        var splits = _splitService.GetAllSplits(customerId: customer.Id);

                        foreach (var split in splits)
                        {
                            try
                            {
                                if (split.Active == true)
                                {
                                    var bill = _billService.GetBillBysyt(split.Id, yy, tt);
                                    if (bill != null)
                                    {
                                        string subject = "صورتحساب دوره :" + term + " / " + year;
                                        string body = "<div style='direction: ltr;text-align: right;font-family: tahoma;'>" +
                                            "مشترک  گرامی : " + customer.GetFullName() + "<br />" +
                                             "انشعاب :" + split.Title + "<br />" +
                                             "صورتحساب دوره :" + term + " / " + year + "<br />" +
                                             "به مبلغ :" + bill.Payable + "<br />" +
                                             "شناسه واریز :" + bill.PaymentID + "<br />" +
                                             "صادر شده است" + "<br />" +
                                             "مهلت پرداخت :" + bill.ExpDate.PersianDateN("/") + "<br />" +
                                             "شرکت انرژی پاک" +
                                             "</div>";

                                        _emailSender.SendEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.DisplayName, customer.Email, null);
                                    }
                                }
                            }
                            catch (Exception exc)
                            {
                                return Json("ارسال پیام با خطا مواجه شد", JsonRequestBehavior.AllowGet);
                            }

                        }
                    }
                    return Json("پیام مورد نظر با موفقیت ارسال شد", JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}