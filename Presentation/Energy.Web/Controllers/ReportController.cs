﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Energy.Web.Extensions;
using Energy.Web.Models.Bills;
using Energy.Core.Domain.Constant;
using Energy.Core.Domain.Bills;
using Energy.Core.Domain.Tariffs;
using Energy.Web.Models.Splits;
using Energy.Web.Models.Report;
using Energy.Services.Splits;
using Energy.Services.Bills;
using Energy.Services.Tariffs;
using Energy.Services.Directory;
using Energy.Web.Models.Reports;
using Energy.Web.Framework.Kendoui;
using System.IO;
using Energy.Services.ExportImport;

namespace Energy.Web.Controllers
{
    public class ReportController : BasePublicController
    {
        #region Fields

        private readonly IBillService _billService;
        private readonly ISplitService _splitService;
        private readonly IRegionService _regionService;
        private readonly IDistributionAreaService _distributionAreaService;
        private readonly IExportManager _exportManager;

        #endregion

        #region Ctor
        public ReportController(
            ISplitService splitService,
            IBillService billService,
            IRegionService regionService,
            IDistributionAreaService distributionAreaService,
            IExportManager exportManager)
        {
            this._splitService = splitService;
            this._billService = billService;
            this._regionService = regionService;
            this._distributionAreaService = distributionAreaService;
            this._exportManager = exportManager;
        }

        #endregion

        #region Utilities

        private string GetTariffName(int cTariff)
        {
            switch (cTariff)
            {
                case 1:
                    return "مصارف خانگی";
                case 2:
                    return "مصارف عمومی";
                case 3:
                    return "مصارف کشاورزی";
                case 4:
                    return "مصارف صنعتی";
                case 5:
                    return "سایر مصارف";
                default:
                    return "تعرفه ندارد";
            }
        }

        [NonAction]
        protected virtual void PrepareReportModel(ReportListModel model)
        {
            //years
            var bills = _billService.GetBills();
            var years = (from y in bills
                         orderby y.Year
                         select y.Year).Distinct();

            model.Years.Add(new SelectListItem { Text = "", Value = "" });
            foreach (var year in years)
                model.Years.Add(new SelectListItem { Text = year.ToString(), Value = year.ToString() });

            //regions
            model.Regions.Add(new SelectListItem { Text = "انتخاب", Value = "0" });

            foreach (var s in _regionService.GetAllRegions())
            {
                model.Regions.Add(new SelectListItem
                {
                    Text = s.Name,
                    Value = s.Id.ToString(),
                    Selected = s.Id == model.Region_Id
                });
            }

            //distributionAreas
            var distributionAreas = _distributionAreaService.GetDistributionAreasByRegionId(model.Region_Id).ToList();
            if (distributionAreas.Count > 0)
            {
                model.DistributionAreas.Add(new SelectListItem { Text = "انتخاب", Value = "0" });

                foreach (var da in distributionAreas)
                {
                    model.DistributionAreas.Add(new SelectListItem { Text = da.Name, Value = da.Id.ToString(), Selected = (da.Id == model.DistributionArea_Id) });
                }
            }
            else
            {
                bool anyRegionSelected = model.Regions.Any(x => x.Selected);

                model.DistributionAreas.Add(new SelectListItem
                {
                    Text = anyRegionSelected ? "سایر" : "انتخاب",
                    Value = "0"
                });
            }

            model.CTariffs.Add(new SelectListItem { Text = "تمام تعرفه ها", Value = "0" });

            int i = 1;
            foreach (Enum enumValue in (typeof(CTariff)).GetEnumValues())
            {
                var Name = GetTariffName(i);
                model.CTariffs.Add(new SelectListItem { Text = Name, Value = (i++).ToString() });
            }
        }

        #endregion

        #region Methods
        public ActionResult Index()
        {
            var model = new ReportListModel();
            PrepareReportModel(model);

            return View("ShowReport", model);
        }

        public ActionResult financeReport(int? year, int? term, bool domain, bool financeReport,
            int? cTariff = -1, int? toYear = 1394, int? toTerm = 4,
            int? region_Id = 0, int? distribution_Id = 0, bool includeAll = false)
        {
            IEnumerable<Bill> bills = null;

            bills = _billService.GetBillsByFilter1(year, term, domain, toYear, toTerm,
                region_Id, distribution_Id, cTariff, splitId: 0, includeAll: includeAll);

            int counter = 0;

            IList<ReportModel> model = new List<ReportModel>();

            if (bills != null)
            {
                foreach (var bill in bills)
                {
                    counter++;

                    var s = new ReportModel
                    {
                        rowNumber = counter,
                        splitId = bill.SplitID,
                        CustomerName = _splitService.GetCustomerName(bill.SplitID),
                        SplitName = _splitService.GetSplitById(bill.SplitID).Title,
                        Total = bill.Total,
                        Tax = bill.Tax,
                        Toll = bill.Toll,
                        Debit = bill.Debit,
                        DelayPenalty = bill.LatePenalty,
                        BillPayable = bill.Payable < 0 ? 0 : bill.Payable,
                        Year = bill.Year,
                        Term = bill.Term,
                        MidUsed = bill.MidUsed,
                        MaxUsed = bill.MaxUsed,
                        MinUsed = bill.MinUsed,
                        AllUsed = bill.AllUsed,
                        ReactUsed = bill.ReactUsed,
                        NetworkCost = bill.NetworkCost,
                        FreelanceCost = bill.FreelanceCost,
                    };
                    model.Add(s);
                }
            }

            var gridModel = new DataSourceResult
            {
                Data = model,
                Total = model.Count
            };

            return Json(gridModel);
        }

        #endregion

        #region Export

        public ActionResult ExportExcelAll(int? year, int? term, bool domain, bool financeReport,
            int? cTariff = -1, int? toYear = 1394, int? toTerm = 4,
            int? region_Id = 0, int? distribution_Id = 0)
        {
            try
            {
                IEnumerable<Bill> bills = null;

                bills = _billService.GetBillsByFilter1(year, term, domain, toYear, toTerm, region_Id, distribution_Id, cTariff);

                byte[] bytes;
                using (var stream = new MemoryStream())
                {
                    _exportManager.ExportReportToXlsx(stream, bills.ToList());
                    bytes = stream.ToArray();
                }
                return File(bytes, "text/xls", "report.xlsx");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Index");
            }
        }

        #endregion
    }
}