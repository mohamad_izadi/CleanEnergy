﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Energy.Core;
using Energy.Core.Domain.Customers;
using Energy.Services.Common;
using Energy.Services.Bills;
using Energy.Services.Security;
using Energy.Services.Splits;
using Energy.Services.Tariffs;
using Energy.Services.Media;
using Energy.Services.Logging;
using Energy.Web.Models.Splits;
using Energy.Web.Extensions;
using Energy.Web.Framework.Kendoui;
using Energy.Services.ImenSms;
using System.IO;

namespace Energy.Web.Controllers
{
    public class BillsCustomerController : BasePublicController
    {
        #region Fields

        private readonly ISplitService _splitService;
        private readonly IBillService _billService;
        private readonly IBillNormalService _billNormalService;
        private readonly ITariffService _tariffService;
        private readonly IPermissionService _permissionService;
        private readonly IWorkContext _workContext;
        private readonly IPictureService _pictureService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IPdfService _pdfService;

        #endregion

        #region Ctor
        public BillsCustomerController(
            ISplitService splitService,
            IBillService billService,
            ITariffService tariffService,
            IPictureService pictureService,
            IBillNormalService billNormalService,
            IPermissionService permissionService,
            IWorkContext workContext,
            ICustomerActivityService customerActivityService,
            IPdfService pdfService)
        {
            this._splitService = splitService;
            this._billService = billService;
            this._tariffService = tariffService;
            this._pictureService = pictureService;
            this._billNormalService = billNormalService;
            this._permissionService = permissionService;
            this._workContext = workContext;
            this._customerActivityService = customerActivityService;
            this._pdfService = pdfService;
        }

        #endregion

        #region Utilities
        private string GetTariffName(int cTariff)
        {
            switch (cTariff)
            {
                case 1:
                    return "مصارف خانگی";
                case 2:
                    return "مصارف عمومی";
                case 3:
                    return "مصارف کشاورزی";
                case 4:
                    return "مصارف صنعتی";
                case 5:
                    return "سایر مصارف";
                default:
                    return "تعرفه ندارد";
            }
        }

        #endregion

        #region Methods
        public ActionResult Index()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.viewBills))
                return AccessDeniedView();

            var customer = _workContext.CurrentCustomer;

            var model = new SplitListModel();
            model.customerName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName) + " " + customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName);
            model.email = customer.Email;
            model.phoneNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone);
            model.CompanyName = customer.GetAttribute<string>(SystemCustomerAttributeNames.Company);
            model.customerId = customer.Id;

            return View("BillsCustomer", model);
        }

        [HttpPost]
        public ActionResult SplitList(DataSourceRequest command, SplitListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.viewBills))
                return AccessDeniedView();

            var splits = _splitService.GetAllSplits(
                customerId: _workContext.CurrentCustomer.Id,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize);

            var gridModel = new DataSourceResult
            {
                Data = splits.Select(x =>
                {
                    var m = x.ToModel();
                    m.CustomerName = _splitService.GetCustomerName(m.Id);
                    m.Tariff = GetTariffName((int)x.CTariffId);
                    return m;
                }),
                Total = splits.TotalCount
            };

            return Json(gridModel);
        }

        [HttpPost]
        public ActionResult BillList(DataSourceRequest command, int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.viewBills))
                return AccessDeniedView();

            var bills = _billService.GetAllBills(
                splitId: id,
                year: 0,
                month: 0,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize);

            foreach (var bill in bills)
            {
                switch (bill.Type)
                {
                    case "main":
                        bill.Type = "اصلی";
                        break;
                    case "archive":
                        bill.Type = "بایگانی";
                        break;
                    case "corrective":
                        bill.Type = "اصلاحی";
                        break;
                    case "midterm":
                        bill.Type = "میان دوره";
                        break;
                    case "zero":
                        bill.Type = "کنتور صفر";
                        break;
                }
            }

            var gridModel = new DataSourceResult
            {
                Data = bills,
                Total = bills.TotalCount,
            };

            //activity log
            //_customerActivityService.InsertActivity("Split.Bills.View", "لیست صورتحساب ها توسط مشترک دیده شد");

            return Json(gridModel);
        }

        public ActionResult DownloadBillPdf(string id, string withDebit)
        {
            //load bills
            var bill = _billService.getById(Convert.ToInt32(id));

            var includeDebit = Convert.ToBoolean(withDebit);
            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _pdfService.PrintBillToPdf(stream, bill, includeDebit);
                bytes = stream.ToArray();
            }
            _customerActivityService.InsertActivity("Split.Bills.View", string.Format("صورتحساب {0} توسط مشترک دیده شد", id));
            return File(bytes, "application/pdf", string.Format("bill_{0}{1}_{2}.pdf", bill.Year, bill.Term, bill.Id));
        }

        #endregion

    }
}