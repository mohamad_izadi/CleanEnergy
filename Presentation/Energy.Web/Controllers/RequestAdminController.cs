﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Energy.Core;
using Energy.Core.Domain.Requests;
using Energy.Services.Security;
using Energy.Services.Splits;
using Energy.Services.Tariffs;
using Energy.Services.Requests;
using Energy.Services.Directory;
using Energy.Web.Models.Requests;
using Energy.Web.Extensions;
using Energy.Core.Infrastructure;
using Energy.Web.Framework.Kendoui;
using Energy.Services.Files;
using Energy.Services.Helpers;
using Energy.Services.Customers;
using Energy.Services.Common;
using Energy.Core.Domain.Customers;
using Energy.Core.Domain.Constant;

namespace Energy.Web.Controllers
{
    public class RequestAdminController : BaseAdminController
    {
        #region Fields

        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IPermissionService _permissionService;
        private readonly ISplitService _splitService;
        private readonly ITariffService _tariffService;
        private readonly IRequestService _requestService;
        private readonly IRegionService _regionService;
        private readonly IDistributionAreaService _distributionAreaService;
        private readonly IFileService _fileService;

        #endregion

        #region Constructors

        public RequestAdminController(IDateTimeHelper dateTimeHelper,
            IPermissionService permissionService,
            ISplitService splitService,
            ITariffService tariffService,
            IRequestService requestService,
            IRegionService regionService,
            IDistributionAreaService distributionAreaService,
            IFileService fileService)
        {
            this._dateTimeHelper = dateTimeHelper;
            this._permissionService = permissionService;
            this._splitService = splitService;
            this._tariffService = tariffService;
            this._requestService = requestService;
            this._regionService = regionService;
            this._distributionAreaService = distributionAreaService;
            this._fileService = fileService;
        }

        #endregion

        #region Utilities

        private void PrepareRequestInfoModel(RequestDetailModel model, Request request)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            if (request == null)
                throw new ArgumentNullException("request");

            model.Address = request.Address;
            model.AddressContact = request.AddressContact;
            model.Amp = request.Amp.ToString();
            model.Volt = Enum.Parse(typeof(CVolt), request.Volt.ToString(), true).ToString();
            model.Phase = Enum.Parse(typeof(CPhase), request.Phase.ToString(), true).ToString(); ;
            model.Power = request.Power.ToString();
            model.CreatedOn = request.CreateDate;
            model.SplitPostalCode = request.SplitPostalCode;

            var customer = EngineContext.Current.Resolve<ICustomerService>().GetCustomerById(request.Customer_Id);
            model.CustomerName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName) + " " +
                customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName);

            var distributionArea = _distributionAreaService.GetDistributionAreaById(request.DistributionArea_Id);
            if (distributionArea != null)
                model.DistributionArea = distributionArea.Name + " - " + distributionArea.Region.Name;

            if (request.RequestType != null)
                model.RequestTypeName = "درخواست انشعاب جدید";

            model.tariff = Enum.Parse(typeof(CTariff), request.tariff_Id.ToString(), true).ToString();

            //files
            var files = _fileService.GetFilesByRequestId(request.Id);
            var fileModels = new List<RequestDetailModel.RequestFileModel>();

            foreach (var file in files)
            {
                string filePath = _fileService.GetFilePath(file.Id);
                var f = _fileService.GetFileById(file.Id);
                fileModels.Add(new RequestDetailModel.RequestFileModel
                {
                    File_Id = file.Id,
                    FileUrl = filePath,
                    FileName = f.FileName,
                    Description = _fileService.GetFileDescription(f.Id),
                });
            }
            model.RequestFileModels = fileModels;
        }

        #endregion

        #region requests
        public ActionResult ListRequests()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageRequests))
                return AccessDeniedView();

            var requestSearchModel = new RequestSearchModel();
            requestSearchModel.RequestType.Add(new SelectListItem
            {
                Value = "0",
                Text = "All"
            });

            //foreach (var at in _requestService.geta)
            //{
            //    activityLogSearchModel.ActivityLogType.Add(new SelectListItem
            //    {
            //        Value = at.Id.ToString(),
            //        Text = at.Name
            //    });
            //}
            return View(requestSearchModel);
        }

        [HttpPost]
        public ActionResult ListRequests(DataSourceRequest command, RequestSearchModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageRequests))
                return AccessDeniedView();

            DateTime? startDateValue = (model.CreatedOnFrom == null) ? null
                : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.CreatedOnFrom.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.CreatedOnTo == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.CreatedOnTo.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            var requests = _requestService.GetAllRequests(startDateValue, endDateValue, null, model.RequestTypeId, command.Page - 1, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = requests.Select(x =>
                {
                    var m = x.ToModel();
                    m.CreatedOn = _dateTimeHelper.ConvertToUserTime(x.CreateDate, DateTimeKind.Utc);
                    return m;
                }),
                Total = requests.TotalCount
            };
            return Json(gridModel);
        }

        public ActionResult Detail(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageRequests))
                return AccessDeniedView();

            var request = _requestService.GetRequestById(id);
            if (request == null)
                //No splits item found with the specified id
                return RedirectToAction("List");

            var model = new RequestDetailModel();
            PrepareRequestInfoModel(model, request);

            return View(model);
        }

        public ActionResult Download(int Id)
        {
            //دريافت مسير دانلود فايل
            var fileName = _fileService.GetFilePath(Id);
            if (fileName == null)
                return RedirectToAction("List");

            var file = _fileService.GetFileById(Id);
            var contentType = file.FileType;

            byte[] binary = System.IO.File.ReadAllBytes(fileName);

            return new FileContentResult(binary, contentType) { FileDownloadName = file.FileName + "." + contentType };
        }

        #endregion
    }
}