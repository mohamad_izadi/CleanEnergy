﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Energy.Web.Models.Tariffs;
using Energy.Web.Extensions;
using Energy.Services.Security;
using Energy.Services.Tariffs;
using Energy.Web.Framework.Kendoui;
using Energy.Core.Domain.Constant;
using Energy.Web.Framework.Mvc;
using Energy.Core.Domain.Tariffs;

namespace Energy.Web.Controllers
{
    public class TariffController : BaseAdminController
    {
        #region Fields

        private readonly IPermissionService _permissionService;
        private readonly ITariffService _tariffService;

        #endregion

        #region Constructors

        public TariffController(IPermissionService permissionService,
            ITariffService tariffService)
        {
            this._permissionService = permissionService;
            this._tariffService = tariffService;
        }

        #endregion

        #region Utilities
        private string GetTariffName(int cTariff)
        {
            switch (cTariff)
            {
                case 1:
                    return "مصارف خانگی";
                case 2:
                    return "مصارف عمومی";
                case 3:
                    return "مصارف کشاورزی";
                case 4:
                    return "مصارف صنعتی";
                case 5:
                    return "سایر مصارف";
                default:
                    return "تعرفه ندارد";
            }
        }

        #endregion

        #region Methods

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult TariffAreaList()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageTariffs))
                return AccessDeniedView();

            var model = new TariffListModel();
            model.CTariffs.Add(new SelectListItem { Text = "تمام تعرفه ها", Value = "0" });

            int i = 1;
            foreach (Enum enumValue in (typeof(CTariff)).GetEnumValues())
            {
                model.CTariffs.Add(new SelectListItem { Text = GetTariffName(i), Value = (i++).ToString() });
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult TariffAreaList(DataSourceRequest command, TariffListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageTariffs))
                return AccessDeniedView();

            var tariffs = _tariffService.getAllTarrifArea(
                CTariff: model.CTariffId,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize);

            var gridModel = new DataSourceResult
            {
                Data = tariffs.Select(x =>
                {
                    var m = x.ToModel();
                    return m;
                }),
                Total = tariffs.TotalCount
            };

            return Json(gridModel);
        }

        [HttpPost]
        public ActionResult TariffAreaUpdate(TariffAreaModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageTariffs))
                return AccessDeniedView();

            var tariffArea = _tariffService.getTariffAreaById(model.Id);
            tariffArea = model.ToEntity(tariffArea);

            if (ModelState.IsValid)
            {
                _tariffService.UpdateTariffArea(tariffArea);
            }

            return new NullJsonResult();
        }

        ////////

        public ActionResult TariffList()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageTariffs))
                return AccessDeniedView();

            var model = new TariffListModel();
            model.CTariffs.Add(new SelectListItem { Text = "تمام تعرفه ها", Value = "0" });

            int i = 1;
            foreach (Enum enumValue in (typeof(CTariff)).GetEnumValues())
            {
                model.CTariffs.Add(new SelectListItem { Text = GetTariffName(i), Value = (i++).ToString() });
            }
            return View("TariffList", model);
        }
        [HttpPost]
        public ActionResult TariffList(DataSourceRequest command, TariffListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageTariffs))
                return AccessDeniedView();

            var tariffs = _tariffService.getAllTarrif(
                CTariff: model.CTariffId,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize);

            var gridModel = new DataSourceResult
            {
                Data = tariffs,
                Total = tariffs.TotalCount
            };

            return Json(gridModel);
        }

        [HttpPost]
        public ActionResult TariffUpdate(Tariff model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageTariffs))
                return AccessDeniedView();

            var tariff = _tariffService.GetById(model.Id);

            tariff.LimitCostKwh = model.LimitCostKwh;
            tariff.MaxFine = model.MaxFine;
            tariff.MinDiscount = model.MinDiscount;
            tariff.NoMaxDiscount = model.NoMaxDiscount;
            tariff.Factor = model.Factor;
            tariff.PowerCost = model.PowerCost;
            tariff.MidCost = model.MidCost;
            tariff.MaxCost = model.MaxCost;
            tariff.MinCost = model.MinCost;

            _tariffService.UpdateTariff(tariff);


            return new NullJsonResult();
        }

        ///

        public ActionResult TariffNormalList()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageTariffs))
                return AccessDeniedView();

            var model = new TariffListModel();
            model.CTariffs.Add(new SelectListItem { Text = "تمام تعرفه ها", Value = "0" });

            int i = 1;
            foreach (Enum enumValue in (typeof(CTariff)).GetEnumValues())
            {
                if(i == 1 || i == 5)
                    model.CTariffs.Add(new SelectListItem { Text = GetTariffName(i), Value = (i).ToString() });
                i++;
            }
            return View("TariffNormalList", model);
        }
        [HttpPost]
        public ActionResult TariffNormalList(DataSourceRequest command, TariffListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageTariffs))
                return AccessDeniedView();

            var tariffNormals = _tariffService.getAllTarrifNormal(
                CTariff: model.CTariffId,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize);

            var gridModel = new DataSourceResult
            {
                Data = tariffNormals,
                Total = tariffNormals.TotalCount
            };

            return Json(gridModel);
        }

        #endregion
    }
}