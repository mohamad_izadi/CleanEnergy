﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Energy.Core.Domain.Customers;
using Energy.Services.Helpers;
using Energy.Services.Security;
using Energy.Services.Seo;
using Energy.Services.Splits;
using Energy.Services.Tariffs;
using Energy.Web.Models.Splits;
using Energy.Web.Extensions;
using Energy.Web.Framework;
using Energy.Web.Framework.Controllers;
using Energy.Web.Framework.Kendoui;
using Energy.Web.Framework.Mvc;
using Energy.Core.Domain.Constant;
using Energy.Core.Domain.Splits;
using Energy.Services.Directory;
using Energy.Services.Customers;
using Energy.Web.Models.CustomersAdmin;
using Energy.Core.Domain.Tariffs;
using Energy.Services.Logging;
using Energy.Core;
using Energy.Core.Domain.Bills;
using System.Converter;
using Energy.Services.Bills;

namespace Energy.Web.Controllers
{
    public class SplitController : BaseAdminController
    {
        #region Fields

        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IPermissionService _permissionService;
        private readonly ISplitService _splitService;
        private readonly IBillService _billService;
        private readonly ITariffService _tariffService;
        private readonly IRegionService _regionService;
        private readonly IDistributionAreaService _distributionAreaService;
        private readonly ICustomerService _customerService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IWorkContext _workContext;

        #endregion

        #region Constructors

        public SplitController(IDateTimeHelper dateTimeHelper,
            IPermissionService permissionService,
            ISplitService splitService,
            IBillService billService,
            ITariffService tariffService,
            IRegionService regionService,
            IDistributionAreaService distributionAreaService,
            ICustomerService customerService,
            ICustomerActivityService customerActivityServic,
            IWorkContext workContext)
        {
            this._dateTimeHelper = dateTimeHelper;
            this._permissionService = permissionService;
            this._splitService = splitService;
            this._billService = billService;
            this._tariffService = tariffService;
            this._regionService = regionService;
            this._distributionAreaService = distributionAreaService;
            this._customerService = customerService;
            this._customerActivityService = customerActivityServic;
            this._workContext = workContext;

        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual void PrepareSplitModel(SplitModel model)
        {
            model.Regions.Add(new SelectListItem { Text = "انتخاب", Value = "0" });

            foreach (var s in _regionService.GetAllRegions())
            {
                model.Regions.Add(new SelectListItem
                {
                    Text = s.Name,
                    Value = s.Id.ToString(),
                    Selected = s.Id == model.Region_Id
                });
            }

            //cities
            var distributionAreas = _distributionAreaService.GetDistributionAreasByRegionId(model.Region_Id).ToList();
            if (distributionAreas.Count > 0)
            {
                model.DistributionAreas.Add(new SelectListItem { Text = "انتخاب", Value = "0" });

                foreach (var da in distributionAreas)
                {
                    model.DistributionAreas.Add(new SelectListItem { Text = da.Name, Value = da.Id.ToString(), Selected = (da.Id == model.DistributionArea_Id) });
                }
            }
            else
            {
                bool anyRegionSelected = model.Regions.Any(x => x.Selected);

                model.DistributionAreas.Add(new SelectListItem
                {
                    Text = anyRegionSelected ? "سایر" : "انتخاب",
                    Value = "0"
                });
            }

            //tariffs
            model.CTariffs.Add(new SelectListItem { Text = "انتخاب", Value = "0" });

            int i = 1;
            foreach (Enum enumValue in (typeof(CTariff)).GetEnumValues())
            {
                model.CTariffs.Add(new SelectListItem { Text = GetTariffName(i), Value = (i++).ToString() });
            }
        }

        private string GetTariffName(int cTariff)
        {
            switch (cTariff)
            {
                case 1:
                    return "مصارف خانگی";
                case 2:
                    return "مصارف عمومی";
                case 3:
                    return "مصارف کشاورزی";
                case 4:
                    return "مصارف صنعتی";
                case 5:
                    return "سایر مصارف";
                default:
                    return "تعرفه ندارد";
            }
        }

        #endregion

        #region Methods

        #region Splits

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSplits))
                return AccessDeniedView();

            var model = new SplitListModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult SplitList(DataSourceRequest command, SplitListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSplits))
                return AccessDeniedView();

            var splits = _splitService.GetAllSplits(
                splitId: model.splitId,
                customerId: model.customerId,
                customerName: model.customerName,
                splitName: model.splitName,
                fileNo: model.fileNo,
                serialNo: model.serialNo,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize);

            var gridModel = new DataSourceResult
            {
                Data = splits.Select(x =>
                {
                    var m = x.ToModel();
                    m.CustomerName = _splitService.GetCustomerName(m.Id);
                    m.Tariff = GetTariffName((int)x.CTariffId);
                    return m;
                }),
                Total = splits.TotalCount
            };

            return Json(gridModel);
        }

        public ActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSplits))
                return AccessDeniedView();

            var model = new SplitModel();
            PrepareSplitModel(model);
            model.Active = true;
            model.IncludeLateFees = true;
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(SplitModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSplits))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var split = model.ToEntity();
                split.CreateDate = DateTime.UtcNow;
                split.Active = true;
                _splitService.InsertSplit(split);

                var firstBill = new Bill
                {
                    SplitID = split.Id,
                    DateRead = split.InstallDate.Value,
                    MidRead = 0,
                    MaxRead = 0,
                    MinRead = 0,
                    ReactRead = 0,
                    PowerRead = 0,
                    Year = _billService.getLastBill().Year,
                    Term = _billService.getLastBill().Term,
                    DateReadPre = split.InstallDate.Value,
                    Type = "main",
                    LastPayment1 = 0,
                    LastPayment2 = 0,
                    Debit = 0,
                    issueDate = DateTime.Now,
                };

                _billService.InsertBill(firstBill);

                //activity log
                _customerActivityService.InsertActivity("Split.Create", " انشعاب جدید توسط کاربر تعریف شد", _workContext.CurrentCustomer);

                SuccessNotification("انشعاب مورد نظر به لیست انشعاب ها افزوده شد");
                return continueEditing ? RedirectToAction("Edit", new { id = split.Id }) : RedirectToAction("List");
            }

            PrepareSplitModel(model);
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSplits))
                return AccessDeniedView();

            var split = _splitService.GetSplitById(id);
            if (split == null)
                //No splits item found with the specified id
                return RedirectToAction("List");

            var model = split.ToModel();
            PrepareSplitModel(model);
            model.CustomerName = _splitService.GetCustomerName(id);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit(SplitModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSplits))
                return AccessDeniedView();

            var split = _splitService.GetSplitById(model.Id);
            if (split == null)
                //No news item found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                split = model.ToEntity(split);
                _splitService.UpdateSplit(split);

                //activity log
                _customerActivityService.InsertActivity("Split.Edit", " انشعاب " + split.Id + " توسط کاربر ویرایش شد", _workContext.CurrentCustomer);

                SuccessNotification("انشعاب مورد نظر با موفقیت ویرایش شد");

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabIndex();

                    return RedirectToAction("Edit", new { id = split.Id });
                }
                return RedirectToAction("List");
            }

            PrepareSplitModel(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSplits))
                return AccessDeniedView();

            var split = _splitService.GetSplitById(id);
            if (split == null)
                //No news item found with the specified id
                return RedirectToAction("List");

            _splitService.DeleteSplit(split);

            SuccessNotification("انشعاب مورد نظر حذف شد");
            return RedirectToAction("List");
        }

        public ActionResult AttachCustomer(string customerId, string splitId)
        {
            var split = _splitService.GetSplitById(Convert.ToInt32(splitId));

            if (split == null)
                throw new ArgumentException("No split found with the specified id");

            split.Customer_Id = Convert.ToInt32(customerId);
            _splitService.UpdateSplit(split);

            return Json(new { msg = string.Format("انشعاب شماره {0} به مشترک مورد نظر متصل شد", splitId) },
                JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Tariff

        [HttpPost]
        public JsonResult CurrentTariffList(int splitId)
        {
            var sAreas = _tariffService.GetSplitAreasBySplit(splitId);

            IList<TariffArea> tariffs = new List<TariffArea>();
            foreach (var sa in sAreas)
            {
                var ta = _tariffService.getTariffAreaById(sa.TariffAreaID);
                tariffs.Add(ta);
            }

            var gridModel = new DataSourceResult
            {
                Data = tariffs.Select(x =>
                {
                    var m = x.ToModel();
                    return m;
                }),
                Total = tariffs.Count
            };

            return Json(gridModel);
        }

        [HttpPost]
        public JsonResult AttachTariff(int splitId, int tariffAreaId)
        {
            var split = _splitService.GetSplitById(splitId);
            split.CTariffId = _tariffService.getTariffAreaById(tariffAreaId).Tariff.CTariff;
            _splitService.UpdateSplit(split);

            var splitArea = new SplitArea();
            splitArea.SplitID = splitId;
            splitArea.TariffAreaID = tariffAreaId;
            _tariffService.InsertSplitArea(splitArea);

            return Json(new { msg = "تعرفه جدید با موفقیت ذخیره شد" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RemoveTariff(int splitId)
        {
            var sAreas = _tariffService.GetSplitAreasBySplit(splitId);

            foreach (var sa in sAreas)
            {
                _tariffService.DeleteSplitArea(sa);
            }

            return Json(new { msg = "تعرفه های مورد نظر حذف شدند" }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion
    }
}