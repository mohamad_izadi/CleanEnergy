﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Energy.Services.Bills;
using Energy.Services.Splits;
using Energy.Services.Tariffs;
using Energy.Web.Models.Accounting;
using System.Text;
using System.IO;
using Energy.Web.Models.Debit;
using Kendo.Mvc.UI;
using System.Converter;
using Energy.Web.Models.Splits;
using Energy.Core.Domain.Bills;
using Energy.Web.Tools;

namespace Energy.Web.Controllers
{
    public class AccountingController : BaseAdminController
    {
        #region Fields

        private readonly ISplitService _splitService;
        private readonly IBillService _billService;
        private readonly IBillNormalService _billNormalService;
        private readonly ITariffService _tariffService;

        #endregion

        #region Ctor
        public AccountingController(
            ISplitService splitService,
            IBillService billService,
            ITariffService tariffService,
            IBillNormalService billNormalService)
        {
            this._splitService = splitService;
            this._billService = billService;
            this._tariffService = tariffService;
            this._billNormalService = billNormalService;
        }

        #endregion

        #region Methods

        #region Input Files
        public ActionResult Index()
        {
            var model = new AccountingModel();

            var bills = _billService.GetBills();
            var years = (from y in bills
                         orderby y.Year
                         select y.Year).Distinct();

            model.Years.Add(new SelectListItem { Text = "انتخاب", Value = "0" });
            foreach (var year in years)
                model.Years.Add(new SelectListItem { Text = year.ToString(), Value = year.ToString() });

            return View("Accounting", model);
        }

        public ActionResult Show_DebitFile(string fileName)
        {
            StringBuilder content = new StringBuilder();

            IList<DebitModel> model = new List<DebitModel>();

            string year = fileName.Substring(5, 4);
            string term = fileName.Substring(9, 2);

            try
            {
                var path = Path.Combine(Server.MapPath("~/Content/Accounting/Upload/"), fileName);

                var fileText = new StreamReader(path, System.Text.Encoding.UTF8);
                string line;
                int counter = -1;

                while ((line = fileText.ReadLine()) != null)
                {
                    counter++;
                    if (counter == 0)
                        continue;
                    var values = line.Split(',');

                    if (values[5].Length == 1 || Convert.ToInt32(values[5].Substring(values[5].Length - 3, 1)) != 2)
                        continue;

                    long lastPayable = 0;
                    var bill = _billService.GetBillBysyt(Convert.ToInt32(values[5].Substring(0, values[5].Length - 3)), Convert.ToInt32(year), Convert.ToInt32(term));
                    if (bill != null)
                    {
                        var lastbill = _billService.findPreviousBill(bill.Id, bill.SplitID);
                        if (lastbill != null)
                        {
                            lastPayable = lastbill.Payable;
                        }
                    }


                    string pDate = values[11].Substring(0, 4) + "/" + values[11].Substring(4, 2) + "/" + values[11].Substring(6, 2);

                    var debit = new DebitModel
                    {
                        selected = true,
                        rowNumber = counter,
                        splitId = values[5].Substring(0, values[5].Length - 3),
                        Payment = Convert.ToInt64(values[3]),
                        PaymentDate = pDate,
                        lastBillPayable = lastPayable,
                        documentNum = values[8],
                        customerName = values[6],
                        description = values[4],
                        Year = year,
                        Term = term
                    };

                    model.Add(debit);
                }
            }
            catch
            {
                content.AppendLine("خطا در خواندن فایل - لطفا دوره صحیح را انتخاب کنید");
                return Content(content.ToString());
            }

            var gridModel = new Energy.Web.Framework.Kendoui.DataSourceResult
            {
                Data = model,
                Total = model.Count
            };

            return Json(gridModel);
        }

        public ActionResult Update_Debits([DataSourceRequest] DataSourceRequest request, DebitModel[] griditems)
        {
            StringBuilder content = new StringBuilder();

            if (griditems == null)
            {
                content.AppendLine("");
                return Content(content.ToString());
            }

            int year = Convert.ToInt32(griditems[0].Year.ToString());
            int term = Convert.ToInt32(griditems[0].Term.ToString());

            var bills = _billService.GetBillsByDate(year, term);


            try
            {
                foreach (var item in griditems)
                {
                    if (item.selected)
                    {
                        var bill = _billService.GetBillBysyt(Convert.ToInt32(item.splitId), year, term);

                        if (bill != null)
                        {
                            if (bill.LastPayment1 == 0)
                            {
                                bill.LastPaymentDate1 = DateHelper.PersianToDateTime(item.PaymentDate);
                                bill.LastPayment1 = item.Payment;
                                var lastbill = _billService.findPreviousBill(bill.Id, bill.SplitID);

                                if (lastbill != null)
                                {
                                    bill.Debit = lastbill.Round + lastbill.Payable - bill.LastPayment1;
                                }
                                _billService.UpdateBill(bill);
                            }
                            else
                            {
                                bill.LastPaymentDate2 = DateHelper.PersianToDateTime(item.PaymentDate);
                                bill.LastPayment2 = item.Payment;
                                bill.Debit -= item.Payment;
                                _billService.UpdateBill(bill);
                            }
                        }
                    }
                }

            }
            catch (Exception)
            {
                content.AppendLine("خطا در وارد کردن اطلاعات ورودی");
                return Content(content.ToString());
            }

            content.AppendLine("محاسبات بدهی برای دوره جاری انجام شد");
            return Json(new { msg = content.ToString() }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Output Files

        public ActionResult ShowSelectingView(int year, int term)
        {
            var splits = _splitService.GetSplitsByBillDate(year, term);
            int counter = 0;

            IList<SplitModel> model = new List<SplitModel>();

            foreach (var split in splits)
            {
                counter++;
                var s = new SplitModel
                {
                    Id = split.Id,
                    Title = split.Title,
                    CustomerName = _splitService.GetCustomerName(split.Id),
                };

                s.rowNumber = counter;
                s.selected = true;

                model.Add(s);
            }

            var gridModel = new Energy.Web.Framework.Kendoui.DataSourceResult
            {
                Data = model,
                Total = model.Count
            };

            return Json(gridModel);
        }

        public ActionResult CreateOutputFile([DataSourceRequest] DataSourceRequest request, SplitModel[] griditems, string year, string term)
        {
            StringBuilder content = new StringBuilder();

            if (griditems == null)
            {
                content.AppendLine("انشعاب های مورد نظر را انتخاب کنید");
                return Content(content.ToString());
            }

            //try
            //{
            string fl = "bills" + year + term.Trim().PadLeft(2, '0') + ".csv";

            var path = Path.Combine(Server.MapPath("~/Content/Accounting/Download/"), fl);

            IList<int> splitIds = new List<int>();

            foreach (var split in griditems)
            {
                if (split.selected)
                    splitIds.Add(split.Id);
            }

            IList<Bill> bills = _billService.GetBillsByDate(Convert.ToInt32(year), Convert.ToInt32(term));

            //code 302
            long sumTax = 0;
            //code 701
            long sumToll = 0;

            long sumLatePenalty = 0;
            string s;
            var yt = " دوره ،" + year.ToString() + "/" + term.ToString() + "،";
            var sumTariff = new long[29];  // number of Tariff = constant //
            long bed = 0;
            long bes = 0;
            var sw = new StreamWriter(path, false, Encoding.GetEncoding(1256));

            foreach (int splitId in splitIds)
            {
                foreach (var b in bills)
                {
                    if (b.SplitID == splitId)
                    {

                        var article = (b.Split.Title + yt);
                        s = "103,001" + "," + b.Split.AccountNo + "," + b.SplitID.ToString() + "," + article +
                            "," + b.Total.ToString().Trim() + "," + "0";
                        sw.WriteLine(s);
                        bed = bed + b.Total;

                        s = "103,002" + "," + b.Split.AccountNo + "," + b.SplitID.ToString() + "," + article +
                            "," + b.Tax.ToString().Trim() + "," + "0";
                        sw.WriteLine(s);
                        bed = bed + b.Tax;

                        s = "103,003" + "," + b.Split.AccountNo + "," + b.SplitID.ToString() + "," + article +
                            "," + b.Toll.ToString().Trim() + "," + "0";
                        sw.WriteLine(s);
                        bed = bed + b.Toll;

                        s = "103,004" + "," + b.Split.AccountNo + "," + b.SplitID.ToString() + "," + article +
                            "," + Convert.ToInt32(b.LatePenalty).ToString().Trim() + "," + "0";

                        sw.WriteLine(s);
                        bed = bed + b.Toll;

                        if (b.Discount > 0)
                        {
                            s = "603,011" + "," + b.Split.AccountNo + "," + article + "," + b.Discount.ToString().Trim() + ",0";
                            sw.WriteLine(s);
                            bed = bed + b.Discount;
                        }
                        sumTax = sumTax + b.Tax;
                        sumToll = sumToll + b.Toll;
                        sumLatePenalty = sumLatePenalty + (long)b.LatePenalty;

                        if (b.Split.Description.ToInt() >= 601 && b.Split.Description.ToInt() <= 629)
                        {
                            sumTariff[b.Split.Description.ToInt() - 601] = sumTariff[b.Split.Description.ToInt() - 601] + b.Total;   // Code3 = TariffID + 600
                        }
                    }
                }
            }

            s = "302,001,3006" + ",," + "مالیات بر ارزش افزوده " + yt + ",0," + sumTax.ToString();
            sw.WriteLine(s);
            bes = bes + sumTax;

            s = "701,007,5009" + ",," + " عوارض برق " + yt + ",0," + sumToll.ToString();
            sw.WriteLine(s);
            bes = bes + sumToll;

            for (int i = 0; i < 29; i++)
            {
                if (sumTariff[i] > 0)
                {
                    s = "603," + GetTariffCode(i + 1) + "," + (601 + i).ToString() + ",," + " جمع در تعرفه " + yt + ",0," +
                         sumTariff[i].ToString();
                    sw.WriteLine(s);
                    bes = bes + sumTariff[i];
                }
            }

            s = "603,012," + ",," + " جریمه دیرکرد " + yt + ",0," + sumLatePenalty.ToString();
            sw.WriteLine(s);

            foreach (int splitId in splitIds)
            {
                foreach (var b in bills)
                {
                    if (b.SplitID == splitId)
                    {
                        if (b.Discount > 0)
                        {
                            s = "103,001," + b.Split.AccountNo + "," + b.SplitID.ToString() + "," + (b.Split.Title + yt) + "," +
                                   b.Discount.ToString().Trim();
                            sw.WriteLine(s);
                            bes = bes + b.Discount;
                        }
                    }
                }
            }

            //sw.WriteLine(bes.ToString() + "   " + bed.ToString());

            sw.Close();
            //}
            //catch (Exception)
            //{
            //    content.AppendLine("خطا در وارد کردن اطلاعات ورودی");
            //    return Json(new { msg = content.ToString() }, JsonRequestBehavior.AllowGet);
            //}

            content.AppendLine("فایل حسابداری برای دوره " + term + " سال " + year + "ایجاد شد");
            return Json(new { msg = content.ToString() }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region File
        public ActionResult AC_UploadFile(IEnumerable<HttpPostedFileBase> files)
        {
            StringBuilder content = new StringBuilder();

            HttpPostedFileBase file = null;
            foreach (var f in files)
                file = f;
            try
            {
                if (file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/Accounting/Upload/"), fileName);
                    file.SaveAs(path);
                    content.AppendLine("فایل با موفقیت اپلود شد");
                }
            }
            catch (Exception er)
            {
                content.AppendLine("فایل قابل ارسال نیست");
                return Content(content.ToString());
            }
            return Content(content.ToString());
        }

        public ActionResult AC_DownloadFile(int year, int term)
        {
            StringBuilder content = new StringBuilder();

            string tt = term.ToString();
            string yy = year.ToString();

            try
            {
                string fl = "bills" + year.ToString() + term.ToString().Trim().PadLeft(2, '0') + ".csv";
                var path = Path.Combine(Server.MapPath("~/Content/Accounting/Download/"), fl);

                Response.Clear();
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + fl);
                Response.TransmitFile(@path);
                Response.End();
            }
            catch (Exception er)
            {
                content.AppendLine("فایلی جهت ذخیره کردن وجود ندارد");
            }

            content.AppendLine("");
            return Content(content.ToString());
        }

        #endregion

        #endregion

        #region Utility

        protected string GetTariffCode(int ct)
        {
            if (ct >= 1 && ct <= 5) return "001";
            if (ct >= 6 && ct <= 8) return "002";
            if (ct >= 9 && ct <= 14) return "005";
            if (ct >= 15 && ct <= 21) return "004";
            if (ct >= 22 && ct <= 29) return "003";
            return "000";
        }

        #endregion
    }
}