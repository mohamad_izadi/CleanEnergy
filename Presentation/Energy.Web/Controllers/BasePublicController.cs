﻿using System.Web.Mvc;
using System.Web.Routing;
using Energy.Core.Infrastructure;
using Energy.Web.Framework;
using Energy.Web.Framework.Controllers;
using Energy.Web.Framework.Security;
using Energy.Web.Framework.Seo;

namespace Energy.Web.Controllers
{
    [StoreClosed]
    [PublicStoreAllowNavigation]
    [LanguageSeoCode]
    [ProHttpsRequirement(SslRequirement.NoMatter)]
    [WwwRequirement]
    public abstract partial class BasePublicController : BaseController
    {
        protected virtual ActionResult InvokeHttp404()
        {
            // Call target Controller and pass the routeData.
            IController errorController = EngineContext.Current.Resolve<CommonController>();

            var routeData = new RouteData();
            routeData.Values.Add("controller", "Common");
            routeData.Values.Add("action", "PageNotFound");

            errorController.Execute(new RequestContext(this.HttpContext, routeData));

            return new EmptyResult();
        }

        /// <summary>
        /// Access denied view
        /// </summary>
        /// <returns>Access denied view</returns>
        protected ActionResult AccessDeniedView()
        {
            //return new HttpUnauthorizedResult();
            return RedirectToAction("AccessDenied", "Security", new { pageUrl = this.Request.RawUrl });
        }
    }
}
