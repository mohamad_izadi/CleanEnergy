﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Converter;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using Energy.Core.Domain.Bills;
using Energy.Web.Framework.Kendoui;
using Energy.Web.Models.Handheld;
using Energy.Web.Models.Bills;
using Energy.Services.Splits;
using Energy.Services.Bills;
using Energy.Services.Tariffs;
using System.Globalization;
using Energy.Web.Tools;
using Energy.Core.Domain.Constant;
using Energy.Web.Models.HighLow;
using Energy.Web.Models.Splits;
using Web.Tools;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace Energy.Web.Controllers
{
    public class HandheldController : BaseAdminController
    {
        #region Fields

        private readonly ISplitService _splitService;
        private readonly IBillService _billService;
        private readonly IBillNormalService _billNormalService;
        private readonly ITariffService _tariffService;

        #endregion

        #region Constructors
        public HandheldController(
            ISplitService splitService,
            IBillService billService,
            IBillNormalService billNormalService,
            ITariffService tariffService)
        {
            this._splitService = splitService;
            this._billService = billService;
            this._billNormalService = billNormalService;
            this._tariffService = tariffService;
        }

        #endregion

        #region Utilities

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GetTermsByYear(string year)
        {
            //this action method gets called via an ajax request
            if (String.IsNullOrEmpty(year))
                throw new ArgumentNullException("year");

            var bills = _billService.GetBills();

            var terms = (from b in bills
                         where b.Year.ToString() == year
                         orderby b.Term
                         select b.Term).Distinct().ToList();

            return Json(terms, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Methods

        #region HH
        public ActionResult Index()
        {
            var model = new HandheldModel();

            var bills = _billService.GetBills();
            var years = (from y in bills
                         orderby y.Year
                         select y.Year).Distinct();

            model.Years.Add(new SelectListItem { Text = "انتخاب", Value = "0" });
            foreach (var year in years)
                model.Years.Add(new SelectListItem { Text = year.ToString(), Value = year.ToString() });

            var splits = _splitService.GetAllSplits();
            model.Splits.Add(new SelectListItem { Text = "انتخاب", Value = "0" });
            foreach (var split in splits)
                model.Splits.Add(new SelectListItem { Text = split.Id + " " + split.Title, Value = split.Id.ToString() });

            return View("handheld", model);
        }

        public ActionResult ReadHandheldFile(string filename)
        {
            StringBuilder content = new StringBuilder();
            try
            {
                filename = filename.Substring(filename.LastIndexOf(@"\", StringComparison.Ordinal) + 1);
                string p = Path.Combine(Server.MapPath("~/Content/Handheld/Upload/"), filename);

                int y = Convert.ToInt32("13" + filename.Substring(0, 2));  // HH_output name format (yytt#####.txt)
                int t = Convert.ToInt32(filename.Substring(2, 2));

                var fileText = new StreamReader(p, Encoding.GetEncoding(1256));
                IList<BillModel> model = new List<BillModel>();


                string line;

                int counter = 0;

                while ((line = fileText.ReadLine()) != null)
                {
                    var culture = new CultureInfo("en");

                    var spid = Convert.ToInt32(line.Substring(0, 10));
                    var spexist = _splitService.CheckExist(spid);
                    if (spexist != null)
                    {
                        counter++;
                        var bill = new BillModel
                        {
                            SplitID = Convert.ToInt32(line.Substring(0, 10)),
                            // skip 7, 1
                            DateReadPersian = line.Substring(18, 10),
                            // skip 7, 2
                            MidRead = Convert.ToDouble(line.Substring(37, 10)),
                            MaxRead = Convert.ToDouble(line.Substring(47, 10)),
                            MinRead = Convert.ToDouble(line.Substring(57, 10)),
                            ReactRead = Convert.ToDouble(line.Substring(67, 10)),
                            Year = y,
                            Term = t,
                            PowerRead = Double.Parse(
                                line.Substring(123, 10),
                                NumberStyles.Float | NumberStyles.AllowThousands,
                                (IFormatProvider)culture.GetFormat(typeof(NumberFormatInfo))),
                            DateReadPrePersian = line.Substring(18, 10),
                        };

                        bill.rowNumber = counter;

                        bill.selected = true;

                        model.Add(bill);
                    }

                }

                var gridModel = new Energy.Web.Framework.Kendoui.DataSourceResult
                {
                    Data = model,
                    Total = model.Count
                };


                return Json(gridModel);

            }
            catch (Exception)
            {
                content.AppendLine("خطا در خواندن فایل");
                return Content(content.ToString());
            }

        }

        [HttpPost]
        public ActionResult CreateNewTerm([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request, BillModel[] griditems, string billtype)
        {
            StringBuilder content = new StringBuilder();

            if (griditems == null)
            {
                content.AppendLine("خطا در خواندن فایل");
                return Content(content.ToString());

            }

            string year = griditems[0].Year.ToString();
            string term = griditems[0].Term.ToString();

            try
            {
                foreach (var bill in griditems)
                {
                    if (bill.selected)
                    {
                        var spid = bill.SplitID;
                        var spexist = _splitService.CheckExist(spid);

                        var lastbill = _billService.findLastBill(spid);
                        long debit = 0;
                        if (lastbill != null)
                            debit = lastbill.Payable + lastbill.Round;

                        if (bill.Type == "corrective")
                        {
                            var archiveBill = _billService.GetBillBysyt(spid, griditems[0].Year, griditems[0].Term);
                            archiveBill.Type = "archive";
                            _billService.UpdateBill(archiveBill);
                        }


                        if (spexist)
                        {
                            var newBill = new Bill
                            {
                                SplitID = Convert.ToInt32(bill.SplitID),
                                // skip 7, 1
                                DateRead = DateHelper.PersianToDateTime(bill.DateReadPersian),
                                // skip 7, 2
                                MidRead = Convert.ToDouble(bill.MidRead),
                                MaxRead = Convert.ToDouble(bill.MaxRead),
                                MinRead = Convert.ToDouble(bill.MinRead),
                                ReactRead = Convert.ToDouble(bill.ReactRead),
                                Year = bill.Year,
                                Term = bill.Term,
                                PowerRead = Convert.ToDouble(bill.PowerRead),
                                DateReadPre = DateHelper.PersianToDateTime(bill.DateReadPrePersian),
                                Type = billtype,
                                LastPayment1 = 0,
                                LastPayment2 = 0,
                                Debit = debit,
                                issueDate = DateTime.Now,
                            };

                            bool exist = _billService.CheckExist(bill.SplitID, bill.Year, bill.Term, billtype);

                            if (!exist)
                            {
                                _billService.InsertBill(newBill);
                            }
                            else
                            {
                                content.AppendLine("صورتحساب انشعاب " + bill.SplitID + " مربوط به سال " + bill.Year + " دوره " + bill.Term +
                                    " ، از قبل در سیستم موجود است<br>");
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                content.AppendLine("خطا در وارد کردن اطلاعات ورودی");
                return Content(content.ToString());
            }

            if (billtype == "main")
                content.AppendLine("صورتحساب های جدید برای دوره " + term + " سال " + year + " ذخیره شد");
            else if (billtype == "midterm")
                content.AppendLine("صورتحساب میان دوره ذخیره شد");
            else if (billtype == "corrective")
                content.AppendLine("صورتحساب های اصلاحیه برای دوره " + term + " سال " + year + "ذخیره شد");


            return Json(new { msg = content.ToString() }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CreateMidTerm([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request, BillModel[] griditems, string billtype)
        {
            StringBuilder content = new StringBuilder();

            if (griditems == null)
            {
                content.AppendLine("خطا در خواندن فایل");
                return Content(content.ToString());
            }

            string year = griditems[0].Year.ToString();
            string term = griditems[0].Term.ToString();

            try
            {
                foreach (var bill in griditems)
                {
                    if (bill.selected)
                    {
                        var spid = bill.SplitID;
                        var spexist = _splitService.CheckExist(spid);

                        var lastbill = _billService.findLastBill(spid);
                        long debit = 0;
                        if (lastbill != null)
                            debit = lastbill.Payable + lastbill.Round;

                        if (spexist)
                        {
                            var newBill = new Bill
                            {
                                SplitID = Convert.ToInt32(bill.SplitID),
                                // skip 7, 1
                                DateRead = DateHelper.PersianToDateTime(bill.DateReadPersian),
                                // skip 7, 2
                                MidRead = Convert.ToDouble(bill.MidRead),
                                MaxRead = Convert.ToDouble(bill.MaxRead),
                                MinRead = Convert.ToDouble(bill.MinRead),
                                ReactRead = Convert.ToDouble(bill.ReactRead),
                                Year = bill.Term != 1 ? bill.Year : bill.Year - 1,
                                Term = bill.Term != 1 ? bill.Term - 1 : 12,
                                PowerRead = Convert.ToDouble(bill.PowerRead),
                                DateReadPre = DateHelper.PersianToDateTime(bill.DateReadPrePersian),
                                Type = billtype,
                                LastPayment1 = 0,
                                LastPayment2 = 0,
                                Debit = debit,
                                issueDate = DateTime.Now,
                            };

                            bool exist = _billService.CheckExist(bill.SplitID, bill.Term != 1 ? bill.Year : bill.Year - 1, bill.Term != 1 ? bill.Term - 1 : 12, billtype);

                            if (!exist)
                            {
                                _billService.InsertBill(newBill);
                            }
                            else
                            {
                                content.AppendLine("صورتحساب انشعاب " + bill.SplitID + " مربوط به سال " + bill.Year + " دوره " + bill.Term +
                                    " ، از قبل در سیستم موجود است<br>");
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                content.AppendLine("خطا در وارد کردن اطلاعات ورودی");
                return Content(content.ToString());
            }

            if (billtype == "main")
                content.AppendLine("صورتحساب های جدید برای دوره " + term + " سال " + year + " ذخیره شد");
            else if (billtype == "midterm")
                content.AppendLine("صورتحساب میان دوره ذخیره شد");
            else if (billtype == "corrective")
                content.AppendLine("صورتحساب های اصلاحیه برای دوره " + term + " سال " + year + "ذخیره شد");


            return Json(new { msg = content.ToString() }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CreateCorrectiveTerm(SplitModel[] griditems, string year, string term, string endYear = "", string endTerm = "")
        {
            StringBuilder content = new StringBuilder();

            var selectedItems = griditems.ToList().Where(s => s.selected == true);
            if (selectedItems.Count() == 0)
                return Json(new { msg = "هیچ انشعابی در لیست انتخاب نشده است".ToString() }, JsonRequestBehavior.AllowGet);

            foreach (var split in selectedItems)
            {
                if (Convert.ToInt32(endYear) == 0)
                {
                    var archiveBill = _billService.GetBillBysyt(split.Id, Convert.ToInt32(year), Convert.ToInt32(term));
                    if (archiveBill != null)
                    {
                        archiveBill.Type = "archive";
                        _billService.UpdateBill(archiveBill);

                        var bill = archiveBill;
                        bill.Type = "corrective";

                        bool exist = _billService.CheckExist(bill.SplitID, bill.Year, bill.Term, "corrective");

                        if (!exist)
                        {
                            _billService.InsertBill(bill);
                            var newBill = _billService.GetCorrectiveBillBysyt(split.Id, Convert.ToInt32(year), Convert.ToInt32(term));
                            var result = Calculate.CalculateBill(_billService, _splitService, _tariffService, _billNormalService, newBill.Id);

                            if (result == 2)
                            {
                                content.AppendLine(string.Format("محاسبه صورتحساب انشعاب {0} به دلیل غیرفعال بودن انشعاب مربوطه، انجام نشد", split.Id));
                                return Json(new { msg = content.ToString() }, JsonRequestBehavior.AllowGet);
                            }
                            else if (result == 0)
                            {
                                content.AppendLine(string.Format("محاسبه صورتحساب انشعاب {0} با مشکل مواجه شد. لطفا با پشتیبانی سیستم تماس بگیرید", split.Id));
                                return Json(new { msg = content.ToString() }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            content.AppendLine("صورتحساب اصلاحی انشعاب " + bill.SplitID + " مربوط به سال " + bill.Year + " دوره " + bill.Term +
                                " ، از قبل در سیستم موجود است<br>");
                        }
                    }
                }
                else
                {
                    int startTerm = Convert.ToInt32(term);
                    int startYear = Convert.ToInt32(year);

                    var archiveBills = _billService.GetBillsByFilter1(fyy: startYear, ftt: startTerm, domain: true,
                         toyy: Convert.ToInt32(endYear), tott: Convert.ToInt32(endTerm), region_Id: 0, distribution_Id: 0, cTariff: 0, splitId: split.Id);

                    foreach (var bill in archiveBills)
                    {
                        bill.Type = "archive";
                        _billService.UpdateBill(bill);
                    }

                    var lastBill = _billService.GetArchiveBillBysyt(split.Id, Convert.ToInt32(endYear), Convert.ToInt32(endTerm));
                    var firstBill = _billService.GetArchiveBillBysyt(split.Id, Convert.ToInt32(startYear), Convert.ToInt32(startTerm));
                    if (firstBill == null || lastBill == null)
                        continue;
                    var preBill = _billService.findPreviousBill(firstBill.Id, firstBill.SplitID);

                    long paymentTotal = 0;
                    foreach (var bill in archiveBills)
                    {
                        paymentTotal += bill.LastPayment1;
                    }

                    var newBill = new Bill()
                    {
                        SplitID = Convert.ToInt32(split.Id),
                        // skip 7, 1
                        DateRead = lastBill.DateRead,
                        // skip 7, 2
                        MidRead = lastBill.MidRead,
                        MaxRead = lastBill.MaxRead,
                        MinRead = lastBill.MinRead,
                        ReactRead = lastBill.ReactRead,
                        PowerRead = lastBill.PowerRead,
                        Year = lastBill.Year,
                        Term = lastBill.Term,
                        DateReadPre = firstBill.DateReadPre,
                        MidReadPre = firstBill.MidReadPre,
                        MaxReadPre = firstBill.MaxReadPre,
                        MinReadPre = firstBill.MinReadPre,
                        ReactReadPre = firstBill.ReactReadPre,
                        PowerReadPre = firstBill.PowerReadPre,
                        Type = "corrective",
                        LastPayment1 = 0,
                        LastPayment2 = 0,
                        Debit = preBill != null ? preBill.Payable - paymentTotal : 0 - paymentTotal,
                        issueDate = DateTime.Now,
                    };

                    bool exist = _billService.CheckExist(newBill.SplitID, newBill.Year, newBill.Term, "corrective");

                    if (!exist)
                    {
                        _billService.InsertBill(newBill);
                        //var bill = _billService.GetCorrectiveBillBysyt(split.Id, Convert.ToInt32(endYear), Convert.ToInt32(endTerm));
                        //var result = Calculate.CalculateBill(_billService, _splitService, _tariffService, _billNormalService, bill.Id);

                        //if (result == 2)
                        //{
                        //    content.AppendLine(string.Format("محاسبه صورتحساب انشعاب {0} به دلیل غیرفعال بودن انشعاب مربوطه، انجام نشد", split.Id));
                        //    return Json(new { msg = content.ToString() }, JsonRequestBehavior.AllowGet);
                        //}
                        //else if (result == 0)
                        //{
                        //    content.AppendLine(string.Format("محاسبه صورتحساب انشعاب {0} با مشکل مواجه شد. لطفا با پشتیبانی سیستم تماس بگیرید", split.Id));
                        //    return Json(new { msg = content.ToString() }, JsonRequestBehavior.AllowGet);
                        //}
                    }
                    else
                    {
                        content.AppendLine("صورتحساب اصلاحی انشعاب " + newBill.SplitID + " مربوط به سال " + newBill.Year + " دوره " + newBill.Term +
                            " ، از قبل در سیستم موجود است<br>");
                    }
                }
            }

            return Json(new { msg = "صورتحساب اصلاحی با موفقیت ایجاد شد".ToString() }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CreateHandheldFile(string year, string term)
        {
            int yy = Convert.ToInt32(year);
            int tt = Convert.ToInt32(term);

            StringBuilder content = new StringBuilder();

            var bills = _billService.GetBills_Splits(yy, tt);
            if (!bills.Any())
            {
                content.AppendLine("اطلاعاتی جهت تولید وجود ندارد");
                return Content(content.ToString());
            }

            int counter = 0;
            string fl = term.Trim().PadLeft(2, '0') + "000099.txt";
            string path = Server.MapPath("~/Content/") + "/HandHeld/Download/" + fl;


            using (var sw = new StreamWriter(path, false, Encoding.GetEncoding(1256)))
                foreach (var b in bills)
                {
                    var s = "";
                    s = s + b.Split.Id.ToString().Trim().PadLeft(10, '0');  // 10 شناسايي
                    s = s + b.Split.Id.ToString().Trim().PadLeft(7, '0');  // 10 شناسايي - پرونده 
                    s = s + "0";   // 1 رقم کنترل
                    s = s + b.Split.Id.ToString().Trim().PadLeft(9, '0');  // 9 شناسايي -اشتراک
                    if (b.Split.Title.Length > 35)
                    {
                        s = s + b.Split.Title.Substring(0, 35);
                    }
                    else
                    {
                        s = s + b.Split.Title.PadLeft(35, ' ');
                    }
                    b.Split.Address = (b.Split.Address ?? "");
                    if (b.Split.Address.Length > 45)
                    {
                        s = s + b.Split.Address.Substring(0, 45);
                    }
                    else
                    {
                        s = s + b.Split.Address.PadLeft(45, ' ');
                    }
                    s = s + b.Split.Factor.ToString().Trim().PadLeft(5, '0');
                    s = s + ((int)b.Split.Timer).ToString().Trim().PadLeft(1, '0');
                    s = s + ((int)b.Split.Phase).ToString().Trim().PadLeft(1, '0');
                    s = s + b.Split.Amp.ToString().Trim().PadLeft(7, '0');
                    s = s + (b.Split.CTariffId - 1).ToString().PadLeft(5, '0');
                    s = s + b.Split.Power.ToString().Trim().PadLeft(6, '0');
                    s = s + b.Split.InstallDate.PersianDateN("/").PadLeft(10, '0');
                    s = s + "".PadLeft(180, '0');
                    s = s + (b.Payable - b.Paid + b.Round).ToString().Trim().PadLeft(13, '0');
                    s = s + "0";
                    s = s + b.Split.Digits.ToString().Trim().PadLeft(2, '0');
                    s = s + b.Split.Digits.ToString().Trim().PadLeft(2, '0');
                    s = s + b.Split.Factor.ToString().Trim().PadLeft(5, '0');
                    s = s + "".PadLeft(2, '0');
                    s = s + b.DateRead.PersianDate("/").PadLeft(10, '0');
                    s = s + b.MidRead.ToString().Trim().PadLeft(10, '0');
                    s = s + b.MaxRead.ToString().Trim().PadLeft(10, '0');
                    s = s + b.MinRead.ToString().Trim().PadLeft(10, '0');
                    s = s + b.ReactRead.ToString().Trim().PadLeft(10, '0');
                    s = s + "".PadLeft(46, '0');
                    s = s + b.Split.SerialNo.ToString().Trim().PadLeft(13, '0');
                    s = s + b.Split.SerialNo.ToString().Trim().PadLeft(13, '0');
                    s = s + ((int)b.Split.Timer).ToString().Trim().PadLeft(1, '0');
                    s = s + (b.Split.Demand == (int)CDemand.Demand ? "1" : "0");
                    s = s + "0";
                    s = s + (b.Split.Postal ?? "").PadLeft(15, '0');
                    s = s + ((int)b.Split.Volt).ToString().Trim().PadLeft(1, '0');
                    s = s + "".PadLeft(6, '0');
                    s = s + b.Year.ToString().Trim().PadLeft(4, '0') +
                        b.Term.ToString().Trim().PadLeft(2, '0');
                    s = s + b.PowerRead.ToString().Trim().PadLeft(10, '0');
                    sw.WriteLine(s);
                    counter = counter + 1;
                };

            content.AppendLine(" تهیه فایل با موفقیت انجام شد. " + " تعداد " + counter + " رکورد تولید شده است  ");
            return Json(new { msg = content.ToString() }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region File
        public ActionResult HH_UploadFile(IEnumerable<HttpPostedFileBase> files)
        {
            StringBuilder content = new StringBuilder();

            HttpPostedFileBase file = null;
            foreach (var f in files)
                file = f;
            try
            {
                if (file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/Handheld/Upload/"), fileName);
                    file.SaveAs(path);
                    content.AppendLine("فایل با موفقیت اپلود شد");
                }
            }
            catch (Exception er)
            {
                content.AppendLine("فایل قابل ارسال نیست");
                return Content(content.ToString());
            }
            return Content(content.ToString());
        }

        public ActionResult HH_DownloadFile(int id)
        {
            StringBuilder content = new StringBuilder();

            string term = id.ToString();

            try
            {
                string fl = term.Trim().PadLeft(2, '0') + "000099.txt";
                string path = Server.MapPath("~/Content") + "/HandHeld/Download/" + fl;
                Response.Clear();
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + fl);
                Response.TransmitFile(@path);
                Response.End();
            }
            catch (Exception er)
            {
                content.AppendLine("فایلی جهت ذخیره کردن وجود ندارد");
            }

            content.AppendLine("");
            return Json(new { msg = content.ToString() }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region create zero term

        public ActionResult CreateZeroTerm(string splitId)
        {
            StringBuilder content = new StringBuilder();

            try
            {
                var firstBill = new Bill
                {
                    SplitID = Convert.ToInt32(splitId),
                    DateRead = DateTime.Now,
                    MidRead = 0,
                    MaxRead = 0,
                    MinRead = 0,
                    ReactRead = 0,
                    PowerRead = 0,
                    Year = _billService.getLastBill().Year,
                    Term = _billService.getLastBill().Term,
                    DateReadPre = DateTime.Now,
                    Type = "zero",
                    LastPayment1 = 0,
                    LastPayment2 = 0,
                    Debit = 0,
                    issueDate = DateTime.Now,
                };

                _billService.InsertBill(firstBill);

                content.AppendLine("دوره صفر برای انشعاب شماره" + splitId + " ایجاد شد");
                return Json(new { msg = content.ToString() }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                content.AppendLine("ایجاد دوره صفر با خطا مواجه شد . لطفا با ادمین سیستم تماس بگیرید");
                return Json(new { msg = content.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #endregion

        #region HighLow

        public JsonResult HighLow([DataSourceRequest] Kendo.Mvc.UI.DataSourceRequest request, BillModel[] griditems)
        {
            StringBuilder content = new StringBuilder();

            //if (griditems == null)
            //{
            //    content.AppendLine("خطا در خواندن فایل");
            //    return Content(content.ToString());
            //}

            int year = griditems[0].Year;
            int term = griditems[0].Term;

            IList<HighLowModel> data = new List<HighLowModel>();

            try
            {
                foreach (var bill in griditems)
                {
                    var spid = bill.SplitID;


                    var spexist = _splitService.CheckExist(spid);

                    var previousbill = _billService.GetBillBysyt(spid, term == 1 ? year - 1 : year, term == 1 ? 12 : term - 1);

                    if (spexist && bill.selected)
                    {
                        var split = _splitService.GetSplitById(spid);
                        var Factor = split.Factor;

                        var model = new HighLowModel();
                        model.rowNumber = bill.rowNumber;

                        //1- Active HighLow
                        if (bill.MidRead == 0 || bill.MaxRead == 0 || bill.MinRead == 0)
                        {
                            model.hasError = true;
                            model.zeroActive = true;
                            model.description = "اکتیو قرایت شده برابر با صفر شده است<br/>";
                        }
                        if ((bill.MidRead - previousbill.MidRead) < 0 ||
                            (bill.MaxRead - previousbill.MaxRead) < 0 ||
                            (bill.MinRead - previousbill.MinRead) < 0)
                        {
                            model.hasError = true;
                            model.negativeActive = true;
                            model.description += "مقدار مصرف اکتیو کوچکتر از صفر شده است<br/>";
                        }
                        if (((bill.MinRead - previousbill.MinRead) * Factor > (1.5 * previousbill.MinUsed)) ||
                            ((bill.MaxRead - previousbill.MaxRead) * Factor > (1.5 * previousbill.MaxUsed)) ||
                            ((bill.MidRead - previousbill.MidRead) * Factor > (1.5 * previousbill.MidUsed)))
                        {
                            model.hasError = true;
                            model.overflowActive = true;
                            model.description += "مقدار اکتیو مصرفی از 1.5 برابر دوره گذشته بیشتر شده است<br/>";
                        }
                        if (((bill.MinRead - previousbill.MinRead) * Factor < (0.5 * previousbill.MinUsed)) ||
                            ((bill.MaxRead - previousbill.MaxRead) * Factor < (0.5 * previousbill.MaxUsed)) ||
                            ((bill.MidRead - previousbill.MidRead) * Factor < (0.5 * previousbill.MidUsed)))
                        {
                            model.hasError = true;
                            model.minorActive = true;
                            model.description += "مقدار اکتیو مصرفی از نصف دوره گذشته کمتر است<br/>";
                        }

                        if ((split.Demand == (int)CDemand.Demand))
                        {
                            //2- Reactive HighLow
                            if (bill.ReactRead == 0)
                            {
                                model.hasError = true;
                                model.zeroReact = true;
                                model.description += "راکتیو قرایت شده برابر با صفر شده است<br/>";
                            }
                            if ((bill.ReactRead - previousbill.ReactRead) < 0)
                            {
                                model.hasError = true;
                                model.negativeReact = true;
                                model.description += "مقدار مصرف راکتیو کوچکتر از صفر شده است<br/>";
                            }
                            if ((bill.ReactRead - previousbill.ReactRead) * Factor > (1.5 * previousbill.ReactUsed))
                            {
                                model.hasError = true;
                                model.overflowReact = true;
                                model.description += "مقدار راکتیو مصرفی از 1.5 برابر دوره گذشته بیشتر شده است<br/>";
                            }
                            if ((bill.ReactRead - previousbill.ReactRead) * Factor < (0.5 * previousbill.ReactUsed))
                            {
                                model.hasError = true;
                                model.minorReact = true;
                                model.description += "مقدار راکتیو مصرفی از نصف دوره گذشته کمتر است<br/>";
                            }


                            //3- Demand HighLow
                            if (bill.PowerRead == 0)
                            {
                                model.hasError = true;
                                model.zeroPower = true;
                                model.description += "دیماند قرایت شده برابر با صفر شده است<br/>";
                            }
                        }
                        data.Add(model);
                    }
                }

            }
            catch (Exception)
            {
                //content.AppendLine("");
                //return Content(content.ToString());
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region Bills Description
        public ActionResult CreateDescription(string Description, int year, int term)
        {
            StringBuilder content = new StringBuilder();

            if (Description.IsNullOrEmpty())
                return RedirectToAction("Index");

            var bills = _billService.GetBillsByDate(year, term);

            foreach (var bill in bills)
            {
                bill.Description = Description;
                _billService.UpdateBill(bill);
            }

            content.AppendLine("توضیحات صورتحساب با موفقیت ذخیره شد");
            return Content(content.ToString());
        }

        #endregion
    }
}