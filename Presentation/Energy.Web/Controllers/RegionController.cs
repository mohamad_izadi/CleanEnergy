﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Energy.Web.Extensions;
using Energy.Web.Models.Directory;
using Energy.Core;
using Energy.Core.Domain.Directory;
using Energy.Services.Common;
using Energy.Services.Directory;
using Energy.Services.ExportImport;
using Energy.Services.Localization;
using Energy.Services.Security;
using Energy.Services.Stores;
using Energy.Web.Framework.Controllers;
using Energy.Web.Framework.Kendoui;
using Energy.Web.Framework.Mvc;

namespace Energy.Web.Controllers
{
    public partial class RegionController : BaseAdminController
    {
        #region Fields

        private readonly IRegionService _regionService;
        private readonly IDistributionAreaService _distributionAreaService;
        private readonly IPermissionService _permissionService;
        private readonly IExportManager _exportManager;

        #endregion

        #region Constructors

        public RegionController(IRegionService regionService,
            IDistributionAreaService distributionAreaService,
            IPermissionService permissionService,
            IExportManager exportManager)
        {
            this._regionService = regionService;
            this._distributionAreaService = distributionAreaService;
            this._permissionService = permissionService;
            this._exportManager = exportManager;
        }

        #endregion

        #region Regions

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageRegions))
                return AccessDeniedView();

            return View();
        }

        [HttpPost]
        public ActionResult RegionList(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageRegions))
                return AccessDeniedView();

            var regions = _regionService.GetAllRegions(true);
            var gridModel = new DataSourceResult
            {
                Data = regions.Select(x => x.ToModel()),
                Total = regions.Count
            };

            return Json(gridModel);
        }

        public ActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageRegions))
                return AccessDeniedView();

            var model = new RegionModel();
            //default values
            model.Published = true;
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(RegionModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageRegions))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var region = model.ToEntity();
                _regionService.InsertRegion(region);

                SuccessNotification("Admin.Configuration.Regions.Added");
                return continueEditing ? RedirectToAction("Edit", new { id = region.Id }) : RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageRegions))
                return AccessDeniedView();

            var region = _regionService.GetRegionById(id);
            if (region == null)
                //No Region found with the specified id
                return RedirectToAction("List");

            var model = region.ToModel();

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit(RegionModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageRegions))
                return AccessDeniedView();

            var region = _regionService.GetRegionById(model.Id);
            if (region == null)
                //No Region found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                region = model.ToEntity(region);
                _regionService.UpdateRegion(region);

                SuccessNotification("Admin.Configuration.Regions.Updated");

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabIndex();

                    return RedirectToAction("Edit", new { id = region.Id });
                }
                return RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageRegions))
                return AccessDeniedView();

            var region = _regionService.GetRegionById(id);
            if (region == null)
                //No Region found with the specified id
                return RedirectToAction("List");

            try
            {
                _regionService.DeleteRegion(region);

                SuccessNotification("Admin.Configuration.Countries.Deleted");
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Edit", new { id = region.Id });
            }
        }

        [HttpPost]
        public ActionResult PublishSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageRegions))
                return AccessDeniedView();

            if (selectedIds != null)
            {
                var regions = _regionService.GetRegionsByIds(selectedIds.ToArray());
                foreach (var region in regions)
                {
                    region.Published = true;
                    _regionService.UpdateRegion(region);
                }
            }

            return Json(new { Result = true });
        }
        [HttpPost]
        public ActionResult UnpublishSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageRegions))
                return AccessDeniedView();

            if (selectedIds != null)
            {
                var regions = _regionService.GetRegionsByIds(selectedIds.ToArray());
                foreach (var region in regions)
                {
                    region.Published = false;
                    _regionService.UpdateRegion(region);
                }
            }
            return Json(new { Result = true });
        }

        #endregion

        #region Distribution Area

        [HttpPost]
        public ActionResult DistributionAreas(int RegionId, DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageRegions))
                return AccessDeniedView();

            var das = _distributionAreaService.GetDistributionAreasByRegionId(RegionId, true);

            var gridModel = new DataSourceResult
            {
                Data = das.Select(x => x.ToModel()),
                Total = das.Count
            };
            return Json(gridModel);
        }

        //create
        public ActionResult DistributionAreaCreatePopup(int RegionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageRegions))
                return AccessDeniedView();

            var model = new DistributionAreaModel();
            model.RegionId = RegionId;
            //default value
            model.Published = true;
            return View(model);
        }

        [HttpPost]
        public ActionResult DistributionAreaCreatePopup(string btnId, string formId, DistributionAreaModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageRegions))
                return AccessDeniedView();

            var Region = _regionService.GetRegionById(model.RegionId);
            if (Region == null)
                //No Region found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                var sp = model.ToEntity();

                _distributionAreaService.InsertDistributionArea(sp);

                ViewBag.RefreshPage = true;
                ViewBag.btnId = btnId;
                ViewBag.formId = formId;
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        //edit
        public ActionResult DistributionAreaEditPopup(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageRegions))
                return AccessDeniedView();

            var sp = _distributionAreaService.GetDistributionAreaById(id);
            if (sp == null)
                //No state found with the specified id
                return RedirectToAction("List");

            var model = sp.ToModel();

            return View(model);
        }

        [HttpPost]
        public ActionResult DistributionAreaEditPopup(string btnId, string formId, DistributionAreaModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageRegions))
                return AccessDeniedView();

            var sp = _distributionAreaService.GetDistributionAreaById(model.Id);
            if (sp == null)
                //No state found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                sp = model.ToEntity(sp);
                _distributionAreaService.UpdateDistributionArea(sp);

                ViewBag.RefreshPage = true;
                ViewBag.btnId = btnId;
                ViewBag.formId = formId;
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        public ActionResult DistributionAreaDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageRegions))
                return AccessDeniedView();

            var state = _distributionAreaService.GetDistributionAreaById(id);
            if (state == null)
                throw new ArgumentException("No state found with the specified id");

            //int RegionId = state.RegionId;
            _distributionAreaService.DeleteDistributionArea(state);

            return new NullJsonResult();
        }

        //#endregion

        //#region Export / import

        //public ActionResult ExportCsv()
        //{
        //    if (!_permissionService.Authorize(StandardPermissionProvider.ManageCountries))
        //        return AccessDeniedView();

        //    string fileName = String.Format("states_{0}_{1}.txt", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"), CommonHelper.GenerateRandomDigitCode(4));

        //    var states = _distributionAreaService.GetStateProvinces(true);
        //    string result = _exportManager.ExportStatesToTxt(states);

        //    return File(Encoding.UTF8.GetBytes(result), "text/csv", fileName);
        //}

        //[HttpPost]
        //public ActionResult ImportCsv(FormCollection form)
        //{
        //    if (!_permissionService.Authorize(StandardPermissionProvider.ManageCountries))
        //        return AccessDeniedView();

        //    try
        //    {
        //        var file = Request.Files["importcsvfile"];
        //        if (file != null && file.ContentLength > 0)
        //        {
        //            int count = _importManager.ImportStatesFromTxt(file.InputStream);
        //            SuccessNotification(String.Format(_localizationService.GetResource("Admin.Configuration.Countries.ImportSuccess"), count));
        //            return RedirectToAction("List");
        //        }
        //        ErrorNotification(_localizationService.GetResource("Admin.Common.UploadFile"));
        //        return RedirectToAction("List");
        //    }
        //    catch (Exception exc)
        //    {
        //        ErrorNotification(exc);
        //        return RedirectToAction("List");
        //    }
        //}

        #endregion
    }
}
