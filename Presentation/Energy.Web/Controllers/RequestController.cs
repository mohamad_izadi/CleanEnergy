﻿using System;
using System.Collections.Generic;
using Energy.Services.Helpers;
using System.Web;
using System.Linq;
using System.Web.Mvc;
using Energy.Core.Domain.Requests;
using Energy.Services.Security;
using Energy.Services.Splits;
using Energy.Services.Tariffs;
using Energy.Services.Requests;
using Energy.Services.Directory;
using Energy.Web.Models.Requests;
using Energy.Web.Extensions;
using Energy.Core.Infrastructure;
using Energy.Core;
using Energy.Web.Framework.Kendoui;
using Energy.Services.Files;
using Energy.Web.Framework.Mvc;

namespace Energy.Web.Controllers
{
    public class RequestController : BasePublicController
    {
        #region Fields

        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IPermissionService _permissionService;
        private readonly ISplitService _splitService;
        private readonly ITariffService _tariffService;
        private readonly IRequestService _requestService;
        private readonly IRegionService _regionService;
        private readonly IDistributionAreaService _distributionAreaService;
        private readonly IFileService _fileService;

        #endregion

        #region Constructors

        public RequestController(IDateTimeHelper dateTimeHelper,
            IPermissionService permissionService,
            ISplitService splitService,
            ITariffService tariffService,
            IRequestService requestService,
            IRegionService regionService,
            IDistributionAreaService distributionAreaService,
            IFileService fileService)
        {
            this._dateTimeHelper = dateTimeHelper;
            this._permissionService = permissionService;
            this._splitService = splitService;
            this._tariffService = tariffService;
            this._requestService = requestService;
            this._regionService = regionService;
            this._distributionAreaService = distributionAreaService;
            this._fileService = fileService;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual void PrepareRequestModel(RequestModel model)
        {
            model.Regions.Add(new SelectListItem { Text = "انتخاب", Value = "0" });

            foreach (var s in _regionService.GetAllRegions())
            {
                model.Regions.Add(new SelectListItem
                {
                    Text = s.Name,
                    Value = s.Id.ToString(),
                    Selected = s.Id == model.Region_Id
                });
            }

            //cities
            var distributionAreas = _distributionAreaService.GetDistributionAreasByRegionId(model.Region_Id).ToList();
            if (distributionAreas.Count > 0)
            {
                model.DistributionAreas.Add(new SelectListItem { Text = "انتخاب", Value = "0" });

                foreach (var da in distributionAreas)
                {
                    model.DistributionAreas.Add(new SelectListItem { Text = da.Name, Value = da.Id.ToString(), Selected = (da.Id == model.DistributionArea_Id) });
                }
            }
            else
            {
                bool anyRegionSelected = model.Regions.Any(x => x.Selected);

                model.DistributionAreas.Add(new SelectListItem
                {
                    Text = anyRegionSelected ? "سایر" : "انتخاب",
                    Value = "0"
                });
            }
        }

        #endregion
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.SendRequests))
                return AccessDeniedView();

            var model = new RequestModel();
            PrepareRequestModel(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(RequestModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.SendRequests))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var request = model.ToEntity();
                request.Customer_Id = EngineContext.Current.Resolve<IWorkContext>().CurrentCustomer.Id;
                request.CreateDate = DateTime.Now;
                request.RequestType_Id = 1; 
                _requestService.InsertRequest(request);

                SuccessNotification("Admin.Configuration.Regions.Added");
                return RedirectToAction("Edit", new { id = request.Id });
            }

            //If we got this far, something failed, redisplay form
            PrepareRequestModel(model);
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.SendRequests))
                return AccessDeniedView();

            var request = _requestService.GetRequestById(id);
            if (request == null)
                //No request item found with the specified id
                return RedirectToAction("Index");

            var model = request.ToModel();

            return View("Create", model);
        }

        #region request attachments
        public ActionResult RequestFileAdd(int fileId, string description, int requestId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.SendRequests))
                return AccessDeniedView();

            if (fileId == 0)
                throw new ArgumentException();

            var request = _requestService.GetRequestById(requestId);
            if (request == null)
                throw new ArgumentException("No request found with the specified id");

            _requestService.InsertRequestFile(new RequestFile
            {
                File_Id = fileId,
                Request_Id = requestId,
                FileType = 1,
                Description = description,
            });

            return Json(new { Result = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RequestFileList(DataSourceRequest command, int requestId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.SendRequests))
                return AccessDeniedView();

            var requestFiles = _requestService.GetRequestFilesByrequestId(requestId);
            var requestFilesModel = requestFiles
                .Select(x => new RequestModel.RequestFileModel
                {
                    Id = x.Id,
                    Request_Id = x.Request_Id,
                    File_Id = x.File_Id,
                    FileName = _fileService.GetFileById(x.File_Id).FileName,
                    Description = x.Description,
                })
                .ToList();

            var gridModel = new DataSourceResult
            {
                Data = requestFilesModel,
                Total = requestFilesModel.Count
            };

            return Json(gridModel);
        }

        [HttpPost]
        public ActionResult RequestFileDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.SendRequests))
                return AccessDeniedView();

            var requestFile = _requestService.GetRequestFileById(id);
            if (requestFile == null)
                throw new ArgumentException("No document file found with the specified id");

            var requestId = requestFile.Request_Id;
            var fileId = requestFile.File_Id;

            _requestService.DeleteRequestFile(requestFile);
            var file = _fileService.GetFileById(fileId);
            _fileService.DeleteFile(file);

            return new NullJsonResult();
        }


        #endregion
    }
}