﻿using System.Web.Mvc;
using Energy.Web.Framework.Security;

namespace Energy.Web.Controllers
{
    public partial class HomeController : BasePublicController
    {
        [ProHttpsRequirement(SslRequirement.No)]
        public ActionResult Index()
        {
            return View();
        }
    }
}
