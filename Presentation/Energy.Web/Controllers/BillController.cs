﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Energy.Core;
using Energy.Core.Domain.Customers;
using Energy.Core.Domain.Constant;
using Energy.Services.Common;
using Energy.Services.Bills;
using Energy.Services.Security;
using Energy.Services.Splits;
using Energy.Services.Tariffs;
using Energy.Services.Logging;
using Energy.Web.Models.Splits;
using Energy.Web.Models.Bills;
using Energy.Web.Framework.Kendoui;
using Energy.Web.Extensions;
using System.Text;
using Web.Tools;
using Energy.Web.Framework.Mvc;
using System.IO;

namespace Energy.Web.Controllers
{
    public class BillController : BaseAdminController
    {
        #region Fields

        private readonly ISplitService _splitService;
        private readonly IBillService _billService;
        private readonly IBillNormalService _billNormalService;
        private readonly ITariffService _tariffService;
        private readonly IPermissionService _permissionService;
        private readonly IWorkContext _workContext;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IPdfService _pdfService;

        #endregion

        #region Ctor
        public BillController(
            ISplitService splitService,
            IBillService billService,
            ITariffService tariffService,
            IBillNormalService billNormalService,
            IPermissionService permissionService,
            IWorkContext workContext,
            ICustomerActivityService customerActivityService,
            IPdfService pdfService)
        {
            this._splitService = splitService;
            this._billService = billService;
            this._tariffService = tariffService;
            this._billNormalService = billNormalService;
            this._permissionService = permissionService;
            this._workContext = workContext;
            this._customerActivityService = customerActivityService;
            this._pdfService = pdfService;
        }

        #endregion

        #region Methods
        public ActionResult Index()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBills))
                return AccessDeniedView();

            var model = new SplitListModel();
            return View("Bills", model);
        }

        public ActionResult UploadFile(int id)
        {
            var model = new BillModel.BillPictureModel();
            model.BillId = id;
            var bill = _billService.getById(id);
            if (bill != null)
                model.PicId = bill.PictureId;
            return View(model);
        }

        public ActionResult SavePicture(BillModel.BillPictureModel model)
        {
            var bill = _billService.getById(model.BillId);
            if (bill == null)
                return null;

            bill.PictureId = model.PicId;
            _billService.UpdateBill(bill);

            //activity log
            _customerActivityService.InsertActivity("Bills.AttachFile", model.Description);
            return Json(null);
        }

        public ActionResult CalculateBill(int id)
        {
            StringBuilder content = new StringBuilder();

            var bill = _billService.getById(id);

            var result = Calculate.CalculateBill(_billService, _splitService, _tariffService, _billNormalService, bill.Id);

            if (result == 1)
                content.AppendLine("محاسبه صورتحساب انشعاب  " + bill.SplitID + " مربوط به سال " + bill.Year + " دوره " + bill.Term + " با موفقیت انجام شد ");
            else if (result == 2)
                content.AppendLine("محاسبه صورتحساب به دلیل غیرفعال بودن انشعاب مربوطه، انجام نشد");
            else if (result == 0)
                content.AppendLine("محاسبه صورتحساب با مشکل مواجه شد. لطفا با پشتیبانی سیستم تماس بگیرید");

            return Json(new { msg = content.ToString() });
        }

        public ActionResult CalculateAllBills()
        {
            StringBuilder content = new StringBuilder();

            var bills = _billService.GetBills();

            var lastYear = (from b in bills
                            select b.Year).Max();

            var lastTerm = (from b in bills
                            where b.Year == lastYear
                            select b.Term).Max();

            var lastTermBills = _billService.GetBillsByDate(lastYear, lastTerm);

            foreach (var bill in lastTermBills)
            {
                Calculate.CalculateBill(_billService, _splitService, _tariffService, _billNormalService, bill.Id);
            }

            content.AppendLine("محاسبه صورتحساب های مربوط به سال " + lastYear.ToString() + " دوره " + lastTerm.ToString() + " با موفقیت انجام شد ");

            return Json(new { msg = content.ToString() });
        }

        public ActionResult BillDetail(int id)
        {
            var bill = _billService.getBillaSpiltById(id);

            var model = bill.ToModel();

            if (model.Split.Demand == (int)CDemand.Normal)
            {
                model.billNormal = _billNormalService.GetByBillId(bill.Id);
                model.tariffRates = _billNormalService.GetTariffRates(bill.TariffID);
                return View("BillDetail_N", model);
            }
            else
                return View("BillDetail_D", model);

        }

        public ActionResult PdfBill(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBills))
                return AccessDeniedView();

            //load bills
            var bill = _billService.getById(id);

            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _pdfService.PrintBillToPdf(stream, bill, true);
                bytes = stream.ToArray();
            }
            return File(bytes, "application/pdf", string.Format("bill_{0}{1}_{2}.pdf", bill.Year, bill.Term, bill.Id));
        }

        public ActionResult PdfBillAll()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBills))
                return AccessDeniedView();

            //load bills
            var bills = _billService.GetBills();

            var lastYear = (from b in bills
                            select b.Year).Max();

            var lastTerm = (from b in bills
                            where b.Year == lastYear
                            select b.Term).Max();

            var lastTermBills = _billService.GetBillsByDate(lastYear, lastTerm);

            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _pdfService.PrintBillsToPdf(stream, lastTermBills, true);
                bytes = stream.ToArray();
            }
            return File(bytes, "application/pdf", string.Format("bills_{0}-{1}.pdf", lastYear, lastTerm));
        }

        #endregion

        #region Kendo Methods

        [HttpPost]
        public ActionResult List(DataSourceRequest command, int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBills))
                return AccessDeniedView();

            var bills = _billService.GetAllBills(
                splitId: id,
                year: 0,
                month: 0,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize);

            if (command.Page == 1 && bills.Count > 0)
                bills[0].IsLastTerm = true;

            foreach (var bill in bills)
            {
                switch (bill.Type)
                {
                    case "main":
                        bill.Type = "اصلی";
                        break;
                    case "archive":
                        bill.Type = "بایگانی";
                        break;
                    case "corrective":
                        bill.Type = "اصلاحی";
                        break;
                    case "midterm":
                        bill.Type = "میان دوره";
                        break;
                    case "zero":
                        bill.Type = "کنتور صفر";
                        break;
                }
            }

            var gridModel = new DataSourceResult
            {
                Data = bills,
                Total = bills.TotalCount,
            };

            return Json(gridModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Update(BillModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageBills))
                return AccessDeniedView();

            var bill = _billService.getById(model.Id);

            bill.DateRead = model.DateRead;
            bill.MinRead = model.MinRead;
            bill.MaxRead = model.MaxRead;
            bill.MidRead = model.MidRead;
            bill.ReactRead = model.ReactRead;
            bill.PowerRead = model.PowerRead;
            bill.Debit = model.Debit;

            _billService.UpdateBill(bill);

            return new NullJsonResult();
        }

        #endregion

    }
}