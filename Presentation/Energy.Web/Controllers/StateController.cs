﻿using System;
using System.Linq;
using System.Web.Mvc;
using Energy.Core;
using Energy.Core.Caching;
using Energy.Services.Directory;
using Energy.Services.Localization;
using Energy.Web.Infrastructure.Cache;
using Energy.Core.Domain.Directory;
using System.Collections.Generic;

namespace Energy.Web.Controllers
{
    public partial class StateController : BasePublicController
	{
		#region Fields

        private readonly IStateService _stateService;
        private readonly ICityService _cityService;
        private readonly IRegionService _regionService;
        private readonly IDistributionAreaService _distributionAreaService;
        private readonly IWorkContext _workContext;
        private readonly ICacheManager _cacheManager;


	    #endregion

		#region Constructors

        public StateController(IStateService stateService,
            ICityService cityService,
            IRegionService regionService,
            IDistributionAreaService distributionAreaService,
            IWorkContext workContext,
            ICacheManager cacheManager)
		{
            this._regionService = regionService;
            this._distributionAreaService = distributionAreaService;
            this._stateService = stateService;
            this._cityService = cityService;
            this._workContext = workContext;
            this._cacheManager = cacheManager;
		}

        #endregion

        #region Cities

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GetCitiesByStateId(string stateId, bool addSelectCityItem)
        {
            //this action method gets called via an ajax request
            if (String.IsNullOrEmpty(stateId))
                throw new ArgumentNullException("stateId");

            string cacheKey = string.Format(ModelCacheEventConsumer.STATEPROVINCES_BY_COUNTRY_MODEL_KEY, stateId, addSelectCityItem, _workContext.WorkingLanguage.Id);
            var cacheModel = _cacheManager.Get(cacheKey, () =>
            {
                var state = _stateService.GetStateById(Convert.ToInt32(stateId));
                var cities = _cityService.GetCitiesByStateId(state != null ? state.Id : 0).ToList();
                var result = (from c in cities
                              select new { id = c.Id, name = c.Name })
                              .ToList();


                if (state == null)
                {
                    //country is not selected ("choose country" item)
                    if (addSelectCityItem)
                    {
                        result.Insert(0, new { id = 0, name = "شهر مورد نظر را انتخاب کنید" });
                    }
                    else
                    {
                        result.Insert(0, new { id = 0, name = "سایر شهر ها" });
                    }
                }
                else
                {
                    //some country is selected
                    if (result.Count == 0)
                    {
                        //country does not have states
                        result.Insert(0, new { id = 0, name = "سایر شهر ها" });
                    }
                    else
                    {
                        //country has some states
                        if (addSelectCityItem)
                        {
                            result.Insert(0, new { id = 0, name = "شهر مورد نظر را انتخاب کنید" });
                        }
                    }
                }

                return result;
            });
            
            return Json(cacheModel, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Distribution Areas

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GetDistributionAreasByRegionId(string RegionId,
            bool addSelectRegionItem)
        {
            //permission validation is not required here


            // This action method gets called via an ajax request
            if (String.IsNullOrEmpty(RegionId))
                throw new ArgumentNullException("RegionId");

            var Region = _regionService.GetRegionById(Convert.ToInt32(RegionId));
            var dAreas = Region != null ? _distributionAreaService.GetDistributionAreasByRegionId(Region.Id, true).ToList() : new List<DistributionArea>();
            var result = (from s in dAreas
                          select new { id = s.Id, name = s.Name }).ToList();

            if (Region == null)
            {
                //Region is not selected ("choose Region" item)
                if (addSelectRegionItem)
                {
                    result.Insert(0, new { id = 0, name = "منطقه توزیع را انتخاب کنید" });
                }
                else
                {
                    result.Insert(0, new { id = 0, name = "سایر" });
                }
            }
            else
            {
                //some Region is selected
                if (result.Count == 0)
                {
                    //Region does not have states
                    result.Insert(0, new { id = 0, name = "سایر" });
                }
                else
                {
                    //Region has some states
                    if (addSelectRegionItem)
                    {
                        result.Insert(0, new { id = 0, name = "منطقه توزیع را انتخاب کنید" });
                    }
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
