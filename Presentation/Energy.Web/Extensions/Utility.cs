﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Globalization;
using Energy.Web;

namespace Energy.Web.Tools
{
    public static class Utility
    {

        public static Int32 CurrentUserID()
        {
            var s = HttpContext.Current.Session["UserID"];

            if (s.IsNull() || (string) s == "") return 0;

            return Convert.ToInt32(s);
        }


        public static String PersianDateN(this DateTime? dt, string chr)
        {
            string ret = "";
            if (dt != null)
            {
                var t = (DateTime) dt;
                var pd = new PersianCalendar();
                int n = pd.GetYear(t);
                n = n * 100 + pd.GetMonth(t);
                n = n * 100 + pd.GetDayOfMonth(t);
                ret = n.ToString().Trim();
                ret = ret.Substring(0, ret.Length - 4) + chr +
                      ret.Substring(ret.Length - 4, 2) + chr + 
                      ret.Substring(ret.Length - 2);
            };
            return ret;
        }


        public static String PersianDate(this DateTime dt, string chr)
        {
            string ret = "";
            var t = (DateTime)dt;
            var pd = new PersianCalendar();
            int n = pd.GetYear(t);
            n = n * 100 + pd.GetMonth(t);
            n = n * 100 + pd.GetDayOfMonth(t);
            ret = n.ToString().Trim();
            ret = ret.Substring(0, ret.Length - 4) + chr +
                  ret.Substring(ret.Length - 4, 2) + chr +
                  ret.Substring(ret.Length - 2);
            return ret;
        }


        public static string Right(string s, int n)
        {
            return s.Substring(s.Length - n);
        }



        public static  int SendEmail(string emailAddr, string mstg)
        {
            if (!emailAddr.IsEmail()) return 0;

            var objetoMail = new MailMessage();
            var client = new SmtpClient
                {
                    Port = 25,
//                    Host = "smtp.clean-energy.ir",
                    Host = "mail.clean-energy.ir",
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new System.Net.NetworkCredential("support@clean-energy.ir", "1234")
                };
            objetoMail.From = new MailAddress("support@clean-energy.ir");
            objetoMail.To.Add(new MailAddress(emailAddr));
            objetoMail.Subject = "شرکت انرژی پاک";
            //objetoMail.IsBodyHtml = true;
            objetoMail.Body = mstg;

            try
            {
               client.Send(objetoMail);
               return 1;
            }
            catch (Exception ex)
            {
                //ex.ToString());
                return -1;
            } 
        }


        public static int SendEmailHtml(string emailAddr, string htmlFile)
        {
            if (!emailAddr.IsEmail()) return 0;
            var reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Adv/adv_1.html"));
            string readFile = reader.ReadToEnd();
            string myString = readFile;

            var objetoMail = new MailMessage();
            var client = new SmtpClient
            {
                Port = 25,
                Host = "smtp.clean-energy.ir",
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new System.Net.NetworkCredential("support@clean-energy.ir", "1234")
            };
            objetoMail.From = new MailAddress("support@clean-energy.ir");
            objetoMail.To.Add(new MailAddress(emailAddr));
            objetoMail.Subject = "شرکت انرژی پاک";
            objetoMail.IsBodyHtml = true;
            objetoMail.Body = myString;


            try
            {
                client.Send(objetoMail);
                return 1;
            }
            catch (Exception ex)
            {
                //ex.ToString());
                return -1;
            }
        }

        public static String Emrooz()
        {
            return DateTime.Today.DayOfWeek.ToString();
        }


        public static Int32 ToInt(this string nstr)
        {
            return nstr == "" ? 0 : Convert.ToInt32(nstr);
        }

        public static Double ToDbl(this string nstr)
        {
            return nstr == "" ? 0 : Convert.ToDouble(nstr);
        }

        public static bool IsEmail(this string s)
        {
            int a = s.LastIndexOf(@"@", StringComparison.Ordinal);
            int d = s.LastIndexOf(@".", StringComparison.Ordinal);
            return (a > 0 && d > 0 && d > a);
        }



        public static string GetErrMsg(Int64 rc)
        {
            switch (rc)
            {
                case 11:
                    return "شماره کارت نامعتبر است";
                case 12:
                    return "موجودی کافی نیست";
                case 13:
                    return "رمز نادرست است";
                case 14:
                    return "تعداد دفعات واردکردن رمز بیش از حد مجاز است";
                case 15:
                    return "کارت نامعتبر است";
                case 16:
                    return "دفعات برداشت وجه بیش از حد مجاز است";
                case 17:
                    return "کاربر از انجام تراکنش منصرف شده است";
                case 18:
                    return "تاریخ انقضای کارت گذشته است";
                case 19:
                    return "مبلغ برداشت وجه بیش از حد مجاز است ";
                case 111:
                    return "صادرکننده کارت نامعتبر است";
                case 112:
                    return "خظای سوییچ صادرکننده کارت ";
                case 113:
                    return "پاسخی از صادرکننده کارت دریافت نشد";
                case 114:
                    return "دارنده کارت مجاز به انجام این تراکنش نیست";
                case 21:
                    return "پذیرنده نامعتبر است";
                case 23:
                    return "خطای امنیتی رخ داده";
                case 24:
                    return "اطلاعات کاربری پذیرنده نامعتبر است";
                case 25:
                    return "مبلغ نامعتبر است ";
                case 31:
                    return "پاسخ نامعتبراست";
                case 32:
                    return "فرمت اطلاعات وارد شده صحیح نمی باشد";
                case 33:
                    return "حساب نامعتبر است";
                case 34:
                    return "خطای سیستمی";
                case 35:
                    return "تاریخ نامعتبر است";
                case 41:
                    return "شماره درخواست تکراری است";
                case 42:
                    return "تراکنش فروش یافت نشد";
                case 43:
                    return "قبلا  درخواست تایید داده شده";
                case 44:
                    return "درخواست تایید یافت نشد";
                case 46:
                    return "تراکنش ستل نشده است";
                case 414:
                    return "صادرکننده قبض نامعتبراست";
                case 415:
                    return "زمان Session  به پایان رسیده است";
                case 416:
                    return "خطا در ثبت اطلاعات";
                case 418:
                    return "اشکال در تعریف اطلاعات مشتری";
                case 419:
                    return "تعداد دفعات ورود اطلاعات از حد مجاز گذشته است";
                case 421:
                    return "IP  نامعتبر";
                case 51:
                    return "تراکنش تکراری است ";
                case 54:
                    return "تراکنش مرجع موجود نیست ";
                case 55:
                    return "تراکنش نامعتبر است";
                case 61:
                    return "خطا در واریز";

                default:
                    return "خطا در انجام تراکنش " + rc;
            }
        }


    }
}
