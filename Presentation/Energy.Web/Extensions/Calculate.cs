﻿using System;
using System.Globalization;
using System.Linq;
using System.Data;
using Energy.Core.Domain.Constant;
using Energy.Core.Domain.Bills;
using Energy.Web.Models.Splits;
using Energy.Services.Splits;
using Energy.Services.Bills;
using Energy.Services.Tariffs;


namespace Web.Tools
{
    public static class Calculate
    {
        public static int CalculateBill(IBillService billService, ISplitService splitService,
            ITariffService tariffService, IBillNormalService billNormalService, Int32 billID)
        {
            var bs = billService.GetBill_SplitsByBillId(billID);
            if (!bs.Split.Active) return 2;   // Use for not active split

            try
            {
                if (bs.Split.Demand == (int)CDemand.Normal)
                {
                    CalCulateBillNormal(billService, splitService, tariffService, billNormalService, billID);
                    return 1;
                }
                else
                {
                    CalCulateBillDemand(billService, splitService, tariffService, billID);
                    return 1;
                }
            }
            catch
            {
                return 0;
            }
        }

        public static void CalCulateBillDemand(IBillService billService, ISplitService splitService,
            ITariffService tariffService, Int32 billID)
        {
            var bill = billService.getById(billID);

            var lastbill = billService.findPreviousBill(billID, bill.SplitID);

            if (lastbill == null)
            {
                return;
            }

            var split = splitService.GetSplitById(bill.SplitID);
            //join three tables together
            var tariffarea = tariffService.GetTariffBySplitId(split.Id);

            bill.TariffID = tariffarea.TariffArea.Tariff_Id;
            bill.TariffAreaID = tariffarea.TariffAreaID;

            if (bill.Type != "corrective")
            {
                bill.DateReadPre = lastbill.DateRead;
                bill.MidReadPre = lastbill.MidRead;
                bill.MaxReadPre = lastbill.MaxRead;
                bill.MinReadPre = lastbill.MinRead;
                bill.ReactReadPre = lastbill.ReactRead;
                bill.PowerReadPre = lastbill.PowerRead;
            }

            bill.MidUsed = (bill.MidRead - bill.MidReadPre) * split.Factor;
            bill.MaxUsed = (bill.MaxRead - bill.MaxReadPre) * split.Factor;
            bill.MinUsed = (bill.MinRead - bill.MinReadPre) * split.Factor;
            bill.ReactUsed = (bill.ReactRead - bill.ReactReadPre) * split.Factor;
            bill.PowerUsed = bill.PowerRead * split.Factor;

            bill.AllUsed = bill.MidUsed + bill.MaxUsed + bill.MinUsed; // +bill.ReactUsed;

            bill.AllDays = (bill.DateRead - bill.DateReadPre).Days;

            if (bill.AllDays <= 0) return;

            bill.MidRate = tariffarea.TariffArea.Tariff.MidCost;
            bill.MaxRate = tariffarea.TariffArea.Tariff.MaxCost;
            bill.MinRate = tariffarea.TariffArea.Tariff.MinCost;

            bill.MidCost = bill.MidRate * bill.MidUsed;
            bill.MaxCost = bill.MaxRate * bill.MaxUsed;
            bill.MinCost = bill.MinRate * bill.MinUsed;
            //- بهای مصرف اکتیو  -//
            bill.AllCost = (long)(bill.MidCost + bill.MaxCost + bill.MinCost);
            var sumCost = bill.AllCost;
            //- بهای مصرف قدرت  -//
            bill.PowerCost = tariffarea.TariffArea.Tariff.PowerCost;
            bill.Power90 = ((bill.PowerUsed < split.Power * 0.9 ? split.Power * 0.9 : bill.PowerUsed));
            bill.Power90Cost = (long)((bill.Power90 * bill.AllDays / 30) * bill.PowerCost);
            sumCost = sumCost + bill.Power90Cost;

            //- تخفیف صنعتی -//
            bill.IndustDisCost = 0;
            if (split.CTariffId == (int)CTariff.Industry)
            {
                var sa = splitService.getSplitAreaBySplitId(split.Id);
                if (sa != null)
                {
                    var ta = tariffService.getTariffAreaById(sa.TariffAreaID);
                    if (ta != null)
                    {
                        bill.IndustDisCost = (long)(ta.Factor * sumCost / 100);
                    }
                }
            }
            sumCost = sumCost - bill.IndustDisCost;
            //- انشعاب آزاد -//
            bill.FreelanceCost = 0;
            if (split.IsFreelance)
            {
                bill.FreelanceCost = (long)(0.2 * sumCost);
            }
            sumCost = sumCost + bill.FreelanceCost;
            //- جریمه تجاوز از قدرت  -//
            bill.ExceedCost = 0;
            var Exceed = (bill.PowerUsed > split.Power ? bill.PowerUsed - split.Power : 0);
            if (Exceed > 0)
            {
                if (split.CTariffId == (int)CTariff.Industry)
                {
                    double ea = Exceed / (bill.PowerRead * split.Factor);
                    if (ea <= 0.1)
                    {
                        bill.ExceedCost = (long)(ea * sumCost / 2);
                    }
                    else   // ea> 0.1
                    {
                        bill.ExceedCost = (long)(ea * sumCost - sumCost / 20);
                    }
                }
                else
                {
                    double ea = Exceed / (bill.PowerRead * split.Factor);
                    bill.ExceedCost = (long)(ea * bill.AllCost) +
                                       (long)(Exceed * (tariffarea.TariffArea.Tariff.PowerCost * bill.AllDays / 30)) +
                                       (long)(ea * bill.FreelanceCost);
                }
            }

            sumCost = sumCost + bill.ExceedCost;

            //- مصارف غیرصنعتی -//
            bill.UnIndustCost = 0;

            bill.TagExpireCost = 0;
            // صنعتی- انقضای پروانه -//
            if ((split.CTariffId == (int)CTariff.Industry) && (split.PowerTagDate != null))
            {
                var dd = Math.Min(bill.AllDays, (bill.DateRead - (DateTime)split.PowerTagDate).Days - 1);
                if (dd > 0)
                {
                    bill.TagExpireCost = (long)(0.2 * sumCost * dd / 30);
                }
                else
                {
                    bill.TagExpireCost = 0;
                }
            }
            sumCost = sumCost + bill.TagExpireCost;

            //- بهای مصرف راکتیو  -//
            bill.ReactCost = 0; bill.LossFactor = 0; bill.PowerFactor = 1;
            if (bill.ReactUsed > 0)
            {
                bill.PowerFactor = (bill.AllUsed) / Math.Sqrt(Math.Pow(bill.AllUsed, 2) + Math.Pow(bill.ReactUsed, 2));
                bill.LossFactor = (bill.PowerFactor < 0.9 ? (0.9 / bill.PowerFactor) - 1 : 0);
                var rec = (bill.LossFactor / bill.ReactUsed) * sumCost;
                if (bill.PowerUsed < split.Power * 0.9)
                {
                    bill.ReactRate = Math.Min(rec, 595);
                }
                else
                {
                    bill.ReactRate = rec;
                }
                bill.ReactCost = (long)(bill.ReactUsed * bill.ReactRate);
                bill.AllCost = (long)(bill.AllCost + bill.ReactCost);
            }
            sumCost = (long)(sumCost + bill.ReactCost);

            //- بهای فصل  -//
            bill.WarmDays = GetSummerDays(bill.DateReadPre, bill.DateRead);
            bill.WarmCost = (sumCost / bill.AllDays) * bill.WarmDays * 0.2;
            sumCost = (long)(sumCost + bill.WarmCost);

            //- آبونمان - //
            var subscription = (long)((bill.AllDays / 30) * 90000);
            sumCost = sumCost + subscription;

            //- جمع   -//
            bill.Total = sumCost;

            sumCost = sumCost - bill.Discount;

            //-   عوارض  -//
            //var tc = new AppDbContext().Defaults.FirstOrDefault();
            bill.Toll = (long)(bill.AllUsed * 30);

            // - هزینه نگهداری شبکه - //
            if (split.NetworkCost)
                bill.NetworkCost = (long)((bill.AllCost + bill.Power90Cost + subscription + bill.WarmCost + bill.FreelanceCost + bill.ExceedCost) * 0.1);
            else
                bill.NetworkCost = 0;
            bill.Total = bill.Total + bill.NetworkCost;
            sumCost = sumCost + bill.NetworkCost;

            //- مالیات -//
            bill.Tax = 0;
            if (!bill.Split.ActivityPermission)
                bill.Tax = (long)(sumCost * 0.09);
            //چک کردن تاریخ مجوز فعالیت
            else if (bill.Split.TempFromDate.HasValue && bill.Split.TempToDate.HasValue)
            {
                if (DateTime.Now < bill.Split.TempFromDate.Value || DateTime.Now > bill.Split.TempToDate.Value)
                    bill.Tax = (long)(sumCost * 0.09);
            }
            else if (split.PowerTagDate != null)
            {
                var dd = Math.Min(bill.AllDays, (bill.DateRead - (DateTime)split.PowerTagDate).Days - 1);
                if (dd > 0)
                {
                    bill.Tax = (long)(sumCost * dd / 30 * 0.09);
                }
                else
                {
                    bill.TagExpireCost = 0;
                }
            }

            //- تاریخ صدور -//
            if (bill.issueDate == null)
                bill.issueDate = DateTime.Now;

            //- مهلت پرداخت  -//
            if (bill.ExpDate == null)
                bill.ExpDate = DateTime.Now.AddDays(15);

            //- جریمه دیرکرد -//
            if (bill.Type == "corrective")
                bill.LatePenalty = 0;
            else
                GetLatePenalty(bill, lastbill);

            if (!bill.Split.IncludeLateFees)
                bill.LatePenalty = 0;

            sumCost = sumCost + (long)bill.LatePenalty;
            //End Edit
            //- جمع کل  -//
            sumCost = sumCost + bill.Tax + bill.Toll;

            // - شناسه پرداخت بدون بدهی - //
            bill.PaymentIdWD = IDGenerator(bill.SplitID, (sumCost - (sumCost % 1000)));

            //-بدهی از قبل  -//
            sumCost = sumCost + bill.Debit; //lastbill.Round + lastbill.Payable - lastbill.Paid;
            //- کسر هزار ریال  -//
            bill.Round = sumCost % 1000;
            //- قابل پرداخت  -//
            bill.Payable = sumCost - bill.Round;

            //- شناسه پرداخت  -//
            bill.PaymentID = IDGenerator(bill.Split.Id, bill.Payable);

            bill.EditDate = DateTime.Now;

            bill.Title = tariffarea.TariffArea.Tariff.Title;

            billService.UpdateBill(bill);

        }

        public static void CalCulateBillNormal(IBillService billService, ISplitService splitService,
            ITariffService tariffService, IBillNormalService billNormalservice, Int32 billID)
        {
            var bill = billService.getById(billID);

            var lastbill = billService.findPreviousBill(billID, bill.SplitID);

            if (lastbill == null)
            {
                return;
            }

            ResetBillNormal(billID, billNormalservice);

            var split = splitService.GetSplitById(bill.SplitID);
            var normSplit = splitService.getSplitAreaBySplitId(split.Id);

            var tariffarea = tariffService.getNormalTariffBySplitId(split.Id);

            var tra = tariffService.GetTariffBySplitId(split.Id);


            //calculate normal and gheire peleie bills!
            if (tra.TariffArea.Tariff_Id.IsIn(10, 12, 14, 16, 18, 19, 22, 26))
            {
                bill.TariffID = tra.TariffArea.Tariff_Id;
                bill.TariffAreaID = tra.TariffAreaID;


                if (bill.Type != "corrective")
                {
                    bill.DateReadPre = lastbill.DateRead;
                    bill.MidReadPre = lastbill.MidRead;
                    bill.MaxReadPre = lastbill.MaxRead;
                    bill.MinReadPre = lastbill.MinRead;
                    bill.ReactReadPre = lastbill.ReactRead;
                    bill.PowerReadPre = lastbill.PowerRead;
                }


                bill.MidUsed = (bill.MidRead - bill.MidReadPre) * split.Factor;
                bill.MaxUsed = (bill.MaxRead - bill.MaxReadPre) * split.Factor;
                bill.MinUsed = (bill.MinRead - bill.MinReadPre) * split.Factor;
                bill.ReactUsed = (bill.ReactRead - bill.ReactReadPre) * split.Factor;
                bill.PowerUsed = bill.PowerRead * split.Factor;

                bill.AllUsed = bill.MidUsed + bill.MaxUsed + bill.MinUsed; // +bill.ReactUsed;

                bill.AllDays = (bill.DateRead - bill.DateReadPre).Days;

                if (bill.AllDays <= 0) return;

                bill.MidRate = tra.TariffArea.Tariff.MidCost;
                bill.MaxRate = tra.TariffArea.Tariff.MaxCost;
                bill.MinRate = tra.TariffArea.Tariff.MinCost;

                bill.MidCost = bill.MidRate * bill.MidUsed;
                bill.MaxCost = bill.MaxRate * bill.MaxUsed;
                bill.MinCost = bill.MinRate * bill.MinUsed;
                //- بهای مصرف اکتیو  -//
                bill.AllCost = (long)(bill.MidCost + bill.MaxCost + bill.MinCost);
                var sumCost = bill.AllCost;
                //- بهای مصرف قدرت  -//
                bill.PowerCost = tra.TariffArea.Tariff.PowerCost;
                bill.Power90 = ((bill.PowerUsed < split.Power * 0.9 ? split.Power * 0.9 : bill.PowerUsed));
                bill.Power90Cost = (long)((bill.Power90 * bill.AllDays / 30) * bill.PowerCost);
                sumCost = sumCost + bill.Power90Cost;

                //- تخفیف صنعتی -//
                bill.IndustDisCost = 0;
                if (split.CTariffId == (int)CTariff.Industry)
                {
                    var sa = splitService.getSplitAreaBySplitId(split.Id);
                    if (sa != null)
                    {
                        var ta = tariffService.getTariffAreaById(sa.TariffAreaID);
                        if (ta != null)
                        {
                            bill.IndustDisCost = (long)(ta.Factor * sumCost / 100);
                        }
                    }
                }
                sumCost = sumCost - bill.IndustDisCost;
                //- انشعاب آزاد -//
                bill.FreelanceCost = 0;
                if (split.IsFreelance)
                {
                    bill.FreelanceCost = (long)(0.2 * sumCost);
                }
                sumCost = sumCost + bill.FreelanceCost;

                //- مصارف غیرصنعتی -//
                bill.UnIndustCost = 0;

                // صنعتی- انقضای پروانه -//
                if ((split.CTariffId == (int)CTariff.Industry) && (split.PowerTagDate != null))
                {
                    var dd = Math.Min(bill.AllDays, (bill.DateRead - (DateTime)split.PowerTagDate).Days - 1);
                    if (dd > 0)
                    {
                        bill.TagExpireCost = (long)(0.2 * sumCost * dd / 30);
                    }
                    else
                    {
                        bill.TagExpireCost = 0;
                    }
                }
                sumCost = sumCost + bill.TagExpireCost;

                //- بهای فصل  -//

                bill.WarmDays = GetSummerDays(bill.DateReadPre, bill.DateRead);
                bill.WarmCost = (sumCost / bill.AllDays) * bill.WarmDays * 0.2;
                sumCost = (long)(sumCost + bill.WarmCost);

                //- آبونمان - //
                var subscription = (long)((bill.AllDays / 30) * 10000);
                sumCost = sumCost + subscription;

                //- جمع   -//
                bill.Total = sumCost;

                //-   عوارض  -//
                //var tc = new AppDbContext().Defaults.FirstOrDefault();
                bill.Toll = (long)((bill.MidUsed + bill.MaxUsed + bill.MinUsed) * 30);

                // - هزینه نگهداری شبکه - //
                if (split.NetworkCost)
                    bill.NetworkCost = (long)((bill.AllCost + bill.Power90Cost + subscription + bill.WarmCost + bill.FreelanceCost + bill.ExceedCost) * 0.1);
                else
                    bill.NetworkCost = 0;
                bill.Total = bill.Total + bill.NetworkCost;
                sumCost = sumCost + bill.NetworkCost;

                //- مالیات -//
                bill.Tax = 0;
                if (!bill.Split.ActivityPermission)
                    bill.Tax = (long)(sumCost * 0.09);
                //چک کردن تاریخ مجوز فعالیت
                else if (bill.Split.TempFromDate.HasValue && bill.Split.TempToDate.HasValue)
                {
                    if (DateTime.Now < bill.Split.TempFromDate.Value || DateTime.Now > bill.Split.TempToDate.Value)
                        bill.Tax = (long)(sumCost * 0.09);
                }
                else if (split.PowerTagDate != null)
                {
                    var dd = Math.Min(bill.AllDays, (bill.DateRead - (DateTime)split.PowerTagDate).Days - 1);
                    if (dd > 0)
                    {
                        bill.Tax = (long)(sumCost * dd / 30 * 0.09);
                    }
                    else
                    {
                        bill.TagExpireCost = 0;
                    }
                }

                //- تاریخ صدور -//
                if (bill.issueDate == null)
                    bill.issueDate = DateTime.Now;

                //- مهلت پرداخت  -//
                if (bill.ExpDate == null)
                    bill.ExpDate = DateTime.Now.AddDays(15);

                //- جریمه دیرکرد -//
                if (bill.Type == "corrective")
                    bill.LatePenalty = 0;
                else
                    GetLatePenalty(bill, lastbill);

                if (!bill.Split.IncludeLateFees)
                    bill.LatePenalty = 0;

                sumCost = sumCost + (long)bill.LatePenalty;

                //- جمع کل  -//
                sumCost = sumCost + bill.Tax + bill.Toll;

                // - شناسه پرداخت بدون بدهی - //
                bill.PaymentIdWD = IDGenerator(bill.SplitID, (sumCost - (sumCost % 1000)));

                //-بدهی از قبل  -//
                sumCost = sumCost + bill.Debit; //lastbill.Round + lastbill.Payable - lastbill.Paid;
                //- کسر هزار ریال  -//
                bill.Round = sumCost % 1000;
                //- قابل پرداخت  -//
                bill.Payable = sumCost - bill.Round;

                //- شناسه پرداخت  -//
                bill.PaymentID = IDGenerator(bill.Split.Id, bill.Payable);

                bill.EditDate = DateTime.Now;

                billService.UpdateBill(bill);
            }
            //calculate normal and peleie bills!
            else if (tra.TariffArea.Tariff_Id.IsIn(1, 2, 3, 4, 5, 7, 8))
            {
                bill.TariffID = tra.TariffArea.Tariff_Id;
                bill.TariffAreaID = tra.TariffAreaID;

                if (bill.Type != "corrective")
                {
                    bill.DateReadPre = lastbill.DateRead;
                    bill.MidReadPre = lastbill.MidRead;
                    bill.MaxReadPre = lastbill.MaxRead;
                    bill.MinReadPre = lastbill.MinRead;
                    bill.ReactReadPre = lastbill.ReactRead;
                    bill.PowerReadPre = lastbill.PowerRead;
                }

                bill.MidUsed = (bill.MidRead - bill.MidReadPre) * split.Factor;
                bill.MaxUsed = (bill.MaxRead - bill.MaxReadPre) * split.Factor;
                bill.MinUsed = (bill.MinRead - bill.MinReadPre) * split.Factor;
                bill.ReactUsed = (bill.ReactRead - bill.ReactReadPre) * split.Factor;

                bill.AllUsed = bill.MidUsed + bill.MaxUsed + bill.MinUsed; // +bill.ReactUsed;

                bill.AllDays = (bill.DateRead - bill.DateReadPre).Days;

                if (bill.AllDays <= 0) return;

                bill.NormalDays = 0;
                bill.WarmDays = 0;
                bill.WarmDaysX = 0;
                bill.AllDays = 0;
                bill.AllDaysX = 0;

                if (tariffarea.Any())
                {
                    tariffarea.ForEach(a =>
                    {
                        a.WarmDays = 0;
                        a.WarmDaysFactor = 0;
                        a.WarmDaysX = 0;
                    });
                }

                var pd = new PersianCalendar();
                for (DateTime aDay = bill.DateReadPre; aDay < bill.DateRead; aDay = aDay.AddDays(1))
                {
                    int yy = pd.GetYear(aDay);
                    int iday = yy * 10000 + pd.GetMonth(aDay) * 100 + pd.GetDayOfMonth(aDay);
                    bool norm = true;
                    if (tariffarea.Any())
                    {
                        tariffarea.ForEach(a =>
                        {
                            int fromdate = yy * 10000 + a.TariffArea.FromMonth * 100 + a.TariffArea.FromDay;
                            int todate = yy * 10000 + a.TariffArea.ToMonth * 100 + a.TariffArea.ToDay;
                            if (iday >= fromdate && iday <= todate)
                            {
                                norm = false;
                                a.WarmDays = a.WarmDays + 1;
                                a.WarmDaysFactor = a.TariffArea.Factor;
                                a.WarmDaysX = a.WarmDaysX + a.WarmDaysFactor;
                                bill.AllDays = bill.AllDays + 1;
                                bill.AllDaysX = bill.AllDaysX + a.WarmDaysFactor;
                            }
                        }); //- foreach -//
                    }
                    if (norm)
                    {
                        bill.NormalDays = bill.NormalDays + 1;
                        bill.AllDays = bill.AllDays + 1;
                        bill.AllDaysX = bill.AllDaysX + 1;
                    }
                }  //-End Days loop -//


                //- Initialize SplitArea other fields and Sum rows tempruary for this bill. -//
                double sWarmDays = 0;
                double sWarmDaysX = 0;
                double sWarmUsed = 0;
                double sWarmUsedAvrg = 0;
                double sWarmCostAvrg = 0;
                double sWarmCost = 0;
                double cFine = 0;
                double cDisct = 0;
                double cFactor = 0;

                if (tariffarea.Any())
                {
                    tariffarea.ForEach(a =>
                    {
                        a.WarmUsed = (bill.AllDaysX > 0 ? (bill.AllUsed * a.WarmDaysX / bill.AllDaysX) : 0);
                        a.WarmUsedAvrg = (a.WarmDays > 0 ? (a.WarmUsed * 30 / a.WarmDays) : 0);
                        a.WarmCostAvrg = GetTariffCost(tariffService, billNormalservice, a.WarmUsedAvrg, a.TariffArea.Tariff_Id, bill.Id, a.TariffArea.Factor);
                        a.WarmCost = a.WarmCostAvrg * a.WarmDays / 30;

                        sWarmDays = sWarmDays + a.WarmDays;
                        sWarmDaysX = sWarmDaysX + a.WarmDaysX;
                        sWarmUsed = sWarmUsed + a.WarmUsed;
                        sWarmUsedAvrg = sWarmUsedAvrg + a.WarmUsedAvrg;
                        sWarmCostAvrg = (sWarmCostAvrg + a.WarmCostAvrg);
                        sWarmCost = sWarmCost + a.WarmCost;
                    }); //- foreach -//
                }

                if (split.Timer != (int)CTimer.Singletime)
                {
                    cFactor = tra.TariffArea.Tariff.Factor;
                    cFine = tra.TariffArea.Tariff.MaxFine;
                    if (split.Timer == (int)CTimer.Tripletime)
                    {
                        cDisct = tra.TariffArea.Tariff.MinDiscount;
                    }
                    else
                    {
                        cDisct = tra.TariffArea.Tariff.NoMaxDiscount;
                    }
                }

                //-  Bill calculates -//

                bill.WarmDays = sWarmDays;
                bill.WarmDaysX = sWarmDaysX;
                bill.WarmUsed = sWarmUsed;
                bill.WarmUsedAvrg = sWarmUsedAvrg;
                bill.WarmCostAvrg = sWarmCostAvrg;
                bill.WarmCost = sWarmCost;

                bill.NormalUsed = (bill.AllDaysX > 0 ? bill.AllUsed * bill.NormalDays / bill.AllDaysX : 0);
                bill.NormalUsedAvrg = (bill.NormalDays > 0 ? bill.NormalUsed * 30 / bill.NormalDays : 0);
                var tid = (int)bill.Split.CTariffId;

                switch (tid)
                {
                    case 1:
                        tid = 1;
                        break;
                    case 5:
                        tid = 7;
                        break;
                    default:
                        break;
                }

                if (normSplit != null)
                    bill.NormalCostAvrg = GetTariffCost(tariffService, billNormalservice, bill.NormalUsedAvrg, tid, bill.Id, 1);  //-Tariff first row ID = Normal = 1 -//

                bill.NormalCost = (bill.NormalCostAvrg * bill.NormalDays / 30);

                bill.AllCost = (Int32)(bill.NormalCost + bill.WarmCost);

                //- جریمه مصرف اوج بار - تخفیف مصرف کم باری -//
                //- Initialize MaxUsed Fine and MinUsed Discount -//
                bill.WarmMaxUsed = bill.MaxUsed * bill.WarmDaysX / bill.AllDaysX;
                bill.WarmMaxFineCost = bill.WarmMaxUsed * cFine * cFactor;
                bill.NormalMaxUsed = bill.MaxUsed - bill.WarmMaxUsed;
                bill.NormalMaxFineCost = bill.NormalMaxUsed * cFine * 1;
                bill.FineCost = (long)(bill.WarmMaxFineCost + bill.NormalMaxFineCost);

                bill.WarmMindUsed = bill.MinUsed * bill.WarmDaysX / bill.AllDaysX;
                bill.WarmMindDiscountCost = bill.WarmMindUsed * cDisct * cFactor;
                bill.NormalMindUsed = bill.MinUsed - bill.WarmMindUsed;
                bill.NormalMindDiscountCost = bill.NormalMindUsed * cDisct * 1;
                bill.DiscountCost = (long)(bill.WarmMindDiscountCost + bill.NormalMindDiscountCost);

                //- Initialize Summery fields of Bill -//
                //- تاریخ صدور -//
                if (bill.issueDate == null)
                    bill.issueDate = DateTime.Now;

                //- مهلت پرداخت  -//
                if (bill.ExpDate == null)
                    bill.ExpDate = DateTime.Now.AddDays(15);

                var sumCost = bill.AllCost;
                //- جریمه و تخفیف مصرف -//
                sumCost = sumCost + bill.FineCost - bill.DiscountCost;
                //- انشعاب آزاد -//
                bill.FreelanceCost = 0;
                if (split.IsFreelance)
                {
                    bill.FreelanceCost = (long)(0.2 * sumCost);
                }
                sumCost = sumCost + bill.FreelanceCost;
                //- بهای فصل  -//
                if (bill.Split.CTariffId != (int)CTariff.Home)
                {
                    bill.WarmDays = GetSummerDays(bill.DateReadPre, bill.DateRead);
                    bill.WarmCost = (sumCost / bill.AllDays) * bill.WarmDays * 0.2;
                    sumCost = (long)(sumCost + bill.WarmCost);
                }

                //- آبونمان - //
                var subscription = (long)((bill.AllDays / 30) * 10000);
                sumCost = sumCost + subscription;

                //- جمع   -//
                bill.Total = sumCost;

                //-   عوارض  -//
                //var tc = new AppDbContext().Defaults.FirstOrDefault();
                bill.Toll = (long)((bill.MidUsed + bill.MaxUsed + bill.MinUsed) * 30);

                // - هزینه نگهداری شبکه - //
                if (split.NetworkCost)
                    bill.NetworkCost = (long)((bill.AllCost + bill.Power90Cost + subscription + bill.WarmCost + bill.FreelanceCost + bill.ExceedCost) * 0.1);
                else
                    bill.NetworkCost = 0;
                bill.Total = bill.Total + bill.NetworkCost;
                sumCost = sumCost + bill.NetworkCost;

                //- مالیات -//
                bill.Tax = 0;
                if (!bill.Split.ActivityPermission)
                    bill.Tax = (long)(sumCost * 0.09);
                //چک کردن تاریخ مجوز فعالیت
                else if (bill.Split.TempFromDate.HasValue && bill.Split.TempToDate.HasValue)
                {
                    if (DateTime.Now < bill.Split.TempFromDate.Value && DateTime.Now > bill.Split.TempToDate.Value)
                        bill.Tax = (long)(sumCost * 0.09);
                }

                //- جریمه دیرکرد -//
                if (bill.Type != "corrective")
                    GetLatePenalty(bill, lastbill);
                else
                    bill.LatePenalty = 0;

                if (!bill.Split.IncludeLateFees)
                    bill.LatePenalty = 0;

                sumCost = sumCost + (long)bill.LatePenalty;

                //- جمع کل  -//
                sumCost = sumCost + bill.Tax + bill.Toll;

                // - شناسه پرداخت بدون بدهی - //
                bill.PaymentIdWD = IDGenerator(bill.SplitID, (sumCost - (sumCost % 1000)));

                //- بدهی از قبل  -//
                sumCost = sumCost + bill.Debit; //lastbill.Round + lastbill.Payable - lastbill.Paid;
                //- کسر هزار ریال  -//
                bill.Round = sumCost % 1000;
                //- قابل پرداخت  -//
                bill.Payable = sumCost - bill.Round;

                //- شناسه پرداخت  -//
                bill.PaymentID = IDGenerator(bill.Split.Id, bill.Payable);

                bill.EditDate = DateTime.Now;

                billService.UpdateBill(bill);
            }

        }

        //---- Used Cost function ----//
        public static double GetTariffCost(
            ITariffService tariffService,
            IBillNormalService billNormalService,
            double used, int tID, int billID, double fact)
        {
            var tariff = tariffService.GetById(tID);
            var tariffrate = tariffService.getTariffRate(tID, used);

            double s = 0;
            bool isLast = false;
            tariffrate.ForEach(tr =>
                {
                    var billn = new BillNormal
                    {
                        BillID = billID,
                        TariffID = tID,
                        FromKwhm = tr.FromKwhm,
                        ToKwhm = tr.ToKwhm,
                        Cost = tr.Cost,
                        Description = fact.ToString() + " - " + tr.Description,
                    };
                    if (!isLast)
                    {
                        if (used > tr.ToKwhm)
                        {
                            s = s + (tr.ToKwhm - tr.FromKwhm + 1) * tr.Cost;
                            billn.Used = (tr.ToKwhm - tr.FromKwhm + 1);
                            billn.UsedCost = billn.Used * billn.Cost;
                        }
                        else
                        {
                            s = (int)(s + (used - tr.FromKwhm + 1) * tr.Cost);
                            billn.Used = (used - tr.FromKwhm + 1);
                            billn.UsedCost = billn.Used * billn.Cost;
                            isLast = true;
                        }
                    }
                    else
                    {
                        billn.Used = 0;
                        billn.UsedCost = 0;
                    }

                    billNormalService.InsertBilln(billn);

                });


            if (tariff.LimitCostKwh > 0)
            {
                if (s / used > tariff.LimitCostKwh)
                {
                    s = used * tariff.LimitCostKwh;
                }
            }
            return s;
        }

        public static void ResetBillNormal(int bid, IBillNormalService billNormalservice)
        {
            var billnorm = billNormalservice.GetByBillId(bid);
            if (!billnorm.Any()) return;
            foreach (var bn in billnorm)
            {
                billNormalservice.DeleteBillNormal(bn);
            }
        }
        //---- Summer days function ------//
        public static int GetSummerDays(DateTime fromDt, DateTime toDt)
        {
            if (fromDt.Year == toDt.Year)
            {
                var y = fromDt.Year;
                int i = 0;
                DateTime summerBegDate = Convert.ToDateTime("21-Jun-" + y);
                DateTime summerEndDate = Convert.ToDateTime("21-Sep-" + y);
                for (DateTime aDay = fromDt; aDay < toDt; aDay = aDay.AddDays(1))
                {
                    if (aDay >= summerBegDate && aDay <= summerEndDate)
                    {
                        i = i + 1;
                    }
                }
                return i;
            }
            else
            {
                int i = 0;
                var fy = fromDt.Year;
                DateTime summerBegDate = Convert.ToDateTime("21-Jun-" + fy);
                DateTime summerEndDate = Convert.ToDateTime("21-Sep-" + fy);
                for (DateTime aDay = fromDt; aDay < toDt; aDay = aDay.AddDays(1))
                {
                    if (aDay >= summerBegDate && aDay <= summerEndDate)
                    {
                        i = i + 1;
                    }
                }

                var ty = toDt.Year;
                summerBegDate = Convert.ToDateTime("21-Jun-" + ty);
                summerEndDate = Convert.ToDateTime("21-Sep-" + ty);
                for (DateTime aDay = fromDt; aDay < toDt; aDay = aDay.AddDays(1))
                {
                    if (aDay >= summerBegDate && aDay <= summerEndDate)
                    {
                        i = i + 1;
                    }
                }

                return i;
            }
        }

        public static string GetTariff(string txt)
        {
            string ret;
            switch (txt)
            {
                case "1":
                    ret = "مصارف خانگی";
                    break;
                case "2":
                    ret = "مصارف عمومی";
                    break;
                case "3":
                    ret = "مصارف کشاورزی";
                    break;
                case "4":
                    ret = "مصارف صنعتی";
                    break;
                case "5":
                    ret = "سایر مصارف";
                    break;
                default:
                    ret = "";
                    break;
            }
            return ret;
        }

        public static string GetVolt(string txt)
        {
            string ret;
            switch (txt)
            {
                case "V220":
                    ret = "220V ثانویه ";
                    break;
                case "V400":
                    ret = "400V ثانویه ";
                    break;
                case "V11Kp":
                    ret = "11KV اولیه ";
                    break;
                case "V11Ks":
                    ret = "11KV ثانویه ";
                    break;
                case "V20Kp":
                    ret = "20KV اولیه ";
                    break;
                case "V20Ks":
                    ret = "20KV ثانویه ";
                    break;
                default:
                    ret = "";
                    break;
            };
            return ret;
        }

        public static string IDGenerator(long sid, long pay)
        {
            if (sid <= 0)
            {
                return "ID Error";
            }
            if (pay <= 0)
            {
                return "Amount Error";
            }
            string stid = sid.ToString().Trim();
            stid = stid.PadLeft(10, '0');
            var A = 53 * 2 +
                        47 * Convert.ToInt32(stid.Substring(9, 1)) +
                        43 * Convert.ToInt32(stid.Substring(8, 1)) +
                        41 * Convert.ToInt32(stid.Substring(7, 1)) +
                        37 * Convert.ToInt32(stid.Substring(6, 1)) +
                        31 * Convert.ToInt32(stid.Substring(5, 1)) +
                        29 * Convert.ToInt32(stid.Substring(4, 1)) +
                        23 * Convert.ToInt32(stid.Substring(3, 1)) +
                        19 * Convert.ToInt32(stid.Substring(2, 1)) +
                        17 * Convert.ToInt32(stid.Substring(1, 1)) +
                        13 * Convert.ToInt32(stid.Substring(0, 1));

            stid = sid.ToString().Trim();

            //const string bkno = "4299775653";
            const int B = 3 * 37 + 5 * 31 + 6 * 29 + 5 * 23 + 7 * 19 + 7 * 17 + 9 * 13 + 9 * 11 + 2 * 7 + 4 * 5;

            var sPay = pay.ToString().Trim().PadLeft(15, '0');
            int[] pr = { 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61 };
            int C = 0;
            for (int i = 14; i >= 0; i = i - 1)
            {
                C = C + pr[i] * Convert.ToInt32(sPay.Substring(i, 1));
            }

            return stid + "2" + ((A + B + C) % 99).ToString().Trim().PadLeft(2, '0');

        }

        public static void GetLatePenalty(Bill curBill, Bill preBill)
        {
            if (preBill.Type == "corrective")
            {
                curBill.LatePenalty = 0;
                return;
            }
            //- جریمه دیرکرد -//
            //- مشترک بدهی پیشین ندارد - //
            if (preBill.Debit < 1000)
            {
                // - مشترک پرداختی و بدهی پیشین ندارد - //
                if (curBill.LastPayment1 == 0)
                {
                    if (preBill.Payable >= 1000)
                    {
                        TimeSpan PaymentDelay = (DateTime)curBill.issueDate - (DateTime)preBill.ExpDate;
                        int delay = PaymentDelay.Days + 1;
                        float delayFactor = (float)delay / 1350;
                        curBill.LatePenalty = delayFactor * preBill.Payable;
                    }
                    else
                        curBill.LatePenalty = 0;
                }
                // - مشترک پرداختی دارد, بدهی پیشین ندارد  - //
                else
                {
                    DateTime lpd = (DateTime)curBill.LastPaymentDate1;
                    // - مشترک تاخیر در پرداخت ندارد - //
                    if (lpd.CompareTo(preBill.ExpDate) < 0)
                    {
                        // - مشترک بدهی اش را کامل تسویه کرده - //
                        if (curBill.Debit < 1000)
                        {
                            curBill.LatePenalty = 0;
                        }
                        // - بدهی مشترک کامل تسویه نشده است - //
                        else
                        {
                            TimeSpan PaymentDelay = (DateTime)curBill.issueDate - (DateTime)preBill.ExpDate;
                            int delay = PaymentDelay.Days + 1;
                            double delayFactor = (double)delay / 1350;
                            curBill.LatePenalty += (double)delayFactor * (curBill.Debit);
                        }

                    }
                    // - مشترک تاخیر در پرداخت دارد - //
                    else
                    {
                        TimeSpan PaymentDelay = (DateTime)curBill.LastPaymentDate1 - (DateTime)preBill.ExpDate;
                        int delay = PaymentDelay.Days + 1;
                        float delayFactor = (float)delay / 1350;
                        curBill.LatePenalty = delayFactor * preBill.Payable;
                    }
                }
            }
            //- مشترک بدهی پیشین دارد - //
            else
            {
                // - مشترک پرداختی ندارد , بدهی پیشین دارد - //
                if (curBill.LastPayment1 == 0)
                {
                    TimeSpan preBillPaymentDelay = (DateTime)curBill.issueDate - (DateTime)preBill.ExpDate;
                    int delay1 = preBillPaymentDelay.Days + 1;
                    float delayFactor1 = (float)delay1 / 1350;

                    TimeSpan debitPaymentDelay = (DateTime)curBill.issueDate - (DateTime)preBill.issueDate;
                    int delay2 = debitPaymentDelay.Days + 1;
                    float delayFactor2 = (float)delay2 / 1350;

                    curBill.LatePenalty = (delayFactor1 * preBill.Total) + (delayFactor2 * (preBill.Debit - preBill.LatePenalty));
                }
                // - مشترک پرداختی و بدهی پیشین دارد - //
                else
                {
                    DateTime lpd = (DateTime)curBill.LastPaymentDate1;
                    // - مشترک تاخیر در پرداخت ندارد - //
                    if (lpd.CompareTo(preBill.ExpDate) < 0)
                    {
                        // - مشترک بدهی اش را کامل تسویه کرده - //
                        if (curBill.Debit < 1000)
                        {
                            TimeSpan PaymentDelay = (DateTime)curBill.LastPaymentDate1 - (DateTime)preBill.issueDate;
                            int delay = PaymentDelay.Days + 1;
                            double delayFactor = (double)delay / 1350;
                            curBill.LatePenalty = (double)delayFactor * (preBill.Debit);
                        }
                        // - بدهی مشترک کامل تسویه نشده است - //
                        else
                        {
                            TimeSpan PaymentDelay = (DateTime)curBill.LastPaymentDate1 - (DateTime)preBill.issueDate;
                            int delay = PaymentDelay.Days + 1;
                            double delayFactor = (double)delay / 1350;
                            curBill.LatePenalty = (double)delayFactor * (preBill.Debit);

                            PaymentDelay = (DateTime)curBill.issueDate - (DateTime)preBill.ExpDate;
                            delay = PaymentDelay.Days + 1;
                            delayFactor = (double)delay / 1350;
                            curBill.LatePenalty += (double)delayFactor * (curBill.Debit);
                        }
                    }
                    //- مشترک تاخیر در پرداخت دارد - //
                    else
                    {
                        // - مشترک بدهی اش را کامل تسویه کرده - //
                        if (curBill.Debit < 1000)
                        {
                            TimeSpan firstPaymentDelay = (DateTime)curBill.LastPaymentDate1 - (DateTime)preBill.ExpDate;
                            int delay1 = firstPaymentDelay.Days + 1;
                            float delayFactor1 = (float)delay1 / 1350;

                            TimeSpan secondPaymentDelay = (DateTime)curBill.LastPaymentDate1 - (DateTime)preBill.issueDate;
                            int delay2 = secondPaymentDelay.Days + 1;
                            float delayFactor2 = (float)delay2 / 1350;

                            curBill.LatePenalty = (delayFactor1 * preBill.Total) + (delayFactor2 * (preBill.Debit - preBill.LatePenalty));
                        }
                        // - بدهی مشترک کامل تسویه نشده است - //
                        else
                        {
                            TimeSpan firstPaymentDelay = (DateTime)curBill.LastPaymentDate1 - (DateTime)preBill.ExpDate;
                            int delay1 = firstPaymentDelay.Days + 1;
                            double delayFactor1 = (double)delay1 / 1350;

                            TimeSpan secondPaymentDelay = (DateTime)curBill.LastPaymentDate1 - (DateTime)preBill.issueDate;
                            int delay2 = secondPaymentDelay.Days + 1;
                            double delayFactor2 = (double)delay2 / 1350;

                            curBill.LatePenalty = (delayFactor1 * preBill.Total) + (delayFactor2 * (preBill.Debit - preBill.LatePenalty));

                            TimeSpan PaymentDelay = (DateTime)curBill.issueDate - (DateTime)curBill.LastPaymentDate1;
                            int delay = PaymentDelay.Days + 1;
                            double delayFactor = (double)delay / 1350;
                            curBill.LatePenalty += (double)delayFactor * (curBill.Debit);
                        }
                    }
                }
            }
        }
    }
}