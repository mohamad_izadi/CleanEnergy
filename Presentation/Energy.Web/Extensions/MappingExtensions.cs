﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Energy.Core.Domain.Logging;
using Energy.Core.Domain.Common;
using Energy.Core.Domain.Customers;
using Energy.Core.Domain.Directory;
using Energy.Core.Domain.Splits;
using Energy.Core.Domain.Bills;
using Energy.Core.Domain.Requests;
using Energy.Services.Common;
using Energy.Services.Localization;
using Energy.Services.Seo;
using Energy.Web.Models.Common;
using Energy.Web.Models.Splits;
using Energy.Web.Models.Bills;
using Energy.Web.Models.CustomersAdmin;
using Energy.Web.Models.Logging;
using Energy.Web.Models.Directory;
using Energy.Web.Models.Requests;
using Energy.Core.Domain.Messages;
using Energy.Web.Models.Messages;
using Energy.Web.Models.Tariffs;
using Energy.Core.Domain.Tariffs;




namespace Energy.Web.Extensions
{
    public static class MappingExtensions
    {
        public static TDestination MapTo<TSource, TDestination>(this TSource source)
        {
            Mapper.AssertConfigurationIsValid();
            return Mapper.Map<TSource, TDestination>(source);
        }

        public static TDestination MapTo<TSource, TDestination>(this TSource source, TDestination destination)
        {
            Mapper.AssertConfigurationIsValid();
            return Mapper.Map(source, destination);
        }

        #region Splits

        //news items
        public static SplitModel ToModel(this Split entity)
        {
            return entity.MapTo<Split, SplitModel>();
        }

        public static Split ToEntity(this SplitModel model)
        {
            return model.MapTo<SplitModel, Split>();
        }

        public static Split ToEntity(this SplitModel model, Split destination)
        {
            return model.MapTo(destination);
        }

        #endregion

        #region Bills

        //news items
        public static BillModel ToModel(this Bill entity)
        {
            return entity.MapTo<Bill, BillModel>();
        }

        public static Bill ToEntity(this BillModel model)
        {
            return model.MapTo<BillModel, Bill>();
        }

        public static Bill ToEntity(this BillModel model, Bill destination)
        {
            return model.MapTo(destination);
        }

        #endregion

        #region Tariffs

        //news items
        public static TariffAreaModel ToModel(this TariffArea entity)
        {
            return entity.MapTo<TariffArea, TariffAreaModel>();
        }

        public static TariffArea ToEntity(this TariffAreaModel model)
        {
            return model.MapTo<TariffAreaModel, TariffArea>();
        }


        public static TariffArea ToEntity(this TariffAreaModel model, TariffArea destination)
        {
            return model.MapTo(destination);
        }

        #endregion

        #region Regions / distribution areas

        public static RegionModel ToModel(this Region entity)
        {
            return entity.MapTo<Region, RegionModel>();
        }

        public static Region ToEntity(this RegionModel model)
        {
            return model.MapTo<RegionModel, Region>();
        }

        public static Region ToEntity(this RegionModel model, Region destination)
        {
            return model.MapTo(destination);
        }

        public static DistributionAreaModel ToModel(this DistributionArea entity)
        {
            return entity.MapTo<DistributionArea, DistributionAreaModel>();
        }

        public static DistributionArea ToEntity(this DistributionAreaModel model)
        {
            return model.MapTo<DistributionAreaModel, DistributionArea>();
        }

        public static DistributionArea ToEntity(this DistributionAreaModel model, DistributionArea destination)
        {
            return model.MapTo(destination);
        }


        #endregion

        #region Requests

        //news items
        public static RequestModel ToModel(this Request entity)
        {
            return entity.MapTo<Request, RequestModel>();
        }

        public static Request ToEntity(this RequestModel model)
        {
            return model.MapTo<RequestModel, Request>();
        }

        public static Request ToEntity(this RequestModel model, Request destination)
        {
            return model.MapTo(destination);
        }

        #endregion

        #region Customer roles

        //customer roles
        public static CustomerRoleModel ToModel(this CustomerRole entity)
        {
            return entity.MapTo<CustomerRole, CustomerRoleModel>();
        }

        public static CustomerRole ToEntity(this CustomerRoleModel model)
        {
            return model.MapTo<CustomerRoleModel, CustomerRole>();
        }

        public static CustomerRole ToEntity(this CustomerRoleModel model, CustomerRole destination)
        {
            return model.MapTo(destination);
        }

        #endregion

        #region Log

        public static LogModel ToModel(this Log entity)
        {
            return entity.MapTo<Log, LogModel>();
        }

        public static Log ToEntity(this LogModel model)
        {
            return model.MapTo<LogModel, Log>();
        }

        public static Log ToEntity(this LogModel model, Log destination)
        {
            return model.MapTo(destination);
        }

        public static ActivityLogTypeModel ToModel(this ActivityLogType entity)
        {
            return entity.MapTo<ActivityLogType, ActivityLogTypeModel>();
        }

        public static ActivityLogModel ToModel(this ActivityLog entity)
        {
            return entity.MapTo<ActivityLog, ActivityLogModel>();
        }

        #endregion

        #region Email account

        public static EmailAccountModel ToModel(this EmailAccount entity)
        {
            return entity.MapTo<EmailAccount, EmailAccountModel>();
        }

        public static EmailAccount ToEntity(this EmailAccountModel model)
        {
            return model.MapTo<EmailAccountModel, EmailAccount>();
        }

        public static EmailAccount ToEntity(this EmailAccountModel model, EmailAccount destination)
        {
            return model.MapTo(destination);
        }

        #endregion

        #region Message templates

        public static MessageTemplateModel ToModel(this MessageTemplate entity)
        {
            return entity.MapTo<MessageTemplate, MessageTemplateModel>();
        }

        public static MessageTemplate ToEntity(this MessageTemplateModel model)
        {
            return model.MapTo<MessageTemplateModel, MessageTemplate>();
        }

        public static MessageTemplate ToEntity(this MessageTemplateModel model, MessageTemplate destination)
        {
            return model.MapTo(destination);
        }

        #endregion

        #region Queued email

        public static QueuedEmailModel ToModel(this QueuedEmail entity)
        {
            return entity.MapTo<QueuedEmail, QueuedEmailModel>();
        }

        public static QueuedEmail ToEntity(this QueuedEmailModel model)
        {
            return model.MapTo<QueuedEmailModel, QueuedEmail>();
        }

        public static QueuedEmail ToEntity(this QueuedEmailModel model, QueuedEmail destination)
        {
            return model.MapTo(destination);
        }

        #endregion
    }
}