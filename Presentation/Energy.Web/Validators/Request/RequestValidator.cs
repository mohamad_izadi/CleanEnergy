﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation;
using Energy.Web.Models.Requests;

namespace Energy.Web.Validators.Request
{
    public class RequestValidator : AbstractValidator<RequestModel>
    {
        public RequestValidator()
        {
            RuleFor(x => x.Address).NotEmpty().WithMessage(" آدرس انشعاب را وارد کنید");

            RuleFor(x => x.SplitPostalCode).NotEmpty().WithMessage("کد پستی انشعاب را وارد کنید");

            RuleFor(x => x.DistributionArea_Id).NotEqual(0).WithMessage("برق منطقه ای مربوط به انشعاب را وارد کنید");

            RuleFor(x => x.tariff_Id).NotEqual(0).WithMessage("تعرفه مورد نظر را انتخاب کنید");

            RuleFor(x => x.SplitPostalCode).Length(10, 10).WithMessage("کد پستی انشعاب را در 10 رقم وارد کنید");
        }
    }
}