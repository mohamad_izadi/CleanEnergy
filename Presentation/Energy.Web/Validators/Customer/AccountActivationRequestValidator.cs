﻿using FluentValidation;
using Energy.Services.Localization;
using Energy.Web.Models.Customer;

namespace Energy.Web.Validators.Customer
{
    public class AccountActivationRequestValidator : AbstractValidator<AccountActivationRequestModel>
    {
        public AccountActivationRequestValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Email).NotEmpty().WithMessage(localizationService.GetResource("Account.AccountActivationRequest.Email.Required"));
            RuleFor(x => x.Email).EmailAddress().WithMessage(localizationService.GetResource("Common.WrongEmail"));
        }
    }
}