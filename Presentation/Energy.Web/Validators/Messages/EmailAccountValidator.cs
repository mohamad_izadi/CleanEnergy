﻿using FluentValidation;
using Energy.Web.Models.Messages;
using Energy.Services.Localization;
using Energy.Web.Framework.Validators;

namespace Energy.Web.Validators.Messages
{
    public class EmailAccountValidator : BaseProValidator<EmailAccountModel>
    {
        public EmailAccountValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Email).NotEmpty();
            RuleFor(x => x.Email).EmailAddress().WithMessage(localizationService.GetResource("Admin.Common.WrongEmail"));
            
            RuleFor(x => x.DisplayName).NotEmpty();
        }
    }
}