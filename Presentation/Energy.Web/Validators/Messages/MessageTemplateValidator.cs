﻿using FluentValidation;
using Energy.Web.Models.Messages;
using Energy.Services.Localization;
using Energy.Web.Framework.Validators;

namespace Energy.Web.Validators.Messages
{
    public class MessageTemplateValidator : BaseProValidator<MessageTemplateModel>
    {
        public MessageTemplateValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Subject).NotEmpty().WithMessage(localizationService.GetResource("Admin.ContentManagement.MessageTemplates.Fields.Subject.Required"));
            RuleFor(x => x.Body).NotEmpty().WithMessage(localizationService.GetResource("Admin.ContentManagement.MessageTemplates.Fields.Body.Required"));
        }
    }
}