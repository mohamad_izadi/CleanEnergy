﻿using FluentValidation;
using Energy.Services.Localization;
using Energy.Web.Framework.Validators;
using Energy.Web.Models.PrivateMessages;

namespace Energy.Web.Validators.PrivateMessages
{
    public class SendPrivateMessageValidator : BaseProValidator<SendPrivateMessageModel>
    {
        public SendPrivateMessageValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Subject).NotEmpty().WithMessage(localizationService.GetResource("PrivateMessages.SubjectCannotBeEmpty"));
            RuleFor(x => x.Message).NotEmpty().WithMessage(localizationService.GetResource("PrivateMessages.MessageCannotBeEmpty"));
        }
    }
}