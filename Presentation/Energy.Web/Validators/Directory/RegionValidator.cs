﻿using FluentValidation;
using Energy.Web.Models.Directory;
using Energy.Services.Localization;
using Energy.Web.Framework.Validators;

namespace Energy.Web.Validators.Directory
{
    public class RegionValidator : BaseProValidator<RegionModel>
    {
        public RegionValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .WithMessage(localizationService.GetResource("Admin.Configuration.Countries.Fields.Name.Required"));
        }
    }
}