﻿using FluentValidation;
using Energy.Web.Models.Directory;
using Energy.Services.Localization;
using Energy.Web.Framework.Validators;

namespace Energy.Web.Validators.Directory
{
    public class DistributionAreaValidator : BaseProValidator<DistributionAreaModel>
    {
        public DistributionAreaValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Configuration.Countries.States.Fields.Name.Required"));
        }
    }
}