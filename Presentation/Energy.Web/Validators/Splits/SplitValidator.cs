﻿using Energy.Web.Models.Splits;
using FluentValidation;

namespace Energy.Web.Validators.Splits
{
    public class SplitValidator : AbstractValidator<SplitModel>
    {
        public SplitValidator() 
        {
            RuleFor(x => x.Title).NotEmpty().WithMessage("عنوان انشعاب را وارد کنید");

            RuleFor(x => x.Volt).NotEqual(0).WithMessage("لطفا ولتاژ درخواستی را وارد کنید");

            RuleFor(x => x.Phase).NotEqual(0).WithMessage("لطفا خط فاز درخواستی را وارد کنید");

            RuleFor(x => x.Demand).NotEqual(0).WithMessage("لطفا نوع مصرف را وارد کنید");

            RuleFor(x => x.Region_Id).NotEqual(0).WithMessage("لطفا منطقه توزیع را وارد کنید");

            RuleFor(x => x.DistributionArea_Id).NotEqual(0).WithMessage("لطفا برق منطقه ای را وارد کنید");

            RuleFor(x => x.Postal).NotEmpty().WithMessage("لطفا کد پستی را وارد کنید");

            RuleFor(x => x.SerialNo).NotEmpty().WithMessage("لطفا شماره سریال کنتور را وارد کنید");

            RuleFor(x => x.InstallDate).NotEmpty().WithMessage("لطفا تاریخ نصب کنتور را وارد کنید");

            RuleFor(x => x.TempFromDate).NotEmpty().WithMessage("لطفا تاریخ شروع مجوز فعالیت را وارد کنید").When(x => x.ActivityPermission);

            RuleFor(x => x.TempToDate).NotEmpty().WithMessage("لطفا تاریخ انقضای مجوز فعالیت را وارد کنید").When(x => x.ActivityPermission);
        }
    }
}