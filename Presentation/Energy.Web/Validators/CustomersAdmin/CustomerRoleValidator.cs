﻿using FluentValidation;
using Energy.Web.Models.CustomersAdmin;
using Energy.Services.Localization;
using Energy.Web.Framework.Validators;

namespace Energy.Web.Validators.CustomersAdmin
{
    public class CustomerRoleValidator : BaseProValidator<CustomerRoleModel>
    {
        public CustomerRoleValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Customers.CustomerRoles.Fields.Name.Required"));
        }
    }
}