﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Energy.Web.Models.HighLow
{
    public class HighLowModel
    {
        public int rowNumber { get; set; }
        public bool hasError { get; set; }
        public bool zeroActive { get; set; }
        public bool zeroReact { get; set; }
        public bool zeroPower { get; set; }
        public bool negativeActive { get; set; }
        public bool negativeReact { get; set; }
        public bool negativePower { get; set; }
        public bool overflowActive { get; set; }
        public bool overflowReact { get; set; }
        public bool overflowPower { get; set; }
        public bool minorActive { get; set; }
        public bool minorReact { get; set; }
        public bool minorPower { get; set; }
        public string description { get; set; }
        public IList<string> fields { get; set; }
    }
}