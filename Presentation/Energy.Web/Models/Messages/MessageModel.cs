﻿using System;
using FluentValidation.Attributes;
using Energy.Web.Validators.Splits;
using System.Web.Mvc;
using System.Collections.Generic;
using Energy.Web.Framework.Mvc;
using Energy.Core.Domain.Constant;
namespace Energy.Web.Models.Messages
{
    public class MessageModel : BaseProModel
    {
        public MessageModel()
        {
            Years = new List<SelectListItem>();
            Terms = new List<SelectListItem>();
        }
        public IList<SelectListItem> Years { get; set; }
        public IList<SelectListItem> Terms { get; set; }
        public string msgText { get; set; }
    }
}