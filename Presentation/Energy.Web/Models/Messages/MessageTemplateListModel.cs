﻿using System.Collections.Generic;
using System.Web.Mvc;
using Energy.Web.Framework;
using Energy.Web.Framework.Mvc;

namespace Energy.Web.Models.Messages
{
    public partial class MessageTemplateListModel : BaseProModel
    {
        public MessageTemplateListModel()
        {
            AvailableStores = new List<SelectListItem>();
        }

        [ProResourceDisplayName("Admin.ContentManagement.MessageTemplates.List.SearchStore")]
        public int SearchStoreId { get; set; }
        public IList<SelectListItem> AvailableStores { get; set; }
    }
}