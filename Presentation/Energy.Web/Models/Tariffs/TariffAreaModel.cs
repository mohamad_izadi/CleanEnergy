﻿using System;
using Energy.Web.Framework.Mvc;
using Energy.Core.Domain.Tariffs;

namespace Energy.Web.Models.Tariffs
{
    public class TariffAreaModel : BaseProEntityModel
    {
        public int TariffID { get; set; }
        public Tariff Tariff { get; set; }
        public Int32 FromDay { get; set; }
        public Int32 FromMonth { get; set; }
        public Int32 ToDay { get; set; }
        public Int32 ToMonth { get; set; }
        public Double Factor { get; set; }
        public String Title { get; set; }
        public String Description { get; set; }
    }
}