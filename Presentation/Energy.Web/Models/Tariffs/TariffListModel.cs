﻿using System.Collections.Generic;
using System.Web.Mvc;
using Energy.Web.Framework;
using Energy.Web.Framework.Mvc;

namespace Energy.Web.Models.Tariffs
{
    public class TariffListModel : BaseProModel
    {
        public TariffListModel()
        {
            CTariffs = new List<SelectListItem>();
        }

        [ProResourceDisplayName("تعرفه")]
        public int CTariffId { get; set; }
        public IList<SelectListItem> CTariffs { get; set; }
    }
}