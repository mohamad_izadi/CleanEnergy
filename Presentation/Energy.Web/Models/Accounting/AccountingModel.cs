﻿using System;
using System.Web.Mvc;
using System.Collections.Generic;
using Energy.Web.Framework.Mvc;
using Energy.Core.Domain.Constant;

namespace Energy.Web.Models.Accounting
{
    public class AccountingModel : BaseProModel
    {
        public AccountingModel()
        {
            Years = new List<SelectListItem>();
            Terms = new List<SelectListItem>();
        }
        public IList<SelectListItem> Years { get; set; }
        public IList<SelectListItem> Terms { get; set; }
    }
}