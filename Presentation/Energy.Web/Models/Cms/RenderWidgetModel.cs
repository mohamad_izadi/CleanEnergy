﻿using System.Web.Routing;
using Energy.Web.Framework.Mvc;

namespace Energy.Web.Models.Cms
{
    public partial class RenderWidgetModel : BaseProModel
    {
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public RouteValueDictionary RouteValues { get; set; }
    }
}