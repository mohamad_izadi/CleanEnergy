﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Energy.Web.Models.Debit
{
    public class DebitModel
    {
        public virtual bool selected { get; set; }
        public virtual int rowNumber { get; set; }
        public virtual string splitId { get; set; }
        public virtual long lastBillPayable { get; set; }
        public virtual long Payment { get; set; }
        public virtual string PaymentDate { get; set; }
        public virtual string documentNum { get; set; }
        public virtual string customerName { get; set; }
        public virtual string description { get; set; }
        public virtual string Year { get; set; }
        public virtual string Term { get; set; }
    }
}