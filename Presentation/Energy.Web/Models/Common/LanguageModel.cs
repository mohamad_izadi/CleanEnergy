﻿using Energy.Web.Framework.Mvc;

namespace Energy.Web.Models.Common
{
    public partial class LanguageModel : BaseProEntityModel
    {
        public string Name { get; set; }

        public string FlagImageFileName { get; set; }

    }
}