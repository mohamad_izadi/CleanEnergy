﻿using System.Collections.Generic;
using Energy.Web.Framework.Mvc;

namespace Energy.Web.Models.Common
{
    public partial class LanguageSelectorModel : BaseProModel
    {
        public LanguageSelectorModel()
        {
            AvailableLanguages = new List<LanguageModel>();
        }

        public IList<LanguageModel> AvailableLanguages { get; set; }

        public int CurrentLanguageId { get; set; }

        public bool UseImages { get; set; }
    }
}