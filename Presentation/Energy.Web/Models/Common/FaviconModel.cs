﻿using Energy.Web.Framework.Mvc;

namespace Energy.Web.Models.Common
{
    public partial class FaviconModel : BaseProModel
    {
        public string FaviconUrl { get; set; }
    }
}