﻿using Energy.Web.Framework.Mvc;

namespace Energy.Web.Models.Common
{
    public partial class StoreThemeModel : BaseProModel
    {
        public string Name { get; set; }
        public string Title { get; set; }
    }
}