﻿using Energy.Web.Framework.Mvc;

namespace Energy.Web.Models.Common
{
    public partial class AdminHeaderLinksModel : BaseProModel
    {
        public string ImpersonatedCustomerEmailUsername { get; set; }
        public bool IsCustomerImpersonated { get; set; }
        public bool DisplayAdminLink { get; set; }
    }
}