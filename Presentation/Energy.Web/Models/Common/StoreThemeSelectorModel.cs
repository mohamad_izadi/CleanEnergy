﻿using System.Collections.Generic;
using Energy.Web.Framework.Mvc;

namespace Energy.Web.Models.Common
{
    public partial class StoreThemeSelectorModel : BaseProModel
    {
        public StoreThemeSelectorModel()
        {
            AvailableStoreThemes = new List<StoreThemeModel>();
        }

        public IList<StoreThemeModel> AvailableStoreThemes { get; set; }

        public StoreThemeModel CurrentStoreTheme { get; set; }
    }
}