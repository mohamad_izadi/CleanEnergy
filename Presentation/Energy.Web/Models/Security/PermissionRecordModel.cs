﻿using Energy.Web.Framework.Mvc;

namespace Energy.Web.Models.Security
{
    public partial class PermissionRecordModel : BaseProModel
    {
        public string Name { get; set; }
        public string SystemName { get; set; }
    }
}