﻿using System.Collections.Generic;
using System.Web.Mvc;
using FluentValidation.Attributes;
using Energy.Web.Validators.Directory;
using Energy.Web.Framework;
using Energy.Web.Framework.Localization;
using Energy.Web.Framework.Mvc;

namespace Energy.Web.Models.Directory
{
    [Validator(typeof(DistributionAreaValidator))]
    public partial class DistributionAreaModel : BaseProEntityModel
    {
        public int RegionId { get; set; }

        [ProResourceDisplayName("عنوان")]
        [AllowHtml]
        public string Name { get; set; }

        [ProResourceDisplayName("نمایش")]
        public bool Published { get; set; }

        [ProResourceDisplayName("ترتیب نمایش")]
        public int DisplayOrder { get; set; }

    }
}