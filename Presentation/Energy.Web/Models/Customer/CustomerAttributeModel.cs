﻿using System.Collections.Generic;
using Energy.Core.Domain.Common;
using Energy.Web.Framework.Mvc;

namespace Energy.Web.Models.Customer
{
    public partial class CustomerAttributeModel : BaseProEntityModel
    {
        public CustomerAttributeModel()
        {
            Values = new List<CustomerAttributeValueModel>();
        }

        public string Name { get; set; }

        public bool IsRequired { get; set; }

        /// <summary>
        /// Default value for textboxes
        /// </summary>
        public string DefaultValue { get; set; }

        public AttributeControlType AttributeControlType { get; set; }

        public IList<CustomerAttributeValueModel> Values { get; set; }

    }

    public partial class CustomerAttributeValueModel : BaseProEntityModel
    {
        public string Name { get; set; }

        public bool IsPreSelected { get; set; }
    }
}