﻿using System.Web.Mvc;
using FluentValidation.Attributes;
using Energy.Web.Framework;
using Energy.Web.Framework.Mvc;
using Energy.Web.Validators.Customer;

namespace Energy.Web.Models.Customer
{
    [Validator(typeof(PasswordRecoveryValidator))]
    public partial class PasswordRecoveryModel : BaseProModel
    {
        [AllowHtml]
        [ProResourceDisplayName("Account.PasswordRecovery.Email")]
        public string Email { get; set; }

        public string Result { get; set; }

        public bool DisplayCaptcha { get; set; }
    }
}