﻿using Energy.Web.Framework.Mvc;

namespace Energy.Web.Models.Customer
{
    public partial class CustomerAvatarModel : BaseProModel
    {
        public string AvatarUrl { get; set; }

        public CustomerNavigationModel NavigationModel { get; set; }
    }
}