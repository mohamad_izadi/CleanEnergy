﻿using System.Web.Routing;
using Energy.Web.Framework.Mvc;

namespace Energy.Web.Models.Customer
{
    public partial class ExternalAuthenticationMethodModel : BaseProModel
    {
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public RouteValueDictionary RouteValues { get; set; }
    }
}