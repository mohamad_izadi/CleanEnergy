﻿using Energy.Web.Framework.Mvc;

namespace Energy.Web.Models.Customer
{
    public partial class AccountActivationModel : BaseProModel
    {
        public string Result { get; set; }
    }
}