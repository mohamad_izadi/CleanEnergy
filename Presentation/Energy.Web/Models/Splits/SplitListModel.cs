﻿using System.Collections.Generic;
using System.Web.Mvc;
using Energy.Web.Framework;
using Energy.Web.Framework.Mvc;

namespace Energy.Web.Models.Splits
{
    public class SplitListModel : BaseProModel
    {
        public int customerId { get; set; }
        public int splitId { get; set; }
        public string customerName { get; set; }
        public string splitName { get; set; }
        public string fileNo { get; set; }
        public string serialNo { get; set; }
        public string email { get; set; }
        public string phoneNumber { get; set; }
        public string CompanyName { get; set; }
    }
}