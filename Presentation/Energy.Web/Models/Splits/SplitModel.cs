﻿using System;
using FluentValidation.Attributes;
using Energy.Web.Validators.Splits;
using System.Web.Mvc;
using System.Collections.Generic;
using Energy.Core.Domain.Bills;
using Energy.Core.Domain.Customers;
using Energy.Core.Domain.Splits;
using Energy.Web.Framework.Mvc;
using Energy.Core.Domain.Constant;
using System.ComponentModel.DataAnnotations;
using Energy.Web.Framework;
using Energy.Web.Models.CustomersAdmin;

namespace Energy.Web.Models.Splits
{
    [Validator(typeof(SplitValidator))]
    public partial class SplitModel : BaseProEntityModel
    {
        public SplitModel()
        {
            this.DistributionAreas = new List<SelectListItem>();
            this.Regions = new List<SelectListItem>();

            this.CTariffs = new List<SelectListItem>();
        }

        [ProResourceDisplayName("تعرفه")]
        public int CTariffId { get; set; }
        public IList<SelectListItem> CTariffs { get; set; }
        public int Customer_Id { get; set; }

        [ProResourceDisplayName("تفصیل مشترک")]
        public string AccountNo { get; set; }

        [ProResourceDisplayName("درخواست")]
        [AllowHtml]
        public int Apply { get; set; }

        [ProResourceDisplayName("نوع مشترک")]
        [AllowHtml]
        public int Legal { get; set; }
        public String Tariff { get; set; }

        [ProResourceDisplayName("دوره مصرف")]
        [AllowHtml]
        public CPermanent Permanent { get; set; }

        [ProResourceDisplayName("شروع مجوز فعالیت")]
        [UIHint("PersianDateNullable")]
        public DateTime? TempFromDate { get; set; }
        
        [ProResourceDisplayName("انقضای مجوز فعالیت")]
        [UIHint("PersianDateNullable")]
        public DateTime? TempToDate { get; set; }

        [ProResourceDisplayName("مجوز فعالیت")]
        public bool ActivityPermission { get; set; }

        [ProResourceDisplayName("نوع مصرف")]
        [AllowHtml]
        public int Demand { get; set; }

        [ProResourceDisplayName("ولتاژ")]
        [AllowHtml]
        public int Volt { get; set; }

        [ProResourceDisplayName("خط فاز")]
        [AllowHtml]
        public int Phase { get; set; }

        [ProResourceDisplayName("آمپر")]
        [AllowHtml]
        public int Amp { get; set; }

        [ProResourceDisplayName("قدرت")]
        [AllowHtml]
        public int Power { get; set; }

        [ProResourceDisplayName("قدرت پروانه")]
        public string PowerTag { get; set; }

        [ProResourceDisplayName("تاریخ انقضای پروانه")]
        [UIHint("PersianDateNullable")]
        public DateTime? PowerTagDate { get; set; }

        [ProResourceDisplayName("محاسبه هزینه انشعاب آزاد")]
        public bool IsFreelance { get; set; }

        [ProResourceDisplayName("شماره پرونده")]
        [AllowHtml]
        public string FileNo { get; set; }
        public string DisPass { get; set; }
        public int SeqNo { get; set; }
        public CUrban Urban { get; set; }

        [ProResourceDisplayName("آدرس")]
        [AllowHtml]
        public string Address { get; set; }

        [ProResourceDisplayName("آدرس مکاتباتی")]
        [AllowHtml]
        public string AddressContact { get; set; }

        [ProResourceDisplayName("کد پستی")]
        [AllowHtml]
        public string Postal { get; set; }

        [ProResourceDisplayName("سریال کنتور")]
        [AllowHtml]
        public string SerialNo { get; set; }

        [ProResourceDisplayName("نوع کنتور")]
        public int AD { get; set; }

        [ProResourceDisplayName("مدل کنتور")]
        public string Maker { get; set; }

        [ProResourceDisplayName("ضریب ترانس ولتاژ")]
        public string VFactor { get; set; }

        [ProResourceDisplayName("ضریب ترانس جریان")]
        public string AFactor { get; set; }

        [ProResourceDisplayName("ضریب کنتور")]
        public int? Factor { get; set; }

        [ProResourceDisplayName("ساعت فرمان")]
        public int Timer { get; set; }

        [ProResourceDisplayName("تعداد رقم")]
        public int? Digits { get; set; }

        [ProResourceDisplayName("تاریخ نصب")]
        [UIHint("PersianDateNullable")]
        public DateTime? InstallDate { get; set; }

        [ProResourceDisplayName("فعال")]
        public bool Active { get; set; }

        [ProResourceDisplayName("محاسبه هزینه نگهداری شبکه")]
        public bool NetworkCost { get; set; }
        public Nullable<DateTime> CreateDate { get; set; }

        [ProResourceDisplayName("عنوان انشعاب")]
        [AllowHtml]
        public string Title { get; set; }

        [ProResourceDisplayName("تفصیل تعرفه")]
        public string Description { get; set; }

        [ProResourceDisplayName("نام مشترک")]
        [AllowHtml]
        public string CustomerName { get; set; }
        public bool selected { get; set; }
        public int rowNumber { get; set; }

        [ProResourceDisplayName("نام تعرفه")]
        [AllowHtml]
        public int? TarrifAreaId { get; set; }

        [ProResourceDisplayName("منطقه توزیع")]
        public int Region_Id { get; set; }
        public IList<SelectListItem> Regions { get; set; }

        [ProResourceDisplayName("برق منطقه ای")]
        public int DistributionArea_Id { get; set; }

        [ProResourceDisplayName("محاسبه جریمه دیرکرد")]
        public bool IncludeLateFees { get; set; }

        public IList<SelectListItem> DistributionAreas { get; set; }

        public CustomerListModel CustomerList { get; set; }
    }
}