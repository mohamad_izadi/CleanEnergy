﻿using System;
using Energy.Web.Framework.Mvc;
using FluentValidation.Attributes;
using Energy.Web.Validators.Request;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Energy.Web.Framework;
using Energy.Core.Domain.Constant;
using System.Web.Mvc;

namespace Energy.Web.Models.Requests
{
    [Validator(typeof(RequestValidator))]
    public partial class RequestModel : BaseProEntityModel
    {
        public RequestModel()
        {
            this.DistributionAreas = new List<SelectListItem>();
            this.Regions = new List<SelectListItem>();

            RequestFileModels = new List<RequestFileModel>();
            AddFileModel = new RequestFileModel();
        }
        public int Customer_Id { get; set; }

        [ProResourceDisplayName("Admin.Configuration.ActivityLog.ActivityLog.Fields.Customer")]
        public string CustomerEmail { get; set; }
        public int RequestType_Id { get; set; }
        public string RequestTypeName { get; set; }
        public Nullable<int> Split_Id { get; set; }

        [ProResourceDisplayName("منطقه توزیع")]
        public int Region_Id { get; set; }
        public IList<SelectListItem> Regions { get; set; }

        [ProResourceDisplayName("برق منطقه ای")]
        public int DistributionArea_Id { get; set; }
        public IList<SelectListItem> DistributionAreas { get; set; }

        [ProResourceDisplayName("آدرس")]
        public String Address { get; set; }

        [ProResourceDisplayName("آدرس مکاتباتی")]
        public String AddressContact { get; set; }

        [ProResourceDisplayName("کد پستی انشعاب")]
        public String SplitPostalCode { get; set; }

        [ProResourceDisplayName("تعرفه")]
        [AllowHtml]
        public int tariff_Id { get; set; }

        [ProResourceDisplayName("ولتاژ")]
        [AllowHtml]
        public CVolt Volt { get; set; }

        [ProResourceDisplayName("خط فاز")]
        [AllowHtml]
        public CPhase Phase { get; set; }

        [ProResourceDisplayName("آمپر")]
        [AllowHtml]
        public int Amp { get; set; }
        public Int32 Power { get; set; }
        public RequestFileModel AddFileModel { get; set; }
        public IList<RequestFileModel> RequestFileModels { get; set; }
        public DateTime CreatedOn { get; set; }

        #region Nested classes
        public partial class RequestFileModel : BaseProEntityModel
        {
            public int Request_Id { get; set; }

            [UIHint("File")]
            [ProResourceDisplayName("Admin.Documents.Files.Fields.FileName")]
            public int File_Id { get; set; }

            [ProResourceDisplayName("Admin.Documents.Files.Fields.FileName")]
            public string FileName { get; set; }

            [ProResourceDisplayName("Admin.Documents.Files.Fields.FileName")]
            public int FileType { get; set; }

            [ProResourceDisplayName("عنوان فایل")]
            public string Description { get; set; }
        }

        #endregion
    }
}
