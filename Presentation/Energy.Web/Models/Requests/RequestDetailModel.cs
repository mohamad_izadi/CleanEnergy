﻿using Energy.Core.Domain.Constant;
using Energy.Web.Framework;
using Energy.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Energy.Web.Models.Requests
{
    public class RequestDetailModel
    {
        public RequestDetailModel()
        {
            RequestFileModels = new List<RequestFileModel>();
        }
        public int Customer_Id { get; set; }
        public string CustomerEmail { get; set; }

        [ProResourceDisplayName("نام مشترک")]
        public string CustomerName { get; set; }

        [ProResourceDisplayName("عنوان درخواست")]
        public string RequestTypeName { get; set; }
        public Nullable<int> Split_Id { get; set; }
        public string splitName { get; set; }

        [ProResourceDisplayName("برق منطقه ای")]
        public string DistributionArea { get; set; }

        [ProResourceDisplayName("آدرس")]
        public String Address { get; set; }

        [ProResourceDisplayName("آدرس مکاتباتی")]
        public String AddressContact { get; set; }

        [ProResourceDisplayName("کد پستی انشعاب")]
        public String SplitPostalCode { get; set; }

        [ProResourceDisplayName("تعرفه")]
        [AllowHtml]
        public string tariff { get; set; }

        [ProResourceDisplayName("ولتاژ")]
        [AllowHtml]
        public string Volt { get; set; }

        [ProResourceDisplayName("خط فاز")]
        [AllowHtml]
        public string Phase { get; set; }

        [ProResourceDisplayName("آمپر")]
        [AllowHtml]
        public string Amp { get; set; }

        [ProResourceDisplayName("قدرت درخواستی")]
        [AllowHtml]
        public string Power { get; set; }
        public IList<RequestFileModel> RequestFileModels { get; set; }

        [ProResourceDisplayName("تاریخ ایجاد")]
        [AllowHtml]
        public DateTime CreatedOn { get; set; }

        #region Nested classes
        public partial class RequestFileModel : BaseProEntityModel
        {
            public int Request_Id { get; set; }

            [UIHint("File")]
            [ProResourceDisplayName("Admin.Documents.Files.Fields.FileName")]
            public int File_Id { get; set; }

            [ProResourceDisplayName("Admin.Documents.Files.Fields.FileName")]
            public string FileName { get; set; }

            [ProResourceDisplayName("Admin.Documents.Files.Fields.FileName")]
            public string FileUrl { get; set; }

            [ProResourceDisplayName("عنوان فایل")]
            public string Description { get; set; }
        }

        #endregion
    }
}