﻿using Energy.Web.Framework;
using Energy.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Energy.Web.Models.Requests
{
    public class RequestSearchModel : BaseProModel
    {
        public RequestSearchModel()
        {
            RequestType = new List<SelectListItem>();
        }
        [ProResourceDisplayName("Admin.Configuration.ActivityLog.ActivityLog.Fields.CreatedOnFrom")]
        [UIHint("DateNullable")]
        public DateTime? CreatedOnFrom { get; set; }

        [ProResourceDisplayName("Admin.Configuration.ActivityLog.ActivityLog.Fields.CreatedOnTo")]
        [UIHint("DateNullable")]
        public DateTime? CreatedOnTo { get; set; }

        [ProResourceDisplayName("نوع درخواست")]
        public int RequestTypeId { get; set; }

        [ProResourceDisplayName("Admin.Configuration.ActivityLog.ActivityLog.Fields.ActivityLogType")]
        public IList<SelectListItem> RequestType { get; set; }
    }
}