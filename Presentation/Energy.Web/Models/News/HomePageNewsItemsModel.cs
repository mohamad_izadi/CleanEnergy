﻿using System;
using System.Collections.Generic;
using Energy.Web.Framework.Mvc;

namespace Energy.Web.Models.News
{
    public partial class HomePageNewsItemsModel : BaseProModel, ICloneable
    {
        public HomePageNewsItemsModel()
        {
            NewsItems = new List<NewsItemModel>();
        }

        public int WorkingLanguageId { get; set; }
        public IList<NewsItemModel> NewsItems { get; set; }

        public object Clone()
        {
            //we use a shallow copy (deep clone is not required here)
            return this.MemberwiseClone();
        }
    }
}