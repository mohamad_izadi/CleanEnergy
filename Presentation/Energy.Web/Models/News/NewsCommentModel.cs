﻿using System;
using Energy.Web.Framework.Mvc;

namespace Energy.Web.Models.News
{
    public partial class NewsCommentModel : BaseProEntityModel
    {
        public int CustomerId { get; set; }

        public string CustomerName { get; set; }

        public string CustomerAvatarUrl { get; set; }

        public string CommentTitle { get; set; }

        public string CommentText { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool AllowViewingProfiles { get; set; }
    }
}