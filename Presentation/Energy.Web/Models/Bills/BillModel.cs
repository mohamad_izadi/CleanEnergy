﻿using System;
using System.Collections.Generic;
using Energy.Core.Domain.Bills;
using Energy.Core.Domain.Customers;
using Energy.Core.Domain.Splits;
using Energy.Web.Framework.Mvc;
using Energy.Core.Domain.Constant;
using Energy.Core.Domain.Tariffs;
using System.ComponentModel.DataAnnotations;
using Energy.Web.Framework;

namespace Energy.Web.Models.Bills
{
    public class BillModel : BaseProEntityModel
    {
        public virtual Split Split { get; set; }
        public virtual Int32 SplitID { get; set; }
        public virtual Int32 Term { get; set; }
        public virtual Int32 Year { get; set; }
        public virtual DateTime DateReadPre { get; set; }
        public virtual string DateReadPrePersian { get; set; }
        public virtual Double MidReadPre { get; set; }
        public virtual Double MaxReadPre { get; set; }
        public virtual Double MinReadPre { get; set; }
        public virtual Double ReactReadPre { get; set; }
        public virtual Double PowerReadPre { get; set; }

        public virtual Int32 TariffID { get; set; }
        public virtual Int32 TariffAreaID { get; set; }

        [UIHint("PersianDateNullable")]
        public virtual DateTime DateRead { get; set; }

        public virtual string DateReadPersian { get; set; }
        public virtual Double MidRead { get; set; }
        public virtual Double MaxRead { get; set; }
        public virtual Double MinRead { get; set; }
        public virtual Double ReactRead { get; set; }
        public virtual Double PowerRead { get; set; }
        public virtual Double FridayRead { get; set; }

        public virtual Double AllDays { get; set; }
        public virtual Double MidUsed { get; set; }
        public virtual Double MaxUsed { get; set; }
        public virtual Double MinUsed { get; set; }
        public virtual Double ReactUsed { get; set; }
        public virtual Double FridayUsed { get; set; }
        public virtual Double AllUsed { get; set; }
        public virtual Double PowerUsed { get; set; }

        public virtual Int64 Total { get; set; }
        public virtual Int64 Discount { get; set; }
        public virtual Int64 Tax { get; set; }
        public virtual Int64 Toll { get; set; }
        public virtual Int64 Round { get; set; }
        public virtual Int64 Payable { get; set; }
        public virtual DateTime? issueDate { get; set; }
        public virtual DateTime? ExpDate { get; set; }
        public virtual Int64 Paid { get; set; }
        public virtual Int64 Debit { get; set; }
        public virtual DateTime? PaidDate { get; set; }

        public virtual String PaymentID { get; set; }

        public virtual String PaymentIdWD { get; set; }

        public virtual DateTime? EditDate { get; set; }
        public virtual Boolean Fixed { get; set; }

        public virtual Double NormalDays { get; set; }
        public virtual Double WarmDays { get; set; }
        public virtual Double WarmDaysFactor { get; set; }
        public virtual Double WarmDaysX { get; set; }
        public virtual Double AllDaysX { get; set; }
        public virtual Double NormalUsed { get; set; }
        public virtual Double WarmUsed { get; set; }
        public virtual Double NormalUsedAvrg { get; set; }
        public virtual Double WarmUsedAvrg { get; set; }
        public virtual Double NormalCostAvrg { get; set; }
        public virtual Double WarmCostAvrg { get; set; }
        public virtual Double NormalCost { get; set; }
        public virtual Double WarmCost { get; set; }
        public virtual Int64 AllCost { get; set; }

        public virtual Double WarmMaxUsed { get; set; }
        public virtual Double WarmMaxFineCost { get; set; }

        public virtual Double NormalMaxUsed { get; set; }
        public virtual Double NormalMaxFineCost { get; set; }

        public virtual Int64 FineCost { get; set; }

        public virtual Double WarmMindUsed { get; set; }
        public virtual Double WarmMindDiscountCost { get; set; }

        public virtual Double NormalMindUsed { get; set; }
        public virtual Double NormalMindDiscountCost { get; set; }

        public virtual Int64 DiscountCost { get; set; }

        public virtual Double MidRate { get; set; }
        public virtual Double MaxRate { get; set; }
        public virtual Double MinRate { get; set; }
        public virtual Double ReactRate { get; set; }
        public virtual Double MidCost { get; set; }
        public virtual Double MaxCost { get; set; }
        public virtual Double MinCost { get; set; }
        public virtual Double ReactCost { get; set; }
        public virtual Double PowerCost { get; set; }
        public virtual Double Power90 { get; set; }
        public virtual Int64 Power90Cost { get; set; }
        public virtual Int64 IndustDisCost { get; set; }
        public virtual Int64 FreelanceCost { get; set; }
        public virtual Int64 ExceedCost { get; set; }
        public virtual Int64 NetworkCost { get; set; }
        public virtual Int64 UnIndustCost { get; set; }
        public virtual Int64 TagExpireCost { get; set; }
        public virtual Double PowerFactor { get; set; }
        public virtual Double LossFactor { get; set; }
        public virtual String Title { get; set; }
        public virtual String Description { get; set; }
        public virtual String Emergency { get; set; }
        public virtual string Type { get; set; }
        public virtual bool exist { get; set; }
        public virtual int rowNumber { get; set; }
        public virtual bool selected { get; set; }
        public virtual IList<BillNormal> billNormal { get; set; }
        public virtual IList<TariffRate> tariffRates { get; set; }
        public virtual double LatePenalty { get; set; }
        public virtual int PictureId { get; set; }
        public virtual string PictureUrl { get; set; }

        #region Nested classes

        public partial class BillPictureModel : BaseProEntityModel
        {
            public int BillId { get; set; }

            [UIHint("Picture")]
            [ProResourceDisplayName("Admin.Documents.Files.Fields.FileName")]
            public int PicId { get; set; }

            [ProResourceDisplayName("Admin.Documents.Files.Fields.Description")]
            public string Description { get; set; }
        }

        #endregion
    }

    
}
