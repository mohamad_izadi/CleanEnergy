﻿using System.Collections.Generic;
using System.Web.Mvc;
using FluentValidation.Attributes;
using Energy.Web.Validators.CustomersAdmin;
using Energy.Web.Framework;
using Energy.Web.Framework.Mvc;

namespace Energy.Web.Models.CustomersAdmin
{
    [Validator(typeof(CustomerRoleValidator))]
    public partial class CustomerRoleModel : BaseProEntityModel
    {
        [ProResourceDisplayName("Admin.Customers.CustomerRoles.Fields.Name")]
        [AllowHtml]
        public string Name { get; set; }

        [ProResourceDisplayName("Admin.Customers.CustomerRoles.Fields.Active")]
        public bool Active { get; set; }

        [ProResourceDisplayName("Admin.Customers.CustomerRoles.Fields.IsSystemRole")]
        public bool IsSystemRole { get; set; }

        [ProResourceDisplayName("Admin.Customers.CustomerRoles.Fields.SystemName")]
        public string SystemName { get; set; }
    }
}