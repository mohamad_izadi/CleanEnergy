﻿using Energy.Web.Framework;
using Energy.Web.Framework.Mvc;

namespace Energy.Web.Models.CustomersAdmin
{
    public partial class RegisteredCustomerReportLineModel : BaseProModel
    {
        [ProResourceDisplayName("Admin.Customers.Reports.RegisteredCustomers.Fields.Period")]
        public string Period { get; set; }

        [ProResourceDisplayName("Admin.Customers.Reports.RegisteredCustomers.Fields.Customers")]
        public int Customers { get; set; }
    }
}