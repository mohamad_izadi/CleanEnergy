﻿using System.Collections.Generic;
using Energy.Web.Models.Common;

namespace Energy.Web.Models.PrivateMessages
{
    public partial class PrivateMessageListModel
    {
        public IList<PrivateMessageModel> Messages { get; set; }
        public PagerModel PagerModel { get; set; }
    }
}