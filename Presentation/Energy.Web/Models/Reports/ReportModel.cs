﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Energy.Web.Models.Reports
{
    public class ReportModel
    {
        public virtual int rowNumber { get; set; }
        public virtual int splitId { get; set; }
        public virtual string CustomerName { get; set; }
        public virtual string SplitName { get; set; }
        public virtual double MidUsed { get; set; }
        public virtual double MaxUsed { get; set; }
        public virtual double MinUsed { get; set; }
        public virtual double AllUsed { get; set; }
        public virtual double ReactUsed { get; set; }
        public virtual long Total { get; set; }
        public virtual long Tax { get; set; }
        public virtual long Toll { get; set; }
        public virtual long Debit { get; set; }
        public virtual double DelayPenalty { get; set; }
        public virtual long BillPayable { get; set; }
        public virtual string customerName { get; set; }
        public virtual string description { get; set; }
        public virtual int Year { get; set; }
        public virtual int Term { get; set; }
        public virtual Int64 NetworkCost { get; set; }
        public virtual long FreelanceCost { get; set; }
    }
}