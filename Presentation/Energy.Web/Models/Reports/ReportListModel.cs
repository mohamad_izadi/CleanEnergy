﻿using Energy.Web.Framework;
using Energy.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Energy.Web.Models.Report
{
    public class ReportListModel : BaseProModel
    {
        public ReportListModel()
        {
            CTariffs = new List<SelectListItem>();
            Years = new List<SelectListItem>();
            Terms = new List<SelectListItem>();

            DistributionAreas = new List<SelectListItem>();
            Regions = new List<SelectListItem>();
        }

        [ProResourceDisplayName("تعرفه")]
        public int CTariffId { get; set; }
        public IList<SelectListItem> CTariffs { get; set; }
        public IList<SelectListItem> Years { get; set; }
        public IList<SelectListItem> Terms { get; set; }

        [ProResourceDisplayName("منطقه توزیع")]
        public int Region_Id { get; set; }
        public IList<SelectListItem> Regions { get; set; }

        [ProResourceDisplayName("برق منطقه ای")]
        public int DistributionArea_Id { get; set; }
        public IList<SelectListItem> DistributionAreas { get; set; }

        public bool IncludeAll { get; set; }
    }
}