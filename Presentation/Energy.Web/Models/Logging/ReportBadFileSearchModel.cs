﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Energy.Web.Framework;
using Energy.Web.Framework.Mvc;

namespace Energy.Web.Models.Logging
{
    public partial class ReportBadFileSearchModel : BaseProModel
    {
        [ProResourceDisplayName("Admin.Configuration.ActivityLog.ReportBadFile.Fields.CreatedOnFrom")]
        [UIHint("PersianDateNullable")]
        public DateTime? CreatedOnFrom { get; set; }

        [ProResourceDisplayName("Admin.Configuration.ActivityLog.ReportBadFile.Fields.CreatedOnTo")]
        [UIHint("PersianDateNullable")]
        public DateTime? CreatedOnTo { get; set; }

        [ProResourceDisplayName("Admin.Configuration.ActivityLog.ReportBadFile.Fields.ReportBadFileType")]
        public int ReportBadFileType { get; set; }
    }
}