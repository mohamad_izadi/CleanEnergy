﻿using System;
using Energy.Web.Framework;
using Energy.Web.Framework.Mvc;

namespace Energy.Web.Models.Logging
{
    public partial class ReportBadFileModel : BaseProEntityModel
    {
        [ProResourceDisplayName("Admin.Configuration.ActivityLog.ReportBadFilde.Fields.ArticleId")]
        public int ArticleId { get; set; }

        [ProResourceDisplayName("Admin.Configuration.ActivityLog.ReportBadFilde.Fields.Title")]
        public string Title { get; set; }

        [ProResourceDisplayName("Admin.Configuration.ActivityLog.ReportBadFilde.Fields.Titleen")]
        public string Titleen { get; set; }

        [ProResourceDisplayName("Admin.Configuration.ActivityLog.ReportBadFilde.Fields.IsFixed")]
        public bool IsFixed { get; set; }

        [ProResourceDisplayName("Admin.Configuration.ActivityLog.ReportBadFilde.Fields.FirstDateOfReport")]
        public DateTime FirstDateOfReport { get; set; }

        [ProResourceDisplayName("Admin.Configuration.ActivityLog.ReportBadFilde.Fields.CountOfReport")]
        public int CountOfReport { get; set; }
    }
}