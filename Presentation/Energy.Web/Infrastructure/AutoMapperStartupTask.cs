﻿using AutoMapper;
using Energy.Core.Domain.Splits;
using Energy.Web.Models.Splits;
using Energy.Core.Infrastructure;
using Energy.Core.Domain.Customers;
using Energy.Core.Domain.Bills;
using Energy.Core.Domain.Logging;
using Energy.Core.Domain.Requests;
using Energy.Core.Domain.Directory;
using Energy.Web.Models.CustomersAdmin;
using Energy.Web.Models.Logging;
using Energy.Web.Models.Bills;
using Energy.Web.Models.Directory;
using Energy.Web.Models.Requests;
using Energy.Web.Models.Messages;
using Energy.Core.Domain.Messages;
using Energy.Web.Models.Tariffs;
using Energy.Core.Domain.Tariffs;



namespace Energy.Web.Infrastructure
{
    public class AutoMapperStartupTask : IStartupTask
    {
        public void Execute()
        {
            //TODO remove 'CreatedOnUtc' ignore mappings because now presentation layer models have 'CreatedOn' property and core entities have 'CreatedOnUtc' property (distinct names)

            //Splits
            Mapper.CreateMap<Split, SplitModel>()
                .ForMember(dest => dest.CustomProperties, mo => mo.Ignore())
                .ForMember(dest => dest.CTariffs, mo => mo.Ignore())
                .ForMember(dest => dest.CustomerName, mo => mo.Ignore())
                .ForMember(dest => dest.selected, mo => mo.Ignore())
                .ForMember(dest => dest.rowNumber, mo => mo.Ignore())
                .ForMember(dest => dest.TarrifAreaId, mo => mo.Ignore())
                .ForMember(dest => dest.Regions, mo => mo.Ignore())
                .ForMember(dest => dest.DistributionAreas, mo => mo.Ignore())
                .ForMember(dest => dest.Tariff, mo => mo.Ignore())
                .ForMember(dest => dest.CustomerList, mo => mo.Ignore());
            Mapper.CreateMap<SplitModel, Split>();

            //Bills
            Mapper.CreateMap<Bill, BillModel>()
                .ForMember(dest => dest.CustomProperties, mo => mo.Ignore())
                .ForMember(dest => dest.DateReadPrePersian, mo => mo.Ignore())
                .ForMember(dest => dest.DateReadPersian, mo => mo.Ignore())
                .ForMember(dest => dest.exist, mo => mo.Ignore())
                .ForMember(dest => dest.rowNumber, mo => mo.Ignore())
                .ForMember(dest => dest.selected, mo => mo.Ignore())
                .ForMember(dest => dest.billNormal, mo => mo.Ignore())
                .ForMember(dest => dest.tariffRates, mo => mo.Ignore());
            Mapper.CreateMap<BillModel, Bill>()
                .ForMember(dest => dest.LastPaymentDate1, mo => mo.Ignore())
                .ForMember(dest => dest.LastPayment1, mo => mo.Ignore())
                .ForMember(dest => dest.LastPaymentDate2, mo => mo.Ignore())
                .ForMember(dest => dest.LastPayment2, mo => mo.Ignore())
                .ForMember(dest => dest.IsLastTerm, mo => mo.Ignore());

            //Tariffs
            Mapper.CreateMap<TariffArea, TariffAreaModel>()
                .ForMember(dest => dest.CustomProperties, mo => mo.Ignore());
            Mapper.CreateMap<TariffAreaModel, TariffArea>()
                .ForMember(dest => dest.Tariff, mo => mo.Ignore())
                .ForMember(dest => dest.SplitAreas, mo => mo.Ignore());

            //regions
            Mapper.CreateMap<RegionModel, Region>()
                .ForMember(dest => dest.DistributionAreas, mo => mo.Ignore());
            Mapper.CreateMap<Region, RegionModel>()
                .ForMember(dest => dest.NumberOfDistributionAreas, mo => mo.MapFrom(src => src.DistributionAreas != null ? src.DistributionAreas.Count : 0))
                .ForMember(dest => dest.CustomProperties, mo => mo.Ignore());

            //distribution areas
            Mapper.CreateMap<DistributionArea, DistributionAreaModel>()
                .ForMember(dest => dest.CustomProperties, mo => mo.Ignore());
            Mapper.CreateMap<DistributionAreaModel, DistributionArea>()
                .ForMember(dest => dest.Region, mo => mo.Ignore());

            //requests
            Mapper.CreateMap<Request, RequestModel>()
                .ForMember(dest => dest.CustomProperties, mo => mo.Ignore())
                .ForMember(dest => dest.Regions, mo => mo.Ignore())
                .ForMember(dest => dest.DistributionAreas, mo => mo.Ignore())
                .ForMember(dest => dest.Region_Id, mo => mo.Ignore())
                .ForMember(dest => dest.AddFileModel, mo => mo.Ignore())
                .ForMember(dest => dest.RequestFileModels, mo => mo.Ignore())
                .ForMember(dest => dest.CustomerEmail, mo => mo.MapFrom(src => src.Customer.Email))
                .ForMember(dest => dest.CreatedOn, mo => mo.Ignore())
                .ForMember(dest => dest.RequestTypeName, mo => mo.MapFrom(src => src.RequestType.Name));

            Mapper.CreateMap<RequestModel, Request>()
                .ForMember(dest => dest.CreateDate, mo => mo.Ignore())
                .ForMember(dest => dest.RequestFiles, mo => mo.Ignore())
                .ForMember(dest => dest.Customer, mo => mo.Ignore())
                .ForMember(dest => dest.RequestType, mo => mo.Ignore());

            //customer roles
            Mapper.CreateMap<CustomerRole, CustomerRoleModel>()
                .ForMember(dest => dest.CustomProperties, mo => mo.Ignore());
            Mapper.CreateMap<CustomerRoleModel, CustomerRole>()
                .ForMember(dest => dest.PermissionRecords, mo => mo.Ignore());

            //logs
            Mapper.CreateMap<Log, LogModel>()
                .ForMember(dest => dest.CustomerEmail, mo => mo.Ignore())
                .ForMember(dest => dest.CreatedOn, mo => mo.Ignore())
                .ForMember(dest => dest.CustomProperties, mo => mo.Ignore());
            Mapper.CreateMap<LogModel, Log>()
                .ForMember(dest => dest.CreatedOnUtc, mo => mo.Ignore())
                .ForMember(dest => dest.LogLevelId, mo => mo.Ignore())
                .ForMember(dest => dest.Customer, mo => mo.Ignore());

            //ActivityLogType
            Mapper.CreateMap<ActivityLogTypeModel, ActivityLogType>()
                .ForMember(dest => dest.SystemKeyword, mo => mo.Ignore());
            Mapper.CreateMap<ActivityLogType, ActivityLogTypeModel>()
                .ForMember(dest => dest.CustomProperties, mo => mo.Ignore());
            Mapper.CreateMap<ActivityLog, ActivityLogModel>()
                .ForMember(dest => dest.ActivityLogTypeName, mo => mo.MapFrom(src => src.ActivityLogType.Name))
                .ForMember(dest => dest.CustomerEmail, mo => mo.MapFrom(src => src.Customer.Email))
                .ForMember(dest => dest.CreatedOn, mo => mo.Ignore())
                .ForMember(dest => dest.CustomProperties, mo => mo.Ignore());

            //logs
            Mapper.CreateMap<Log, LogModel>()
                .ForMember(dest => dest.CustomerEmail, mo => mo.Ignore())
                .ForMember(dest => dest.CreatedOn, mo => mo.Ignore())
                .ForMember(dest => dest.CustomProperties, mo => mo.Ignore());

            Mapper.CreateMap<LogModel, Log>()
                .ForMember(dest => dest.CreatedOnUtc, mo => mo.Ignore())
                .ForMember(dest => dest.LogLevelId, mo => mo.Ignore())
                .ForMember(dest => dest.Customer, mo => mo.Ignore());

            //ActivityLogType
            Mapper.CreateMap<ActivityLogTypeModel, ActivityLogType>()
                .ForMember(dest => dest.SystemKeyword, mo => mo.Ignore());
            Mapper.CreateMap<ActivityLogType, ActivityLogTypeModel>()
                .ForMember(dest => dest.CustomProperties, mo => mo.Ignore());
            Mapper.CreateMap<ActivityLog, ActivityLogModel>()
                .ForMember(dest => dest.ActivityLogTypeName, mo => mo.MapFrom(src => src.ActivityLogType.Name))
                .ForMember(dest => dest.CustomerEmail, mo => mo.MapFrom(src => src.Customer.Email))
                .ForMember(dest => dest.CreatedOn, mo => mo.Ignore())
                .ForMember(dest => dest.CustomProperties, mo => mo.Ignore());

            //email account
            Mapper.CreateMap<EmailAccount, EmailAccountModel>()
                .ForMember(dest => dest.Password, mo => mo.Ignore())
                .ForMember(dest => dest.IsDefaultEmailAccount, mo => mo.Ignore())
                .ForMember(dest => dest.SendTestEmailTo, mo => mo.Ignore())
                .ForMember(dest => dest.CustomProperties, mo => mo.Ignore());
            Mapper.CreateMap<EmailAccountModel, EmailAccount>()
                .ForMember(dest => dest.Password, mo => mo.Ignore());

            //message template
            Mapper.CreateMap<MessageTemplate, MessageTemplateModel>()
                .ForMember(dest => dest.AllowedTokens, mo => mo.Ignore())
                .ForMember(dest => dest.HasAttachedDownload, mo => mo.Ignore())
                .ForMember(dest => dest.Locales, mo => mo.Ignore())
                .ForMember(dest => dest.AvailableEmailAccounts, mo => mo.Ignore())
                .ForMember(dest => dest.CustomProperties, mo => mo.Ignore());
            Mapper.CreateMap<MessageTemplateModel, MessageTemplate>()
                .ForMember(dest => dest.LimitedToStores, mo => mo.Ignore());

            //queued email
            Mapper.CreateMap<QueuedEmail, QueuedEmailModel>()
                .ForMember(dest => dest.EmailAccountName, mo => mo.MapFrom(src => src.EmailAccount != null ? src.EmailAccount.FriendlyName : string.Empty))
                .ForMember(dest => dest.CreatedOn, mo => mo.Ignore())
                .ForMember(dest => dest.PriorityName, mo => mo.Ignore())
                .ForMember(dest => dest.SentOn, mo => mo.Ignore())
                .ForMember(dest => dest.CustomProperties, mo => mo.Ignore());
            Mapper.CreateMap<QueuedEmailModel, QueuedEmail>()
                .ForMember(dest => dest.Priority, dt => dt.Ignore())
                .ForMember(dest => dest.PriorityId, dt => dt.Ignore())
                .ForMember(dest => dest.CreatedOnUtc, dt=> dt.Ignore())
                .ForMember(dest => dest.SentOnUtc, mo => mo.Ignore())
                .ForMember(dest => dest.EmailAccount, mo => mo.Ignore())
                .ForMember(dest => dest.EmailAccountId, mo => mo.Ignore())
                .ForMember(dest => dest.AttachmentFilePath, mo => mo.Ignore())
                .ForMember(dest => dest.AttachmentFileName, mo => mo.Ignore())
                .ForMember(dest => dest.AttachedDownloadId, mo => mo.Ignore());

        }


        public int Order
        {
            get { return 0; }
        }
    }
}