using Autofac;
using Autofac.Core;
using Energy.Core.Caching;
using Energy.Core.Infrastructure;
using Energy.Core.Infrastructure.DependencyManagement;
using Energy.Web.Controllers;

namespace Energy.Web.Infrastructure
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder)
        {
            //we cache presentation models between requests
            builder.RegisterType<NewsController>()
                .WithParameter(ResolvedParameter.ForNamed<ICacheManager>("nop_cache_static"));
            
        }

        public int Order
        {
            get { return 2; }
        }
    }
}
